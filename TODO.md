# TODO

`****` sqrt_inv_kZtZ is computed only when Z is modified DONE

`****` store filter1 and filter_gaussian as attributes DONE

`****` create a test file that loads a matlab cifar-10 model, perform an encoding pass, train an svm with miso, and evaluate the performance on a grid of regularization parameters. DONE

`****` create a test file that learns a one-layer ckn unsupervised model, perform an encoding pass, train an svm with miso, and evaluate the performance on a grid of regularization parameters. DONE

`****` create a test file that learns a two-layer ckn unsupervised model with high performance, perform an encoding pass, train an svm with miso, and evaluate the performance on a grid of regularization parameters. DONE

`****` linear_pooling: if n_filters > 10 (to be replaced by 2 conv1d on each direction)

`****` create template for miso (on float or double, c.f. Daan's code)

`****` write bindings for k-means

`**` for big data sets: one run to extract batches + on each batches sample patches

`*` Mask for non-squared images
