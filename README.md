# CONVOLUTIONAL KERNEL NETWORK
---

Re-implementation of Convolutional Kernel Network (CKN) from Mairal (2016)
in Python based on the TensorFlow framework (<https://www.tensorflow.org/>).

Authors: Ghislain Durif <br>
Credits: Alberto Bietti, Dexiong Chen, Julien Mairal, Daan Wynen <br>
Copyright: 2017 Inria <br>

If you encounter any issue when installing or using the 'ckntf' package, you can
contact us at <ckn.dev@inria.fr> (for bug report, support, etc.).

---

The [ckntf](./ckntf) package is available under the BSD license. The additional
[miso_svm](./miso_svm), [spherical_kmeans](./spherical_kmeans) and
[whitening](./whitening) packages are available under the GPL-v3 license.


---

## Requirement:
* Python3.x
* Numpy, Scipy
* Tensorflow

* Python packages [miso_svm](./miso_svm), [spherical_kmeans](./spherical_kmeans)
  and [whitening](./whitening) are required to run included tutorials and
  examples. They are available in the corresponding sub-folders (and require
  the MKL library that is available with the Anaconda Python distribution).

---

## Installation

You can check the dedicated [file](./doc/Install.md). <br>

Regarding packages `miso_svm`, `spherical_kmeans` and `whitening`, you can
check the installation instructions in [miso_svm/README.md](./miso_svm/README.md),
[spherical_kmeans/README.md](./spherical_kmeans/README.md) and
[whitening/README.md](./whitening/README.md) respectively.

---

## Tutorial

**NB:** To run the tutorial, you need to download the Cifar10 data set
(either or both version):
* the pre-whitened version `cifar10_whitened.pkl` from
  <http://pascal.inrialpes.fr/data2/gdurif/cifar10_whitened.pkl>
* the standard version `cifar10_batches` from
  <http://pascal.inrialpes.fr/data2/gdurif/cifar10_batches.tar.gz>
You can either directly put the file `cifar10_whitened.pkl` and
the directory `cifar10_batches` (after you extracted it
with the command `tar -zxvf cifar10_batches.tar.gz`) in
the `ckntf/data` sub-folder before installing the `ckntf` package
(with `python setup.py install`), or you can store them somewhere else on
your machine, and you will have to modify the tutorial examples (see
[unsupervised_example.py](./ckntf/tutorials/unsupervised_example.py) or
[supervised_example.py](./ckntf/tutorials/supervised_example.py))
to load the data with the correct path.

A tutorial is available as a sub-module [ckntf.tutorials](./ckntf/tutorials) and
a doc [page](./doc/Tutorials.md). To replicate the following results that
were presented in Mairal (2016), every details can be found on the dedicated
[page](./doc/Tutorials.md). <br>

### Results

Reproduction of the results from Mairal (2016) with the 'ckntf' python package.
The results from the original paper (Mairal, 2016) were achieved using
some `cudnn`-based Matlab code available at
<https://gitlab.inria.fr/mairal/ckn-cudnn-matlab>.

#### Unsupervised CKN

Here is a summary of the results regarding **unsupervised** CKN on
Cifar10 image data set, with online whitening but without data augmentation
and/or model averaging.

| Architectures | nb layers | nb filters    | filter size   | subsampling | sigma        | Accuracy |
|:--------------|:---------:|:-------------:|:-------------:|:-----------:|:------------:|:--------:|
| (1)           | 1         | 64            | 3x3           | 2           | 0.6          | ~68.0    |
| (2)           | 2         | 64 <br> 256   | 3x3 <br> 2x2  | 2 <br> 6    | 0.6 <br> 0.6 | ~77.4    |
| (3)           | 2         | 256 <br> 1024 | 3x3 <br> 2x2  | 2 <br> 6    | 0.6 <br> 0.6 | ~81.2    |
| (4)           | 2         | 512 <br> 4096 | 3x3 <br> 2x2  | 2 <br> 6    | 0.6 <br> 0.6 | ~83.6    |


#### Supervised CKN

Regarding **supervised** CKN model training (without data augmentation),
the best results regarding prediction accuracy were achieved with a 14 layer
CKN model. Details about the architectures that were used can be found
[here](./ckntf/tutorials/supervised_example.py).

**NB:** The Matlab-based code was able to reach an accuracy of 90.5% with
the 14 layer model on the pre-whitened Cifar10 data set. For unknow reasons
(we did run extensive tests), the Tensorflow-based package 'ckntf' was only
able to reach an accuracy of 88.50%. However, for smaller architectures (such
as the 5 layer architecture defined in the tutorial), the 'ckntf' package was
able to exactly reproduce the results from the Matlab-based code (accuracy
of 81.5%).

---

## For developpers

(on GNU/Linux and MacOs only)

* To build/install/test the package, see:

```bash
./dev_command.sh help
```

---

## References

Mairal, J., 2016. End-to-end kernel learning with supervised convolutional
kernel networks, in: Advances in Neural Information Processing
Systems. pp. 1399–1407.
