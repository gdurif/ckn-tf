import os
import sys

from distutils.core import setup, Extension
import distutils.util
import warnings

import tensorflow as tf

# required pkgs
dependencies = ['numpy', 'scipy']

# version
ckn_version = open('version', 'r').read().replace('\n', '')

# data
if not os.path.isfile(os.path.join('ckntf', 'data', 'cifar10_whitened.pkl')) \
    and not os.path.isdir(os.path.join('ckntf', 'data', 'cifar10_batches')):
    warnings.warn("The Cifar10 data set is not available in "
                    + "the 'ckntf.data' sub-folder. You will not be able "
                    + "to reproduce the joined examples. Please check the "
                    + "README.md file in the main directory to see how to "
                    + "download the data files before installing "
                    + "the ckntf package.")

ckntf_data_list = []
if os.path.isfile(os.path.join('ckntf', 'data', 'cifar10_whitened.pkl')):
    ckntf_data_list.append(os.path.join('data', 'cifar10_whitened.pkl'))
if os.path.isdir(os.path.join('ckntf', 'data', 'cifar10_batches')):
    for i in range(5):
        ckntf_data_list.append(os.path.join('data',
                    'cifar10_batches', 'data_batch' + str(i+1)))
    ckntf_data_list.append(os.path.join('data', 'cifar10_batches', 'test_batch'))


version='{v[0]}.{v[1]}'.format(v=list(sys.version_info[:2]))

# includes numpy : package numpy.distutils , numpy.get_include()
# python setup.py build
# python setup.py install --prefix=dist,
incs = ['.'] + [tf.sysconfig.get_include()]

osname = distutils.util.get_platform()
cc_flags = ['-fPIC', '-std=c++11', '-shared']

libdirs = []
libs = []
link_flags = []

##path = os.environ['PATH']; print "XX OS %s, path %s" %(osname,path)

ckntf_op = Extension(
    'ckntf.core.ops_cc.matrix_inverse_sqrt_op',
    sources = [os.path.join('ckntf', 'core', 'ops_cc', 'matrix_inverse_sqrt_op.cc'),
               os.path.join('ckntf', 'core', 'ops_cc', 'linalg_ops_common.cc')],
    include_dirs = incs,
    extra_compile_args = ['-O2', '-D_GLIBCXX_USE_CXX11_ABI=0'] + cc_flags,
    library_dirs = libdirs,
    libraries = libs,
    extra_link_args = link_flags,
    language = 'c++',
    depends = [os.path.join('ckntf', 'core', 'ops_cc', 'linalg_ops_common.h')],
)


setup ( name = 'ckntf',
        version= ckn_version,
        description='Python implementation of Convolutional Kernel Network',
        author = 'Ghislain Durif',
        author_email = 'ckn.dev@inria.fr',
        url = 'https://gitlab.inria.fr/thoth/ckn',
        license='BSD',
        ext_modules = [ckntf_op,],
        packages = ['ckntf', 'ckntf.core', 'ckntf.core.ops_cc', 'ckntf.data',
                    'ckntf.input', 'ckntf.output', 'ckntf.tutorials'],

        package_dir={'ckntf': 'ckntf'},
        package_data={'ckntf': ckntf_data_list},
        include_package_data=True,

        classifiers=[
            # How mature is this project? Common values are
            #   3 - Alpha
            #   4 - Beta
            #   5 - Production/Stable
            'Development Status :: 4 - Beta',

            # Indicate who your project is intended for
            'Intended Audience :: Science/Research',
            'Topic :: Scientific/Engineering :: Mathematics',

            # Pick your license as you wish (should match "license" above)
            'License :: OSI Approved :: BSD License',

            # Specify the Python versions you support here. In particular, ensure
            # that you indicate whether you support Python 2, Python 3 or both.
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.2',
            'Programming Language :: Python :: 3.3',
            'Programming Language :: Python :: 3.4',
            'Programming Language :: Python :: 3.5',
            'Programming Language :: Python :: 3.6',
        ],

        # keywords='optimization',
        # install_requires=['numpy', 'scipy', 'scikit-learn'],
)

# Check TF version as it requires > 1.0
if int(tf.__version__.split(".")[0]) < 1:
    print("Please upgrade to TensorFlow >= 1.0.0")
