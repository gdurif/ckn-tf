"""Spherical k-means

Python version (for comparison)
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Dexiong Chen, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Dexiong Chen", "Julien Mairal"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import numpy as np

def kmeans(X, k, max_iter=1000, init=None):
	"""normal kmeans"""
	n_samples, n_features = X.shape
	if init is None:
		perm =  np.random.choice(np.arange(n_samples), k)
		centroids = X[perm]

	norm2 = np.linalg.norm(X, axis=1, keepdims=1)**2
	prev_obj = np.inf

	for n_iter in range(max_iter):
		dist2 = np.linalg.norm(centroids, axis=1)**2 - 2.0*np.dot(X, centroids.T) + norm2
		assign = np.argmin(dist2, axis=1)
		obj = dist2[np.unravel_index(assign, dist2.shape)].mean()
		# print dist.shape
		if (n_iter+1)%10 == 0:
			print("kmeans iter %d, objective: %f"%(n_iter+1, obj))

		for j in range(k):
			Xj = X[assign==j]
			if Xj.shape[0] == 0:
				centroids[j] = X[np.random.randint(n_samples)]
			else:
				centroids[j] = np.mean(Xj, axis=0)

		if np.abs(prev_obj-obj)/(np.abs(obj)+1e-20) < 1e-8:
			break
		prev_obj = obj

		# stop criteria
	return centroids, assign

def spherical_kmeans(X, k, max_iter=1000, init=None):
	"""X: n x d points with unit-norm
	"""
	n_samples, n_features = X.shape
	if init is None:
		perm =  np.random.choice(np.arange(n_samples), k)
		centroids = X[perm]

	prev_obj = np.inf
	for n_iter in range(max_iter):
		cos_sim = np.dot(X, centroids.T)
		assign = np.argmax(cos_sim, axis=1)
		obj = cos_sim[np.unravel_index(assign, cos_sim.shape)].mean()

		if (n_iter+1)%10 == 0:
			print("spherical kmeans iter %d, objective: %f"%(n_iter+1, obj))

		for j in range(k):
			Xj = X[assign==j]
			if Xj.shape[0] == 0:
				centroids[j] = X[np.random.randint(n_samples)]
			else:
				centroids[j] = np.sum(Xj, axis=0)
				norm = np.linalg.norm(centroids[j])
				centroids[j] /= norm
		if np.abs(prev_obj-obj)/(np.abs(obj)+1e-20) < 1e-8:
			break
		prev_obj = obj

		# stop criteria
	return centroids, assign
