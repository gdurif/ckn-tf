#! /usr/bin/env python
"""Wrapper for fast spherical k-means by Julien Mairal"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import numpy as np

from numbers import Number

from spherical_kmeans._kmeans import cfast_kmeans


def fast_kmeans(input_data, n_clusters, num_iter, verbose=1):
    """fast spherical k-means (wrapper for Julien Mairal's code in C)

    Args:
        input_data (np.array): matrix n_samples x n_features.
        n_clusters (int): number of clusters to consider.
        num_iter (int): number of iterations in the k-means algorithm.
        verbose (int): 0-1 value indicating verbosity.

    """

    ## check input
    if(not isinstance(n_clusters, Number) and not n_clusters.is_integer()):
        raise TypeError("'n_clusters' should be an integer")
    if(not isinstance(num_iter, Number) and not num_iter.is_integer()):
        raise TypeError("'num_iter' should be an integer")
    if(not isinstance(verbose, Number) and not verbose.is_integer() and verbose not in (0,1)):
        raise TypeError("'verbose' should be 0-1 valued")

    if(not isinstance(input_data, np.ndarray)):
        raise TypeError("'input_data' should be a numpy array")

    if(len(input_data.shape) != 2):
        raise ValueError("'input_data' should a matrix (i.e. a 2D array) of shape n_samples x n_features")

    if(n_clusters > input_data.shape[0]):
        raise ValueError("Requested number of clusters is too large (higher than number of samples)")

    ## run
    return cfast_kmeans(input_data, n_clusters, num_iter, verbose)


## test
if __name__=='__main__':
    import logging
    import sys
    logging.basicConfig(stream=sys.stdout,
                        format='%(levelname)s:%(message)s',
                        level=logging.INFO)
    from timeit import default_timer as timer

    from spherical_kmeans.baseline import spherical_kmeans

    import sklearn.datasets
    from datetime import datetime

    def _load_dataset():
        ds = sklearn.datasets.load_digits()
        X = ds.data.astype('float32')
        X -= X.mean(axis=1, keepdims=True)
        X /= np.linalg.norm(X, axis=1, keepdims=True)
        Y = ds.target.astype('float32')
        return X, Y

    print('### CHECK fast k-means ###')

    logging.info('loading data')
    X, Y = _load_dataset()

    logging.debug("data")
    logging.debug(X.shape)
    logging.debug(X.flags)
    logging.debug("X[0:1, 0:4] =")
    logging.debug(X[0, :4])
    logging.debug(X[1, :4])

    print('START: Running C version of spherical k-means')
    start = timer()
    results_1 = fast_kmeans(X, 10, 1000, 0)
    end = timer()
    print("END: running in {:.4f} sec".format(end - start))

    logging.debug("results fast k-means")
    logging.debug(results_1.shape)
    logging.debug(results_1.flags)
    logging.debug("Z[0:1, 0:4] =")
    logging.debug(results_1[0, :4])
    logging.debug(results_1[1, :4])


    print('START: Running python version of spherical k-means (baseline)')
    start = timer()
    results_2,_ = spherical_kmeans(X, 10, 1000)
    end = timer()
    print("END: running in {:.4f} sec".format(end - start))

    logging.debug("results Python k-means")
    logging.debug(results_2.shape)
    logging.debug(results_2.flags)
    logging.debug("Z[0:1, 0:4] =")
    logging.debug(results_2[0, :4])
    logging.debug(results_2[1, :4])

    # FIXME
    # ### comparing output
    # import matplotlib.pyplot as plt
    # import seaborn as sns
    #
    # def corr2_coeff(A,B):
    #     # Rowwise mean of input arrays & subtract from input arrays themeselves
    #     A_mA = A - A.mean(1)[:,None]
    #     B_mB = B - B.mean(1)[:,None]
    #
    #     # Sum of squares across rows
    #     ssA = (A_mA**2).sum(1)
    #     ssB = (B_mB**2).sum(1)
    #
    #     # Finally get corr coeff
    #     return np.dot(A_mA,B_mB.T)/np.sqrt(np.dot(ssA[:,None],ssB[None]))
    #
    # plt.title('fast k-means vs Python k-means')
    # sns.heatmap(corr2_coeff(results_1, results_2))
    # plt.show()
