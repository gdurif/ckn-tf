#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>
#include <numpy/npy_math.h>
#include "cblas_alt_template.h"

#include "linalg.h"

#include "ctypes_utils.h"
#include "kmeans.h"

#include <iostream>
#include <stdio.h>
using namespace std;

#define MAKE_INIT_NAME(x) init ## x (void)
#define MODNAME_INIT(s) MAKE_INIT_NAME(s)

#define STR_VALUE(arg)        #arg
#define FUNCTION_NAME(name) STR_VALUE(name)

#define MODNAME_STR FUNCTION_NAME(MODNAME)

/*
    Get the include directories within python using

    import distutils.sysconfig
    print distutils.sysconfig.get_python_inc()
    import numpy as np
    print np.get_include()

    gcc  -fPIC -shared -g -Wall -O3 \
    -I /usr/include/python2.7 -I /usr/lib64/python2.7/site-packages/numpy/core/include \
    mymath.c -o mymath.so

*/


template<typename T>
bool all_finite(const T* const x, const int n) {
    bool finite(true);
#pragma omp parallel for shared(finite)
    for (int i=0; i<n; ++i)
        if (!npy_isfinite(x[i]))
            finite = false; // we only ever write this value to finite, so no race condition
    return finite;
}

template <typename T>
PyArrayObject* new_array(vector<npy_intp> shape) {
    const int ndim = shape.size();
    PyArrayObject* result = (PyArrayObject *) PyArray_SimpleNew(ndim, shape.data(), getTypeNumber<T>());
    return result;
}


template <typename T>
static PyArrayObject* fast_kmeans_generic(PyArrayObject* input,
                                          int n_clusters,
                                          int num_iter,
                                          int verbose) {

    // input dimension
    vector<int> input_shape = get_array_shape(input);
    assert_py_obj ((PyArray_FLAGS(input) & NPY_ARRAY_C_CONTIGUOUS), "input maps are not C contiguous.");
    assert_py_obj (input_shape.size()==2, ("input should exactly have 2 dimension, but have "+to_string(input_shape.size())+".").c_str());

    int n_samples = input_shape[0];
    int n_features = input_shape[1];

    // pointer
    T* raw_input = reinterpret_cast<T*>(PyArray_DATA(input));

    // ouput
    PyArrayObject* output = new_array<T>({n_clusters, n_features});
    assert_py_obj(output != NULL, "failed to create output array");
    T* raw_output = reinterpret_cast<T*>(PyArray_DATA(output));

    // k-means
    wrapper_kmeans(raw_input, raw_output,
                   n_samples, n_features,
                   n_clusters, num_iter,
                   verbose);

    // return
    return output;

}



static PyObject* pykmeans_fast_kmeans(PyObject *self,
                                      PyObject *args,
                                      PyObject *keywds) {

    PyArrayObject* pyinput=NULL;
    int n_clusters;
    int num_iter;
    int verbose;

    int threads(-1);

    // {{{ parse inputs
    static char *kwlist[] = {
                            "input",
                            "n_clusters",
                            "num_iter",
                            "verbose",
                            NULL};

    const char* format =  "Oiii";
    if (!PyArg_ParseTupleAndKeywords(args, keywds, format, kwlist,
                                     &pyinput,
                                     &n_clusters,
                                     &num_iter,
                                     &verbose)) {
        return NULL;
    }
    // }}}

    threads = set_omp_threads(threads);

    /* actual computation */

    auto T = PyArray_TYPE(pyinput);

    if (T == getTypeNumber<float>())
        return (PyObject *) fast_kmeans_generic<float>(pyinput, n_clusters, num_iter, verbose);
    else if (T == getTypeNumber<double>())
        return (PyObject *) fast_kmeans_generic<double>(pyinput, n_clusters, num_iter, verbose);
    else {
        PyErr_SetString(PyExc_TypeError, ("Got wrong data type: "+to_string(T)).c_str()); \
        return NULL;
    }
}






static PyMethodDef method_list[] = {
          {"cfast_kmeans",  (PyCFunction)pykmeans_fast_kmeans, METH_VARARGS | METH_KEYWORDS, "Fast version of spherical k-means."},
          {NULL, NULL, 0, NULL}          /* Sentinel */
};

static struct PyModuleDef kmeansmodule = {
          PyModuleDef_HEAD_INIT,
          "_kmeans",    /* name of module */
          NULL, /* module documentation, may be NULL */
          -1,         /* size of per-interpreter state of the module,
                     or -1 if the module keeps state in global variables. */
          method_list,
          NULL//, NULL, NULL, NULL
};

PyMODINIT_FUNC
PyInit__kmeans(void) {

    PyObject* m;
    m = PyModule_Create(&kmeansmodule);
    assert_py_obj(m!=NULL, "failed to create kmeans module object");

    // initialize wrapper classes
    MatrixWrapperType.tp_new = PyType_GenericNew;
    VectorWrapperType.tp_new = PyType_GenericNew;
    MapWrapperType.tp_new = PyType_GenericNew;
    assert_py_obj(PyType_Ready(&MapWrapperType) >= 0,
                      "Map wrapper type failed to initialize");
    assert_py_obj(PyType_Ready(&MatrixWrapperType) >= 0,
                      "Matrix wrapper type failed to initialize");
    assert_py_obj(PyType_Ready(&VectorWrapperType) >= 0,
                      "Vector wrapper type failed to initialize");

    /* required, otherwise numpy functions do not work */
    import_array();

    Py_INCREF(&MatrixWrapperType);
    Py_INCREF(&MapWrapperType);
    Py_INCREF(&VectorWrapperType);
    PyModule_AddObject(m, "MyDealloc_Type_Mat", (PyObject *)&MatrixWrapperType);
    PyModule_AddObject(m, "MyDealloc_Type_Map", (PyObject *)&MapWrapperType);
    PyModule_AddObject(m, "MyDealloc_Type_Vec", (PyObject *)&VectorWrapperType);

    return m;
}
