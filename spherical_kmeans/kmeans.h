#ifndef FAST_KMEANS_H
#define FAST_KMEANS_H

#include "linalg.h"
#include <iostream>


template <typename T>
void fast_kmeans(const Matrix<T>& X, Matrix<T>& Z, const int num_iter = 10, const int verbose=0) {
    const int n=X.n();
    const int p=Z.n();
    if (num_iter >=0) {
        Vector<int> per;
        per.randperm(n);
        Vector<T> col1, col2;
        for (int ii=0; ii<p; ++ii) {
            X.refCol(per[ii],col1);
            Z.refCol(ii,col2);
            col2.copy(col1);
        }
    } else {
        Z.setAleat();
        Z.normalize();
    }

    for (int ii=0; ii<num_iter; ++ii) {
        if(verbose)
            printf("K-means epoch %d\n",ii+1);
        const int size_block=p;
        Vector<T> tmp(n);
        Vector<int> idx(n);
#pragma omp parallel for
        for (int jj =0; jj<n; jj+=size_block) {
            const int endblock=MIN(jj+size_block,n);
            const int length_block=endblock-jj;
            Matrix<T> subX, ZtX;
            X.refSubMat(jj,length_block,subX);
            Z.mult(subX,ZtX,true);
            Vector<T> col;
            for (int kk=0; kk<length_block; ++kk) {
                ZtX.refCol(kk,col);
                idx[jj+kk]=col.max();
                tmp[jj+kk]=col[idx[jj+kk]];
            }
        }
        Vector<int> numElem(p);
        numElem.setZeros();
        Z.setZeros();
        Vector<T> col1, col2;
        for (int jj =0; jj<n; ++jj) {
            const int ind=idx[jj];
            numElem[ind]++;
            X.refCol(jj,col1);
            Z.refCol(ind,col2);
            col2.add(col1);
        }
        for (int jj =0; jj<p; ++jj) {
            Z.refCol(jj,col1);
            if (numElem[jj]) {
                col1.normalize();
            } else {
                const int ind=tmp.min();
                tmp[ind]=1;
                X.refCol(ind,col2);
                col1.copy(col2);
            }
        }
    }
};


template <typename T>
void wrapper_kmeans(T* X, T* Z,
                    const int n_samples,
                    const int n_features,
                    const int n_clusters,
                    const int num_iter,
                    const int verbose) {
    Matrix<T> X_mat(X, n_features, n_samples);
    Matrix<T> Z_mat(Z, n_features, n_clusters);

    X_mat.normalize();

    // printf("\n");
    // printf("input kmeans\n\n");
    // printf("X[0,0:4] = %f, %f, %f, %f, %f \n", X[0], X[1], X[2], X[3], X[4]);
    // printf("X[1,0:4] = %f, %f, %f, %f, %f \n\n", X[0+n_features], X[1+n_features],
    //                                            X[2+n_features], X[3+n_features],
    //                                            X[4+n_features]);
    // printf("X[0,0:4] = %f, %f, %f, %f, %f \n", X_mat[0], X_mat[1], X_mat[2], X_mat[3], X_mat[4]);
    // printf("X[1,0:4] = %f, %f, %f, %f, %f \n\n", X_mat[0+n_features], X_mat[1+n_features],
    //                                            X_mat[2+n_features], X_mat[3+n_features],
    //                                            X_mat[4+n_features]);
    // printf("X[0,0:4] = %f, %f, %f, %f, %f \n", X_mat(0,0), X_mat(1,0), X_mat(2,0), X_mat(3,0), X_mat(4,0));
    // printf("X[1,0:4] = %f, %f, %f, %f, %f \n\n", X_mat(0,1), X_mat(1,1),
    //                                           X_mat(2,1), X_mat(3,1),
    //                                           X_mat(4,1));
    // std::cout << std::endl; // flush stdout

    fast_kmeans(X_mat, Z_mat, num_iter, verbose);


    // printf("\n");
    // printf("results kmeans\n\n");
    // printf("Z[0,0:4] = %f, %f, %f, %f, %f \n", Z[0], Z[1], Z[2], Z[3], Z[4]);
    // printf("Z[1,0:4] = %f, %f, %f, %f, %f \n\n", Z[0+n_features], Z[1+n_features],
    //                                            Z[2+n_features], Z[3+n_features],
    //                                            Z[4+n_features]);
    // printf("Z[0,0:4] = %f, %f, %f, %f, %f \n", Z_mat[0], Z_mat[1], Z_mat[2], Z_mat[3], Z_mat[4]);
    // printf("Z[1,0:4] = %f, %f, %f, %f, %f \n\n", Z_mat[0+n_features], Z_mat[1+n_features],
    //                                            Z_mat[2+n_features], Z_mat[3+n_features],
    //                                            Z_mat[4+n_features]);
    // printf("Z[0,0:4] = %f, %f, %f, %f, %f \n", Z_mat(0,0), Z_mat(1,0), Z_mat(2,0), Z_mat(3,0), Z_mat(4,0));
    // printf("Z[1,0:4] = %f, %f, %f, %f, %f \n\n", Z_mat(0,1), Z_mat(1,1),
    //                                           Z_mat(2,1), Z_mat(3,1),
    //                                           Z_mat(4,1));
    std::cout << std::endl; // flush stdout

};


#endif // FAST_KMEANS_H
