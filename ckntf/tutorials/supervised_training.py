"""Tutorial: training a CKN model in supervised mode combined to MISO SVM classification"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
                "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import logging
import os
import sys

from timeit import default_timer as timer
from pprint import pprint

import pkg_resources

import tensorflow as tf
import numpy as np
import ckntf.core
import ckntf.input



def ckn_model_training_supervised(args, params):
    """Script: training a CKN model in supervised mode combined with
    a MISO SVM classification.

    Step:
        1) Parameter definition:
            - Object 'args': general characteristics, data file, output
            directory for tensorboard
            - Object 'params': parameters of the single layer

        2) Import of the whitened Cifar-10 data set

        3) Initialization: unsupervised training of the filters

        4) Supervised training of the filters

    Args:
        args: object with the following attributes
            'sample_size' (int): number of images to consider from the
                CIFAR-10 data test set (max is 10000). The train set will
                be 5 times the test set size.
            'batch_size' (int): size of each batch of images for the
                training (large batches require a lot of memory).
            'data_file' (string): CIFAR-10 data file name.
            'online_whitening' (bool): indicating if image whitening should
                be performed online during the training or not. If False, it is
                recommended to use the pre-whitened images.
            'output_dir' (string): directory where to store the TensorBoard
                metadata.
            'l2_reg' (float): regularization parameter for classification layer.
            'learning_rate' (float): learning rate for TensorFlow optimizer.
            'tag' (string): a special tag to identify Tensorboard metadata.
                If None, only a timestamp is used.

        params: object or list of dictionary with attributes/keywords
            'filter_height' (int): filter dimension (height) in pixels.
            'filter_width' (int): filter dimension (width) in pixels.
            'out_channels' (int): output dimension of the layer, i.e. number of
                filters.
            'subsampling' (int): sampling factor in linear pooling.
            'n_sampled_patches' (int): number of patches sampled across all
                images for the spherical k-means initialization.
            'kernel' (string): non-linearity function name.
            'kernel_param' (float or double): hyper-parameters for the
                non-linearity function.
            'dtype' (tf.dtype): type of the data (tf.float32, etc.).
            'padding' (string): type of padding, "SAME" = 0-pading,
                "VALID" = no padding.
            'name' (string): name of the layer.
            'epsilon_norm' (float or double): regularization parameter for
                normalization.
            'epsilon_inv' (float or double): regularization parameter for
                inversion.

    """

    print("Tutorial: a single CKN layer model in supervised framework "
            + "with MISO SVM classification")

    print("Parameters:")
    pprint(vars(args))
    pprint(params)

    ## for tensorboard
    print("For tensorboard:\n    run `tensorboard --logdir="
          + args.output_dir + "` in a shell"
          + "\n    and go to"
          + "`localhost:6006` on your web-browser")

    ### import data
    logging.info("importing data")
    (Xtr, Ytr,
     Xte, Yte) = ckntf.input.load_cifar10(args.data_folder,
                                        args.sample_size,
                                        whitened=not args.online_whitening)
    (N, H, W, C) = Xtr.shape
    n_class = len(set(Ytr))
    logging.info("data imported")

    # data spec
    logging.info("dimensions of the training data set ({})".format(Xtr.shape))
    logging.info("dimensions of the test data set ({})".format(Xte.shape))

    ### run tensorflow
    sess = tf.Session()

    ####### Profiling Options
    run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE,
                                output_partition_graphs=True)
    # define the model
    model = ckntf.core.CKNmodel.from_param(params, sess, args.batch_size,
                                         args.online_whitening)

    # encode the model
    print("#### encode the model ####")
    model.encode(H, W, C)
    model.encode_supervised(n_class, loss_type='squared_hinge',
                            l2_reg=args.l2_reg)
    model.encode_optim(sample_size=Xtr.shape[0],
                       learning_rate=args.learning_rate)

    # initialize tf variables
    model.initialize_variables()

    ##### initialization of the filters
    print("#### initialization (unsupervised training) ####")

    ## training of the model
    logging.info("START: initialization (unsupervised training) of the {} layer(s) "
                    .format(len(model.layer_list)) \
                    + "in the model with k-means")
    start = timer()
    model.initialize_filters(input_data=Xtr[0:500],
                             output_dir=args.output_dir,
                             n_iter_kmeans=0)
    end = timer()
    logging.info("### END: initialization of the {} layers in {:.4f} sec"
                    .format(len(model.layer_list), end - start))

    ##### supervised training
    print("#### supervised training ####")

    logging.info("### START: supervised training of the {} layer(s) "
                    .format(len(model.layer_list)))
    start = timer()
    model.sup_train(output_dir=args.output_dir,
                    input_data=Xtr,
                    input_labels=Ytr,
                    test_data=Xte,
                    test_labels=Yte,
                    n_iter=110 * Xtr.shape[0] // args.batch_size,
                    check_point=args.check_point,
                    batch_size_eval=200,
                    alternate_miso=False,
                    tag=args.tag)
    end = timer()
    logging.info("### END: training of the {} layers in {:.4f} sec"
                    .format(len(model.layer_list), end - start))

    print("#### tutorial is over ####")
