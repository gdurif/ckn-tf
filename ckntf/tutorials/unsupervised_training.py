"""Tutorial: training a CKN model in unsupervised mode followed by MISO SVM classification"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
                "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import logging
import os
import sys

from timeit import default_timer as timer
from pprint import pprint

import pkg_resources

import tensorflow as tf
import numpy as np
import ckntf.core
import ckntf.input
import miso_svm



def ckn_model_training_unsupervised(args, params):
    """Script: training a CKN model in unsupervised mode followed by
    a MISO SVM classification.

    Step:
        1) Parameter definition:
            - Object 'args': general characteristics, data file, output
            directory for tensorboard
            - Object 'params': parameters of the single layer

        2) Import of the whitened Cifar-10 data set

        3) Unsupervised training of the filters

        4) Compute the output of the model (i.e. transformed images)

        5) Classification of the images based on the output of the model

    Args:
        args: object with the following attributes
            'sample_size' (int): number of images to consider from the
                CIFAR-10 data test set (max is 10000). The train set will
                be 5 times the test set size.
            'batch_size' (int): size of each batch of images for the
                training (large batches require a lot of memory).
            'data_folder' (string): directory where the 'cifar10_whitened.pkl'
                file and the 'cifar10_batches' directory are stored.
            'online_whitening' (bool): indicating if image whitening should
                be performed online during the training or not. If False, it is
                recommended to use the pre-whitened images.
            'output_dir' (string): directory where to store the TensorBoard
                metadata.

        params: object or list of dictionary with attributes/keywords
            'filter_height' (int): filter dimension (height) in pixels.
            'filter_width' (int): filter dimension (width) in pixels.
            'out_channels' (int): output dimension of the layer, i.e. number of
                filters.
            'subsampling' (int): sampling factor in linear pooling.
            'n_sampled_patches' (int): number of patches sampled across all
                images for the spherical k-means initialization.
            'kernel' (string): non-linearity function name.
            'kernel_param' (float or double): hyper-parameters for the
                non-linearity function.
            'dtype' (tf.dtype): type of the data (tf.float32, etc.).
            'padding' (string): type of padding, "SAME" = 0-pading,
                "VALID" = no padding.
            'name' (string): name of the layer.
            'epsilon_norm' (float or double): regularization parameter for
                normalization.
            'epsilon_inv' (float or double): regularization parameter for
                inversion.

    """

    print("Tutorial: a single CKN layer model in unsupervised framework "
            + "with MISO SVM classification")

    print("Parameters:")
    pprint(vars(args))
    pprint(params)

    ## for tensorboard
    print("For tensorboard:\n    run `tensorboard --logdir="
          + args.output_dir + "` in a shell"
          + "\n    and go to"
          + "`localhost:6006` on your web-browser")

    ### import data
    logging.info("importing data")
    (Xtr, Ytr,
     Xte, Yte) = ckntf.input.load_cifar10(args.data_folder,
                                        args.sample_size,
                                        whitened=not args.online_whitening)
    (N, H, W, C) = Xtr.shape
    logging.info("data imported")

    # data spec
    logging.info("dimensions of the training data set ({})".format(Xtr.shape))
    logging.info("dimensions of the test data set ({})".format(Xte.shape))

    ### run tensorflow
    sess = tf.Session()

    ####### Profiling Options
    run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE,
                                output_partition_graphs=True)
    # define the model
    model = ckntf.core.CKNmodel.from_param(params, sess, args.batch_size,
                                         args.online_whitening)

    # encode the model
    print("#### encode the model ####")
    model.encode(H, W, C)

    ##### unsupervised training of the filters
    print("#### unsupervised training ####")

    ## training of the model
    logging.info("START: unsupervised training of the {} layer(s) "
                    .format(len(model.layer_list)) \
                    + "in the model with k-means")
    start = timer()
    model.unsup_train(input_data=Xtr)
    end = timer()
    logging.info("### END: training of the {} layers in {:.4f} sec"
                    .format(len(model.layer_list), end - start))

    ## get output of the model in training and test sets
    logging.info("START: get output (training and test maps) of the model")
    start = timer()
    features_tr = model.get_output(Xtr)
    end = timer()
    logging.info("### END1: compute 'features_tr' in {:.4f} sec"
                    .format(end - start))
    start = timer()
    features_te = model.get_output(Xte)
    end = timer()
    logging.info("### END2: compute 'features_te' in {:.4f} sec"
                    .format(end - start))

    logging.debug("training output map dimension: {}".format(features_tr.shape))
    logging.debug("training output map type: {}".format(type(features_tr)))
    logging.debug("test output map dimension: {}".format(features_te.shape))
    logging.debug("test output map type: {}".format(type(features_te)))

    ## tensorboard visualization
    logging.info("save metadata for tensorboard visualization")
    writer = tf.summary.FileWriter(args.output_dir, sess.graph)
    writer.close()

    labels_tr = Ytr
    labels_te = Yte

    ## CLASSIFICATION
    print("#### classification step ####")
    start = timer()
    acc,lamb = miso_svm.quick(features_tr.reshape(Xtr.shape[0], -1),
                              features_te.reshape(Xte.shape[0], -1),
                              labels_tr, labels_te,
                              eps=1e-4, threads=16, start_exp=-15, end_exp=15,
                              add_iter=3, accelerated=True,
                              verbose=False, seed=None)
    end = timer()
    logging.info("### END: miso in {:.4f} sec".format(end - start))
    print("### Best accuracy = {:.4%} for Lambda = {}\n"
                .format(acc, lamb))

    print("#### tutorial is over ####")
