"""Tutorial: training a CKN model in unsupervised mode followed by MISO SVM classification"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
                "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import logging
import os
import sys

import pkg_resources
import tensorflow as tf

import ckntf.core
import ckntf.tutorials


### EXAMPLES
class args:
    ## data
    sample_size = 10000
    batch_size = 128
    data_folder = pkg_resources.resource_filename('ckntf', 'data')
    ## online whitening
    online_whitening = True
    ## tensorboard
    output_dir = 'unsupervised_mode'


def tuto_1layer():
    """Run the function `ckn_model_training_unsupervised` with the following
    architecture, a single-layer model:

        filter_height = 3
        filter_width = 3
        out_channels = 64
        subsampling = 2
        n_sampled_patches = 1000000
        kernel = 'exp0'
        kernel_param = 0.6
        dtype = tf.float32
        padding = "SAME"
        name = "layer1"
        epsilon_norm = 1e-5
        epsilon_inv = 1e-3

    """

    args.output_dir = 'unsupervised_mode_single_layer_model'

    ## CKN model parameters
    class params:
        filter_height = 3
        filter_width = 3
        out_channels = 64
        subsampling = 2
        n_sampled_patches = 1000000
        kernel = 'exp0'
        kernel_param = 0.6
        dtype = tf.float32
        padding = "SAME"
        name = "layer1"
        epsilon_norm = 1e-5
        epsilon_inv = 1e-3

    ## run script
    ckntf.tutorials.ckn_model_training_unsupervised(args, params)


def tuto_2layers():
    """Run the function `ckn_model_training_unsupervised` with the following
    architecture, a two-layer model:

    Layer 1:
        filter_height = 3
        filter_width = 3
        out_channels = 64
        subsampling = 2
        n_sampled_patches = 1000000
        kernel = 'exp0'
        kernel_param = 0.6
        dtype = tf.float32
        padding = "SAME"
        name = "layer1"
        epsilon_norm = 1e-5
        epsilon_inv = 1e-3

    Layer 2:
        filter_height = 3
        filter_width = 3
        out_channels = 256
        subsampling = 6
        n_sampled_patches = 1000000
        kernel = 'exp0'
        kernel_param = 0.6
        dtype = tf.float32
        padding = "SAME"
        name = "layer2"
        epsilon_norm = 1e-5
        epsilon_inv = 1e-3

    """

    args.output_dir = 'unsupervised_mode_two_layer_model'

    ## CKN model parameters
    params = [
        {
            'filter_height': 3, 'filter_width': 3,
            'out_channels': 64, 'subsampling': 2,
            'n_sampled_patches': 1000000,
            'kernel': 'exp0', 'kernel_param': 0.6,
            'dtype': tf.float32, 'padding': "SAME", 'name': "layer1",
            'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
        },
        {
            'filter_height': 2, 'filter_width': 2,
            'out_channels': 256, 'subsampling': 6,
            'n_sampled_patches': 1000000,
            'kernel': 'exp0', 'kernel_param': 0.6,
            'dtype': tf.float32, 'padding': "SAME", 'name': "layer2",
            'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
        }
    ]

    ## run script
    ckntf.tutorials.ckn_model_training_unsupervised(args, params)


def tuto_2layers_large():
    """Run the function `ckn_model_training_unsupervised` with the following
    architecture, a two-layer model with large filters:

    Layer 1:
        filter_height = 3
        filter_width = 3
        out_channels = 256
        subsampling = 2
        n_sampled_patches = 1000000
        kernel = 'exp0'
        kernel_param = 0.6
        dtype = tf.float32
        padding = "SAME"
        name = "layer1"
        epsilon_norm = 1e-5
        epsilon_inv = 1e-3

    Layer 2:
        filter_height = 2
        filter_width = 2
        out_channels = 1024
        subsampling = 6
        n_sampled_patches = 1000000
        kernel = 'exp0'
        kernel_param = 0.6
        dtype = tf.float32
        padding = "SAME"
        name = "layer2"
        epsilon_norm = 1e-5
        epsilon_inv = 1e-3

    """

    args.output_dir = 'unsupervised_mode_two_layer_model'

    ## CKN model parameters
    params = [
        {
            'filter_height': 3, 'filter_width': 3,
            'out_channels': 512, 'subsampling': 2,
            'n_sampled_patches': 1000000,
            'kernel': 'exp0', 'kernel_param': 0.6,
            'dtype': tf.float32, 'padding': "SAME", 'name': "layer1",
            'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
        },
        {
            'filter_height': 3, 'filter_width': 3,
            'out_channels': 4096, 'subsampling': 6,
            'n_sampled_patches': 1000000,
            'kernel': 'exp0', 'kernel_param': 0.6,
            'dtype': tf.float32, 'padding': "SAME", 'name': "layer2",
            'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
        }
    ]

    ## run script
    ckntf.tutorials.ckn_model_training_unsupervised(args, params)




if __name__ == "__main__":

    logging.basicConfig(stream=sys.stdout,
                        format='%(levelname)s:%(message)s',
                        level=logging.INFO)

    # tuto_2layers_large()
    tuto_1layer()
