"""Tutorial: training a CKN model in supervised mode combined to MISO SVM classification"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
                "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import logging
import os
import sys

import pkg_resources
import tensorflow as tf

import ckntf.core
import ckntf.tutorials

### EXAMPLES
class args:
    ## data
    sample_size = 10000
    batch_size = 128
    data_folder = pkg_resources.resource_filename('ckntf', 'data')
    ## check pointing
    check_point = 5000
    ## online whitening
    online_whitening = False
    ## tensorboard
    output_dir = 'supervised_mode'
    ## regularization parameter
    l2_reg = 1e-2
    ## learning rate
    learning_rate = 10
    ## tag
    tag = "experiment0"


def tuto_1layer():
    """Run the function `ckn_model_training_supervised` with the following
    architecture, a single-layer model:

        filter_height = 3
        filter_width = 3
        out_channels = 64
        subsampling = 2
        n_sampled_patches = 1000000
        kernel = 'exp0'
        kernel_param = 0.6
        dtype = tf.float32
        padding = "SAME"
        name = "layer1"
        epsilon_norm = 1e-5
        epsilon_inv = 1e-3

    """

    ## CKN model parameters
    params = [
        {
            'filter_height': 3, 'filter_width': 3,
            'out_channels': 64, 'subsampling': 2,
            'n_sampled_patches': 1000000,
            'kernel': 'exp0', 'kernel_param': 0.6,
            'dtype': tf.float32, 'padding': "SAME", 'name': "layer1",
            'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
        }
    ]

    ## run script
    ckntf.tutorials.ckn_model_training_supervised(args, params)


def tuto_2layers():
    """Run the function `ckn_model_training_supervised` with the following
    architecture, a two-layer model:

    Layer 1:
        filter_height = 3
        filter_width = 3
        out_channels = 64
        subsampling = 2
        n_sampled_patches = 1000000
        kernel = 'exp0'
        kernel_param = 0.6
        dtype = tf.float32
        padding = "SAME"
        name = "layer1"
        epsilon_norm = 1e-5
        epsilon_inv = 1e-3

    Layer 2:
        filter_height = 2
        filter_width = 2
        out_channels = 256
        subsampling = 6
        n_sampled_patches = 1000000
        kernel = 'exp0'
        kernel_param = 0.6
        dtype = tf.float32
        padding = "SAME"
        name = "layer2"
        epsilon_norm = 1e-5
        epsilon_inv = 1e-3

    """

    ## CKN model parameters
    params = [
        {
            'filter_height': 3, 'filter_width': 3,
            'out_channels': 64, 'subsampling': 2,
            'n_sampled_patches': 1000000,
            'kernel': 'exp0', 'kernel_param': 0.6,
            'dtype': tf.float32, 'padding': "SAME", 'name': "layer1",
            'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
        },
        {
            'filter_height': 2, 'filter_width': 2,
            'out_channels': 256, 'subsampling': 6,
            'n_sampled_patches': 1000000,
            'kernel': 'exp0', 'kernel_param': 0.6,
            'dtype': tf.float32, 'padding': "SAME", 'name': "layer2",
            'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
        }
    ]

    ## run script
    ckntf.tutorials.ckn_model_training_supervised(args, params)


def tuto_5layers():
    """Run the function `ckn_model_training_supervised` with the following
    architecture, a model with 5 layers:

    filter_dim=[3, 1, 3, 1, 3]
    out_channels=[128, 128, 128, 128, 128]
    subsampling=[2, 1, 2, 1, 3]
    kernel=['exp0', 'poly02', 'exp0', 'poly02', 'exp0']
    kernel_param=[0.5, 2, 0.5, 2, 0.5]
    name=["layer1", ... , "layer5"]
    n_sampled_patches=1000
    dtype=tf.float32
    padding="SAME"
    epsilon_norm=1e-5
    epsilon_inv=1e-2

    """

    ## CKN model parameters
    params = ckntf.core.utils.format_params(
                filter_dim=[3, 1, 3, 1, 3],
                out_channels=[128, 128, 128, 128, 128],
                subsampling=[2, 1, 2, 1, 3],
                kernel=['exp0', 'poly02', 'exp0', 'poly02', 'exp0'],
                kernel_param=[0.5, 2, 0.5, 2, 0.5],
                name=["layer{}".format(ind+1) for ind in range(5)],
                n_sampled_patches=1000,
                dtype=tf.float32, padding="SAME",
                epsilon_norm=1e-5, epsilon_inv=1e-2)

    args.l2_reg = 0.1

    ## run script
    ckntf.tutorials.ckn_model_training_supervised(args, params)


def tuto_14layers():
    """Run the function `ckn_model_training_supervised` with the following
    architecture, a deep model with 14 layers:

    filter_dim=[3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1]
    out_channels=[256, 128, 256, 128, 256, 256, 256, 256, 256, 256, 256, 256, 256, 256]
    subsampling=[1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 2]
    kernel=['exp0', 'poly2', 'exp0', 'poly2', 'exp0', 'poly2', 'exp0', 'poly2',
            'exp0', 'poly2', 'exp0', 'poly2', 'exp0', 'poly2']
    kernel_param=[0.5, 2, 0.5, 2, 0.5, 2, 0.5, 2, 0.5, 2, 0.5, 2, 0.5, 2]
    name=["layer1", ... , "layer14"]
    n_sampled_patches=1000000
    dtype=tf.float32
    padding="SAME"
    epsilon_norm=1e-5
    epsilon_inv=1e-5

    """

    ## CKN model parameters
    params = ckntf.core.utils.format_params(
                filter_dim=[3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1],
                out_channels=[256, 128, 256, 128, 256, 256, 256, 256, 256, 256, 256, 256, 256, 256],
                subsampling=[1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 2],
                kernel=['exp0', 'poly2', 'exp0', 'poly2', 'exp0', 'poly2', 'exp0', 'poly2',
                        'exp0', 'poly2', 'exp0', 'poly2', 'exp0', 'poly2'],
                kernel_param=[0.5, 2, 0.5, 2, 0.5, 2, 0.5, 2, 0.5, 2, 0.5, 2, 0.5, 2],
                name=["layer{}".format(ind+1) for ind in range(14)],
                n_sampled_patches=1000000,
                dtype=tf.float32, padding="SAME",
                epsilon_norm=1e-5, epsilon_inv=1e-5)

    args.l2_reg = 0.1
    args.learning_rate = 50

    ## run script
    ckntf.tutorials.ckn_model_training_supervised(args, params)


 
if __name__ == "__main__":

    logging.basicConfig(stream=sys.stdout,
                        format='%(levelname)s:%(message)s',
                        level=logging.INFO)

    ### debug option
    ckntf.core.ckn_layer.DEBUG = False
    ckntf.core.ckn_model.DEBUG = False

    tuto_5layers()
