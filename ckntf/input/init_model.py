"""load a network structure for a CKN model"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Ghislain Durif", "Daan Wynene"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"


import json
import logging

import numpy as np

import ckntf.core
from ckntf.core.utils import obj

def read_model(file_name):
    """read a CKN network structure stored in a json file

    Example of structure in file 'file_name'
    {
        "input_file": "matlab_whitened.pkl",
        "layer_file": "example_config_2layers.json",
        "output_file": "results.dat",
        "output_dir": "output/",
        "batch_size": 100
    }

    The 'layer_file' should contain a list of layer parameters.

    Args:
        file_name (string): name of the json file where general
            properties of the network are stored.

    """
    with open(file_name) as f:
        model = json.load(f)

    model = obj(model)

    return model


def init_model(model, H, W, C, session):
    """init an object of type CKNmodel based on a 'NetworkStructure' object

    Args:
        model (object): a model structure returned by 'read_model'.
        H (int): image height.
        W (int): image width.
        C (int): number of channels in input.
        session (Tensorflow session): Tensorflow session where the
            computations will be done.

    """

    ## define the layers
    logging.debug("Definition of the layers in the model")

    with open(model.layer_file) as f:
        param_list = json.load(f)

    ## model
    model = ckntf.core.CKNmodel.from_param(param_list, session, model.batch_size)

    ## encode the layers
    logging.debug("Encoding of the {} layers in the model"
                    .format(len(param_list)))
    model.encode(H, W, C)

    ## return the model
    return model
