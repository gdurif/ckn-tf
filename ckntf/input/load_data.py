"""load data set for CKN tests"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Alberto Bietti, Ghislain Durif, Daan Wynen, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Ghislain Durif", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import os
import pickle
import warnings

import numpy as np

def unpickle_data(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict


def pickle_data(obj, file):
    with open(file, 'wb') as fo:
        pickle.dump(obj, fo)


def read_dataset_cifar10(folder):
    """read the pickle CIFAR10 data set

    The dataset is divided into 5 files provided by
    <https://www.cs.toronto.edu/~kriz/cifar.html>.
    Images and labels are each returned in one numpy array for training
    and one for testing.

    Args:
        folder (string): folder where the dataset is stored.

    Return:
        a tuple with training and test sets, respectively images and labels
        Xtr, Xte and Ytr, Yte.

    """

    sample_size = 10000
    n_train = 5*sample_size
    n_test = sample_size

    Xtr = np.empty((n_train, 96, 32), dtype=np.float32)
    Ytr = np.empty(n_train, dtype=np.float32)
    for i in range(1, 6):
        d = unpickle_data(os.path.join(folder, 'data_batch_{}'.format(i)))
        Xtr[(i-1)*sample_size:i*sample_size] = d[b'data'].reshape(sample_size, 96, 32)/255.0
        Ytr[(i-1)*sample_size:i*sample_size] = d[b'labels']

    d = unpickle_data(os.path.join(folder, 'test_batch'))
    Xte = np.ascontiguousarray(d[b'data'].astype(np.float32).reshape(sample_size, 96, 32))/255.0
    Yte = np.array(d[b'labels'], dtype=np.float32)
    return Xtr.reshape(-1,3,32,32).transpose(0,2,3,1), Ytr, \
           Xte.reshape(-1,3,32,32).transpose(0,2,3,1), Yte


def read_whitened_dataset_cifar10(file):
    """read the pickle pre-whitened CIFAR10 data set

    Images and labels are each returned in one numpy array for training
    and one for testing.

    Images were whitened in a preliminary offline whitening step.

    Args:
        file (string): file where the data are stored.

    Return:
        a tuple with training and test sets, respectively images and labels
        Xtr, Xte and Ytr, Yte.

    """

    (Xtr, Ytr, Xte, Yte) = unpickle_data(file)
    return Xtr, Ytr, Xte, Yte


def load_cifar10(path, sample_size, whitened=True):
    """load cifar10 data set

    Extract sample_size*5 batches from train set and sample_size from test set

    Args:
        path (string): path to the directory where the 'cifar10_whitened.pkl'
            file and the 'cifar10_batches' directory are stored.
        sample_size (int): the number of images that is returned, in [1:10000].
        whitened (bool): indicates if the returned data set was whitened
            in a preliminary step.

    """

    if whitened:
        (Xtr, Ytr, Xte, Yte) = read_whitened_dataset_cifar10(
                                    file=os.path.join(path,
                                                      "cifar10_whitened.pkl")
                                    )

    else:
        (Xtr, Ytr, Xte, Yte) = read_dataset_cifar10(
                                    folder=os.path.join(path,
                                                        "cifar10_batches")
                                    )


    n_train = 5*sample_size
    n_test = sample_size

    if(n_train > Xtr.shape[0]):
        warnings.warn("sample_size is higher than the actual number of batches in cifar10 train set (50000)", UserWarning)
        n_train = Xtr.shape[0]

    if(n_test > Xte.shape[0]):
        warnings.warn("sample_size is higher than the actual number of batches in cifar10 test set (10000)", UserWarning)
        n_train = Xtr.shape[0]


    (Xtr, Ytr, Xte, Yte) = (Xtr[:n_train], Ytr[:n_train], Xte[:n_test], Yte[:n_test])

    return Xtr, Ytr, Xte, Yte
