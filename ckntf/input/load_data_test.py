"""tests on functions to load data set for CKN tests"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"


import logging
import sys
logging.basicConfig(stream=sys.stdout,
                    format='%(levelname)s:%(message)s',
                    level=logging.DEBUG)
import pkg_resources
import unittest
import matplotlib.pyplot as plt

import ckntf.input

class args:
    sample_size = 10000
    ## data
    data_file = pkg_resources.resource_filename('ckntf',
                                                'data/cifar10_whitened.pkl')
    data_path = pkg_resources.resource_filename('ckntf',
                                                'data/cifar10_batches')

    folder = pkg_resources.resource_filename('ckntf', 'data')

class LoadDataTest(unittest.TestCase):

    def test_read_dataset_cifar10(self):
        logging.info("testing `read_dataset_cifar10`")
        Xtr, \
        Ytr, \
        Xte, \
        Yte = ckntf.input.read_dataset_cifar10(args.data_path)

        self.assertEqual(Xtr.shape, (50000, 32, 32, 3))
        self.assertEqual(Xte.shape, (10000, 32, 32, 3))
        self.assertEqual(Ytr.shape, (50000,))
        self.assertEqual(Yte.shape, (10000,))

        plt.imshow(Xtr[1])
        plt.show()

    def test_read_whitened_dataset_cifar10(self):
        logging.info("testing `read_whitened_dataset_cifar10`")
        Xtr, \
        Ytr, \
        Xte, \
        Yte = ckntf.input.read_whitened_dataset_cifar10(args.data_file)

        self.assertEqual(Xtr.shape, (args.sample_size*5, 32, 32, 3))
        self.assertEqual(Xte.shape, (args.sample_size, 32, 32, 3))
        self.assertEqual(Ytr.shape, (args.sample_size*5,))
        self.assertEqual(Yte.shape, (args.sample_size,))

        plt.imshow(Xtr[1]/0.4 + 0.2)
        plt.show()

    def test_load_cifar10(self):
        logging.info("testing `load_cifar10`")
        logging.info("pre-whitened images")
        Xtr, \
        Ytr, \
        Xte, \
        Yte = ckntf.input.load_cifar10(args.folder,
                                     args.sample_size,
                                     whitened=True)

        self.assertEqual(Xtr.shape, (args.sample_size*5, 32, 32, 3))
        self.assertEqual(Xte.shape, (args.sample_size, 32, 32, 3))
        self.assertEqual(Ytr.shape, (args.sample_size*5,))
        self.assertEqual(Yte.shape, (args.sample_size,))

        plt.imshow(Xtr[1]/0.4 + 0.2)
        plt.show()

        logging.info("standard images")
        Xtr, \
        Ytr, \
        Xte, \
        Yte = ckntf.input.load_cifar10(args.folder,
                                     args.sample_size,
                                     whitened=False)

        self.assertEqual(Xtr.shape, (args.sample_size*5, 32, 32, 3))
        self.assertEqual(Xte.shape, (args.sample_size, 32, 32, 3))
        self.assertEqual(Ytr.shape, (args.sample_size*5,))
        self.assertEqual(Yte.shape, (args.sample_size,))

        plt.imshow(Xtr[1])
        plt.show()


if __name__ == "__main__":
    unittest.main()
