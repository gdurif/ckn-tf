"""tests on functions to load data set for CKN tests"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"


import logging
import sys
logging.basicConfig(stream=sys.stdout,
                    format='%(levelname)s:%(message)s',
                    level=logging.DEBUG)
import pkg_resources
import unittest
import matplotlib.pyplot as plt

from timeit import default_timer as timer

import numpy as np

import ckntf.input
from ckntf.input.transform_data import whiten_images

class args:
    sample_size = 1
    ## data
    folder = pkg_resources.resource_filename('ckntf', 'data')

class TransformDataTest(unittest.TestCase):

    def test_whiten_images(self):
        logging.info("testing `whiten_images`")

        logging.info("loading pre-whitened images")
        Xw, _,_,_ = ckntf.input.load_cifar10(args.folder,
                                           args.sample_size,
                                           whitened=True)

        logging.info("loading standard images")
        X, _,_,_ = ckntf.input.load_cifar10(args.folder,
                                          args.sample_size,
                                          whitened=False)

        X0 = np.copy(X)

        X1 = np.copy(X)
        X2 = np.copy(X)
        X3 = np.copy(X)
        X4 = np.copy(X)

        use_C = False
        logging.info("START: whitening step without reconstruction")
        start = timer()
        X1 = whiten_images(X1, 3, 3, reconstruct_image=False, use_C=use_C)
        end = timer()
        logging.info("END: whitening without reconstruction done in {:.4f} sec"
                        .format(end - start))
        logging.info("Output shape = {}".format(X1.shape))

        use_C = False
        logging.info("START: whitening step with 0-padding without reconstruction")
        start = timer()
        X4 = whiten_images(X4, 3, 3, padding="SAME",
                           reconstruct_image=False, use_C=use_C)
        end = timer()
        logging.info("END: whitening with 0-padding without reconstruction done in {:.4f} sec"
                        .format(end - start))
        logging.info("Output shape = {}".format(X4.shape))


        use_C = False
        logging.info("START: whitening step with reconstruction (Python version)")
        start = timer()
        whiten_images(X, 3, 3, reconstruct_image=True, use_C=use_C)
        end = timer()
        logging.info("END: whitening with reconstruction done in {:.4f} sec"
                        .format(end - start))
        logging.info("Output shape = {}".format(X.shape))

        use_C = True
        logging.info("START: whitening step with reconstruction (C version)")
        start = timer()
        whiten_images(X2, 3, 3, reconstruct_image=True, use_C=use_C)
        end = timer()
        logging.info("END: whitening with reconstruction done in {:.4f} sec"
                        .format(end - start))
        logging.info("Output shape = {}".format(X2.shape))

        use_C = False
        logging.info("START: whitening step with 0-padding and reconstruction (Python version)")
        start = timer()
        whiten_images(X3, 3, 3, padding="SAME",
                      reconstruct_image=True, use_C=use_C)
        end = timer()
        logging.info("END: whitening with 0-padding and reconstruction done in {:.4f} sec"
                        .format(end - start))
        logging.info("Output shape = {}".format(X3.shape))

        plt.figure(1)
        plt.subplot(5,2,1)
        plt.imshow(X0[1])
        plt.title("Original image")
        plt.subplot(5,2,3)
        plt.imshow(Xw[1]/0.4 + 0.2)
        plt.title("Pre-whitened image")
        plt.subplot(5,2,5)
        plt.imshow(X[1]/0.4 + 0.2)
        plt.title("Whitened image (Python)")
        plt.subplot(5,2,7)
        plt.imshow(X2[1]/0.4 + 0.2)
        plt.title("Whitened image (C)")
        plt.subplot(5,2,9)
        plt.imshow(X3[1]/0.4 + 0.2)
        plt.title("Whitened image (Python, 0-padding)")
        plt.subplot(5,2,2)
        plt.imshow(X0[2])
        plt.title("Original image")
        plt.subplot(5,2,4)
        plt.imshow(Xw[2]/0.4 + 0.2)
        plt.title("Pre-whitened image")
        plt.subplot(5,2,6)
        plt.imshow(X[2]/0.4 + 0.2)
        plt.title("Whitened image (Python)")
        plt.subplot(5,2,8)
        plt.imshow(X2[2]/0.4 + 0.2)
        plt.title("Whitened image (C)")
        plt.subplot(5,2,10)
        plt.imshow(X3[2]/0.4 + 0.2)
        plt.title("Whitened image (Python, 0-padding)")
        plt.tight_layout()
        plt.show()


if __name__ == "__main__":
    unittest.main()
