"""image transformation"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Alberto Bietti, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import numpy as np
from sklearn.feature_extraction import image
import logging

def pywhiten(input_image, patch_height, patch_width):
    """whitening code in Python

    Args:
        input_images (np.array): image of shape [heigth, width, channels].
        patch_height (int): height of the required patch.
        patch_width (int): width of the required patch.

    """
    p = image.extract_patches_2d(input_image, (patch_height, patch_width))
    npatch = p.shape[0]

    ## centering by channel
    p = p.reshape(p.shape[0], patch_height * patch_width, -1)
    mean_p = p.mean(axis=(1))
    p = p - np.repeat(np.expand_dims(mean_p, axis=1), patch_height * patch_width, axis=1)
    p = p.reshape(npatch, -1)

    ## centering
    mean_p = p.mean(axis=0)
    p = p - mean_p

    ## filtering
    U, s, _ = np.linalg.svd(p.T, full_matrices=False)
    s[s < 0] = 0
    ind = s > 1e-8 * s.max()
    s[ind] = 1. / np.sqrt(s[ind])
    s[~ind] = 0
    Wfilt = np.matmul(U, np.matmul(np.diag(s), U.T))
    p = np.matmul(p, Wfilt)
    return p


def whiten_images(X, patch_height, patch_width,
                  padding="VALID",
                  reconstruct_image=False,
                  verbose=True, use_C=True):
    """whiten images (cut down high frequencies)

    Args:
        X (np.array): images, shape (num_images, height, width, num_channels).
        patch_height (int): height of the required patch.
        patch_width (int): width of the required patch.
        padding (string): indicates if zero-padding (padding="SAME") or
            no padding (padding="VALID") is used in patch extraction before
            whitening. Default is "VALID" (no padding).
        reconstruct_image (bool): should images be reconstructed.
        verbose (bool): set verbosity.
        use_C (bool): indicates if the C++ routine should be used
            (if available).

    Whitening with reconstruction is made in place on X.

    If available, this function calls the whitening routine written in C++ from
    the Python package 'whitening'. If not, it uses a Python-based whitening
    routine.

    Note: the C++ wrapped function from the package 'whitening' only supports
    squared patches (i.e. patch_height = patch_width).

    Returns:
        If no reconstruction, returns an np.array containing whitened images.

    """
    Xout = []
    # input dimension
    h, w, c = X.shape[1:]
    # output dimension if no reconstruction
    if padding == "VALID":
        h1 = (h - patch_height + 1)
        w1 = (w - patch_width + 1)
        h2 = h
        w2 = w
    elif padding == "SAME":
        h1 = h
        w1 = w
        h2 = h + 2 * (patch_width // 2)
        w2 = w + 2 * (patch_height // 2)
    else:
        raise ValueError("`padding` arg should be equal to \"SAME\" (zero-padding) "
                         + "or \"VALID\" (no padding)")
    c1 = patch_height * patch_width * c

    # C++ routine
    try:
        assert use_C == True
        assert patch_height == patch_width
        from whitening.whiten import whiten as cwhiten
        def whiten(X, patch_height, patch_width):
            return cwhiten(X, patch_height)
    except:
        logging.warning("Could not import or use C++-based whitening, "
                        + "use of Python-based whitening instead")
        def whiten(X, patch_height, patch_width):
            return pywhiten(X, patch_height, patch_width)

    # logging.debug("whitening")
    for idx in range(X.shape[0]):
        im = X[idx]
        if padding == "SAME":
            im = np.pad(im, pad_width=((patch_width // 2,),(patch_height // 2,),(0,)),
                        mode='constant', constant_values=0)

        p = whiten(im, patch_height, patch_width)

        if reconstruct_image:
            p = p.reshape(p.shape[0], patch_height, patch_width, -1)
            im = image.reconstruct_from_patches_2d(p, (h2, w2, c))
            if padding == "SAME":
                X[idx] = im[(patch_width // 2):-(patch_width // 2),
                            (patch_height // 2):-(patch_height // 2), :]
            else:
                X[idx] = im
        else:
            p = np.expand_dims(p, axis=0)
            p = p.reshape(1, h1, w1, c1)
            Xout.append(p)

    if not reconstruct_image:
        Xout = np.concatenate(Xout, axis=0)
        return Xout
