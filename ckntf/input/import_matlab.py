"""import data/results from Matlab"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = ", Ghislain Durif, Daan Wynen, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Ghislain Durif", "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"


import logging
import sys
import scipy.io
import numpy as np
import json

from ckntf.core.utils import obj


def import_model_matlab(model, path, data_set="cifar-10_w", model_file=None):
    """import model objects computed by matlab version of ckn

    Args:
        model (object): a model structure returned by 'read_model'.
        path (string): path where matlab files are located.
        data_set (string): name of the data set.
        model_file (string): name of the matlab file where the model is stored
            (filters), should end with a '.mat' extension, default is None
            and the matlab file name is deduced from the netwok architecture.

    """

    if (not isinstance(path, str)):
        raise ValueError("'path' should be a path as a string")

    if (not isinstance(data_set, str)):
        raise ValueError("'data_set' should be a path as a string")

    if(model_file is not None):
        if (not isinstance(model_file, str) or not model_file.endswith('.mat')):
            raise ValueError("'model_file' should be a file name as "
                                + "a string ending with '.mat'")

    else:
        model_file = get_matlab_model_filename(model, path, data_set)


    logging.info("loading matlab model file")
    try:
        import scipy.io
        mat = scipy.io.loadmat(model_file, squeeze_me=True,
                               struct_as_record=False)
        filter_list = []

        if(mat['model'].nlayers > 1):
            for layer in mat['model'].layer:
                filters = layer.W
                sigma = layer.sigma
                out_channels = layer.W.shape[1]
                filter_list.append(
                    sigma**2 * np.ascontiguousarray(
                                    layer.W.reshape(layer.npatch,
                                                    layer.npatch,
                                                    -1,
                                                    out_channels)
                                            .transpose(1, 0, 2, 3)))
        else:
            layer = mat['model'].layer
            filters = layer.W
            sigma = layer.sigma
            out_channels = layer.W.shape[1]

            # filter_list.append(
            #     sigma**2 * np.ascontiguousarray(
            #                     layer.W.reshape(layer.npatch,
            #                                     layer.npatch,
            #                                     -1,
            #                                     out_channels)
            #                             ))

            filter_list.append(
                sigma**2 * np.ascontiguousarray(
                                layer.W.reshape(layer.npatch,
                                                layer.npatch,
                                                -1,
                                                out_channels)
                                        .transpose(1, 0, 2, 3)))

        return filter_list

    except:
        # print "Error:", sys.exc_info()[0]
        raise


def import_output_matlab(model, path, data_set="cifar-10_w", output_file=None):
    """import output objects computed by matlab version of ckn

    Args:
        model (object): a model structure returned by 'read_model'.
        path (string): path where matlab files are located.
        data_set (string): name of the data set.
        output_file (string): name of the matlab file where the output is
            stored, should end with a '.mat' extension, default is None
            and the matlab file name is deduced from the netwok architecture.

    """

    if (not isinstance(path, str)):
        raise ValueError("'path' should be a path as a string")

    if (not isinstance(data_set, str)):
        raise ValueError("'data_set' should be a path as a string")

    if(output_file is not None):
        if (not isinstance(output_file, str) or not output_file.endswith('.mat')):
            raise ValueError("'output_file' should be a file name as "
                                + "a string ending with '.mat'")

    else:
        output_file = get_matlab_output_filename(model, path, data_set)

    logging.info("loading matlab output file")
    try:
        import scipy.io
        mat = scipy.io.loadmat(output_file)
        features_tr = np.ascontiguousarray(mat['psiTr'].T)
        features_te = np.ascontiguousarray(mat['psiTe'].T)
        labels_tr = np.ascontiguousarray(
                        mat['Ytr'].flatten().astype(np.float32).squeeze())
        labels_te = np.ascontiguousarray(
                        mat['Yte'].flatten().astype(np.float32).squeeze())
        return features_tr, features_te, labels_tr, labels_te

    except NotImplementedError as ex:
        import h5py
        print('scipy.io failed to read SVM input:\n')
        print(ex)
        print('\nusing h5py instead')
        with h5py.File(output_file) as hf:
            features_tr = np.asarray(hf['psiTr'], dtype=np.float32)
            features_te = np.asarray(hf['psiTe'], dtype=np.float32)
            labels_tr = np.asarray(hf['Ytr'], dtype=np.float32).squeeze()
            labels_te = np.asarray(hf['Yte'], dtype=np.float32).squeeze()
            return features_tr, features_te, labels_tr, labels_te


def get_matlab_filename(model, path, data_set):
    """get the matlab output file name corresponding to a model

    Args:
        model (object): a model structure returned by 'read_model'.
        path (string): path where matlab files are located.
        data_set (string): name of the data set.

    Matlab function:

    function name=get_name(dataset,npatches,subsampling,nfilters,sigmas,
                            zero_pad,type_layer,type_init,pooling_mode,lambda2);
    nlayers=length(npatches);
    name=sprintf(['model_%s_' repmat('%g_%g_%g_%g_',[1,nlayers])
                    '%g_%g_%g_%g_%g.mat'],dataset,npatches,subsampling,nfilters,
                    sigmas,zero_pad,type_layer,type_init,pooling_mode,lambda2);
    end

    example: model_cifar-10_w_3_2_2_6_64_256_0.6_0.6_1_0_0_0_0.01.mat

    """

    ## layer parameters
    with open(model.layer_file) as f:
        param_list = json.load(f)

    for ind, param in enumerate(param_list):
        param_list[ind] = obj(param)

    ## construct the name
    name = "model_"

    ## data set
    name = name + data_set + "_"

    ## patch size
    for i, layer in enumerate(param_list):
        name = name + str(layer.filter_height) + "_"

    ## subsampling
    for i, layer in enumerate(param_list):
        name = name + str(layer.subsampling) + "_"

    ## out_channels
    for i, layer in enumerate(param_list):
        name = name + str(layer.out_channels) + "_"

    ## sigma
    for i, layer in enumerate(param_list):
        name = name + str(layer.kernel_param) + "_"

    ## zero padding, type_layer, type_init, pooling_mode
    name = name + "1" + "_" + "0" + "_" + "0" + "_" + "0" + "_"

    ## epsilon_inv
    name = name + str(param_list[0].epsilon_inv)

    ## extension
    name = name + ".mat"

    return name


def get_matlab_model_filename(model, path, data_set):
    """get the matlab model file name corresponding to a model

    Args:
        model (object): a model structure returned by 'read_model'.
        path (string): path where matlab files are located.
        data_set (string): name of the data set.

    example: 'path'/logs/26_model_cifar-10_w_3_2_2_6_64_256_0.6_0.6_1_0_0_0_0.01.mat

    """

    model_savename = path + "/logs/26_" \
                        + get_matlab_filename(model, path, data_set)

    return model_savename


def get_matlab_output_filename(model, path, data_set):
    """get the matlab output file name corresponding to a model

    Args:
        model (object): a model structure returned by 'read_model'.
        path (string): path where matlab files are located.
        data_set (string): name of the data set.

    example: 'path'/logs/26_output_model_cifar-10_w_3_2_2_6_64_256_0.6_0.6_1_0_0_0_0.01.mat

    """

    output_savename = path + "/logs/26_output_"  \
                        + get_matlab_filename(model, path, data_set)

    return output_savename


## test
if __name__=='__main__':

    logging.basicConfig(stream=sys.stdout,
                        format='%(levelname)s:%(message)s',
                        level=logging.DEBUG)

    from ckntf.input import read_model

    ## model structure
    logging.info("Model parameters")
    model_file = "example_config_model.json"
    model = read_model(model_file)

    ## path to matlab files
    path = "/local_scratch/gdurif/Codes/CKN-original"

    ## import filters
    filters = import_model_matlab(model, path, data_set="cifar-10_w")
    print("nb of filters = %d" % (len(filters)))
    for i, filter_mat in enumerate(filters):
        print("filter %d shape = %s" % (i, filter_mat.shape))
        print(filter_mat.flags)

    ## import output
    features_tr, features_te, labels_tr, labels_te = import_output_matlab(model, path, data_set="cifar-10_w")
    print("\nfeatures_tr")
    print("shape = %s" % (features_tr.shape,))
    print(features_tr.flags)
    print("\nfeatures_te")
    print("shape = %s" % (features_te.shape,))
    print(features_te.flags)
    print("\nlabels_tr")
    print("shape = %s" % (labels_tr.shape,))
    print(labels_tr.flags)
    print("\nlabels_te")
    print("shape = %s" % (labels_te.shape,))
    print(labels_te.flags)
