"""convert Matlab whitened cifar10 data set to python format"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
                "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import logging
import sys
logging.basicConfig(stream=sys.stdout,
                    format='%(levelname)s:%(message)s',
                    level=logging.DEBUG)

import numpy as np
import scipy.io

import ckntf.input

# data
logging.info("### import data")

#### Matlab CKN path
matlab_dir = "/scratch/artemis/gdurif/scratch_code/CKN-original"

#### load whitened cifar10 from Julien Mairal
data_file = matlab_dir + "/data/cifar_white.mat"
mat = scipy.io.loadmat(data_file, squeeze_me=True, struct_as_record=False)

#### data
# image on the right side (requires to transpose filters)
Xtr = np.ascontiguousarray(np.transpose(np.reshape(mat['Xtr'], (32,3,32,-1)), axes=[3, 2, 0, 1]))
Xte = np.ascontiguousarray(np.transpose(np.reshape(mat['Xte'], (32,3,32,-1)), axes=[3, 2, 0, 1]))

# # transposed images (does not require to transpose filters)
# Xtr = np.ascontiguousarray(np.transpose(np.reshape(mat['Xtr'], (32,3,32,-1)), axes=[3, 0, 2, 1]))
# Xte = np.ascontiguousarray(np.transpose(np.reshape(mat['Xte'], (32,3,32,-1)), axes=[3, 0, 2, 1]))

Ytr = np.ascontiguousarray(mat['Ytr'].flatten().astype(np.int32).squeeze())
Yte = np.ascontiguousarray(mat['Yte'].flatten().astype(np.int32).squeeze())

### visu
# import matplotlib.pyplot as plt
# plt.imshow(Xtr[1]/0.4 + 0.2)
# plt.show()
#
# ## non whitened cifar (for comparison) N x 3072 (1024 R, 1024 G, 1024 B)
# from PIL import Image
# data_file0 = "/scratch/clear/mairal/ckn/trunk/data/cifar-10-batches-mat" + "/data_batch_1.mat"
# mat0 = scipy.io.loadmat(data_file0, squeeze_me=True, struct_as_record=False)
# Xtr0 = mat0['data'].reshape(-1, 3, 32, 32).transpose(0,2,3,1)
# Image.fromarray(Xtr0[1], 'RGB').show()
## OK


# save data
pkl_data_file = "/scratch2/clear/gdurif/data/images/cifar10/matlab_whitened.pkl"
ckntf.input.pickle_data((Xtr, Ytr, Xte, Yte), pkl_data_file)
logging.info("SAVING done")
