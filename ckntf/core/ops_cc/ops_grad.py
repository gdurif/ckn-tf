"""register gradients of ops defined in `load_ops.py`"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Dexiong Chen, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "Apache 2.0"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

from tensorflow.python.framework import ops
from tensorflow.python.ops import math_ops, array_ops

@ops.RegisterGradient("MatrixInverseSqrt")
def _MatrixInverseSqrtGrad(op, grad0, grad1, grad2):
    e = op.outputs[1]
    v = op.outputs[2]
    ei = array_ops.expand_dims(e, -2)
    ej = array_ops.expand_dims(e, -1)
    f = math_ops.reciprocal(
            (ei + ej)*ei*ej)
    # print(grad0.eval())
    grad = -math_ops.matmul(
        v,
        math_ops.matmul(
            f*math_ops.matmul(
                v, math_ops.matmul(
                    grad0, v), adjoint_a=True),
            v, adjoint_b=True))
    grad = array_ops.matrix_band_part(
        grad + array_ops.matrix_transpose(grad), -1, 0)
    grad = array_ops.matrix_set_diag(grad,
                                       0.5 * array_ops.matrix_diag_part(grad))
    return grad
