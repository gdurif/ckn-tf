"""tests for ops defined in ops_grad.py"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Dexiong Chen, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "Apache 2.0"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import numpy as np

from tensorflow.python.framework import constant_op
from tensorflow.python.framework import dtypes
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import gradient_checker
from tensorflow.python.platform import test
from ckntf.core.ops_cc import matrix_inverse_sqrt
import tensorflow as tf

class MatrixInverseSqrtGradTest(test.TestCase):
    pass

def _GetMatrixInverseSqrtGradTest(dtype_, size_):
    def Test(self):
        np.random.seed(1)
        n = size_
        np_dtype = dtype_.as_numpy_dtype
        x = np.random.uniform(
            low=-1.0, high=1.0, size=n * n).reshape([n, n]).astype(np_dtype)
        epsilon = np.finfo(np_dtype).eps
        # Optimal stepsize for central difference is O(epsilon^(1/3))
        delta = 0.1 * epsilon**(1.0/3.0)
        if dtype_ == dtypes.float32:
            tol = 1e-1
        else:
            tol = 1e-4
        with self.test_session():
            tf_x = constant_op.constant(x)
            tf_xtx = math_ops.matmul(tf_x, tf_x, adjoint_b=True) + epsilon*array_ops.identity(tf_x)
            tf_y = matrix_inverse_sqrt(tf_xtx)
            x_init = np.random.uniform(
                                low=-1.0,
                                high=1.0,
                                size=n * n).reshape([n, n]).astype(np_dtype)
            theoretical, numerical = gradient_checker.compute_gradient(
                tf_x,
                tf_x.get_shape().as_list(),
                tf_y,
                tf_y.get_shape().as_list(),
                x_init_value=x_init,
                delta=delta)
            self.assertAllClose(theoretical, numerical, atol=tol, rtol=tol)
    return Test


if __name__ == "__main__":
    """TODO: when tested with dtypes.float32, there are numerical errors"""
    for dtype in (dtypes.float64, ):
        for size in 1, 2, 5, 10:
            name = '%s_%s' % (dtype, size)
            setattr(MatrixInverseSqrtGradTest, 'testInverseSqrtGrad' + name,
                _GetMatrixInverseSqrtGradTest(dtype, size))
    test.main()
