"""load ops defined and registered in cc"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Dexiong Chen, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "Apache 2.0"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

from glob import glob
import os
import pkg_resources
op_file = glob(pkg_resources.resource_filename('ckntf',
                os.path.join('core', 'ops_cc', 'matrix_inverse_sqrt_op*.so')))

import tensorflow as tf
from tensorflow.python.framework import ops
_matrix_inverse_sqrt_module = tf.load_op_library(op_file[0])
_matrix_inverse_sqrt = _matrix_inverse_sqrt_module.matrix_inverse_sqrt
import ckntf.core.ops_cc.ops_grad

def matrix_inverse_sqrt(tensor, eps=1e-7, name=None):
    with tf.device("/device:CPU:0"):
        with ops.name_scope(name, "matrix_inverse_sqrt", [tensor]) as scope:
            output, e, v = _matrix_inverse_sqrt(tensor + eps*tf.eye(tensor.get_shape()[0].value, dtype=tensor.dtype))
    return output
