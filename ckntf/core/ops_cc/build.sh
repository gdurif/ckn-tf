export TF_INC=$(python3 -c 'import tensorflow as tf; print(tf.sysconfig.get_include())');
g++ -std=c++11 -shared -I $TF_INC -O2 matrix_inverse_sqrt_op.cc linalg_ops_common.cc -o matrix_inverse_sqrt_op.so -fPIC -D_GLIBCXX_USE_CXX11_ABI=0

