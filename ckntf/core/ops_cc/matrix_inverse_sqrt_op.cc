/*
author = "Dexiong Chen, THOTH TEAM INRIA Grenoble Alpes"
copyright = "INRIA"
credits = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
           "Julien Mairal", "Daan Wynen"]
license = "Apache 2.0"
version = "1.0"
maintainer = "Ghislain Durif"
email = "ghislain.durif@inria.fr"
status = "Development"
date = "2017"
 */

#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "third_party/eigen3/Eigen/Core"
#include "third_party/eigen3/Eigen/Eigenvalues"
#include "tensorflow/core/framework/kernel_def_builder.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "linalg_ops_common.h"
#include "tensorflow/core/lib/core/errors.h"
#include "tensorflow/core/platform/logging.h"
#include "tensorflow/core/platform/types.h"

using namespace tensorflow;

REGISTER_OP("MatrixInverseSqrt")
    .Input("input: T")
    .Output("output: T")
    .Output("e: T")
    .Output("v: T")
    .Attr("T: {double, float}")
    .SetShapeFn([](::tensorflow::shape_inference::InferenceContext *c) {
      c->set_output(0, c->input(0));
      c->set_output(1, c->Vector(c->Dim(c->input(0), -1)));
      c->set_output(2, c->input(0));
      return Status::OK();
    })
    .Doc(R"doc(
Computes the inverse square root of one matrix.
The input is a tensor of shape `[..., M, M]` whose inner-most 2 dimensions
form square matrices. The output is a tensor of the same shape as the input
containing the inverse for all input submatrices `[..., :, :]`.
The op uses LU decomposition with partial pivoting to compute the inverses.
If a matrix is not invertible there is no guarantee what the op does. It
may detect the condition and raise an exception or it may simply return a
garbage result.
input: Shape is `[..., M, M]`.
output: Shape is `[..., M, M]`.
)doc");

template <class Scalar>
class MatrixInverseSqrtOp : public LinearAlgebraOp<Scalar> {
public:
  INHERIT_LINALG_TYPEDEFS(Scalar);

  explicit MatrixInverseSqrtOp(OpKernelConstruction *context) : Base(context) {}

  TensorShapes
  GetOutputMatrixShapes(const TensorShapes &input_matrix_shapes) const final {
    int64 n = input_matrix_shapes[0].dim_size(0);
    return TensorShapes(
        {TensorShape({n, n}), TensorShape({n}), TensorShape({n, n})});
  }

  void ComputeMatrix(OpKernelContext *context, const ConstMatrixMaps &inputs,
                     MatrixMaps *outputs) final {
    const ConstMatrixMap &input = inputs[0];
    if (input.rows() == 0) {
      // By definition, an empty matrix's inverse sqaure root is an empty
      // matrix.
      return;
    }
    Eigen::SelfAdjointEigenSolver<Matrix> eig(input,
                                              Eigen::ComputeEigenvectors);
    OP_REQUIRES(
        context, eig.info() == Eigen::Success,
        errors::InvalidArgument("Self Adjoint Eigen decomposition was not "
                                "successful. The input might not be valid."));
    outputs->at(0) = eig.operatorInverseSqrt();
    // outputs->at(1) =
    // eig.eigenvectors()*eig.eigenvalues().cwiseInverse().array().pow(0.75).matrix().asDiagonal()*eig.eigenvectors().adjoint();
    outputs->at(1) = eig.eigenvalues().cwiseSqrt().template cast<Scalar>();
    outputs->at(2) = eig.eigenvectors();
  }
};

REGISTER_LINALG_OP("MatrixInverseSqrt", (MatrixInverseSqrtOp<float>), float);
REGISTER_LINALG_OP("MatrixInverseSqrt", (MatrixInverseSqrtOp<double>), double);
