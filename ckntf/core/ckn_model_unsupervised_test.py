"""running test on the CKN model class in unsupervised mode"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
                "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import logging
import sys
logging.basicConfig(stream=sys.stdout,
                    format='%(levelname)s:%(message)s',
                    level=logging.DEBUG)

from timeit import default_timer as timer
from pprint import pprint

import pkg_resources

import tensorflow as tf
import numpy as np
import math
import ckntf.core
import ckntf.input
from ckntf.core.utils import obj

class args:
    ## data
    sample_size = 1000
    batch_size = 500
    data_folder = pkg_resources.resource_filename('ckntf', 'data')
    ## online whitening
    online_whitening = True
    ## tensorboard
    output_dir = 'test_unsupervised_mode'

## CKN model parameters
# params = [
#     {
#         'filter_height': 3, 'filter_width': 3,
#         'out_channels': 64, 'subsampling': 2,
#         'n_sampled_patches': 10000, 'kernel': 'exp0', 'kernel_param': 0.6,
#         'dtype': tf.float32, 'padding': "SAME", 'name': "layer1",
#         'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
#     },
#     {
#         'filter_height': 2, 'filter_width': 2,
#         'out_channels': 256, 'subsampling': 6,
#         'n_sampled_patches': 10000, 'kernel': 'exp0', 'kernel_param': 0.6,
#         'dtype': tf.float32, 'padding': "SAME", 'name': "layer2",
#         'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
#     }
# ]
params = [
    {
        'filter_height': 3, 'filter_width': 3,
        'out_channels': 64, 'subsampling': 2,
        'n_sampled_patches': 10000, 'kernel': 'exp0', 'kernel_param': 0.6,
        'dtype': tf.float32, 'padding': "SAME", 'name': "layer1",
        'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
    }
]

## for tensorboard
print("For tensorboard:\n    run `tensorboard --logdir="
      + args.output_dir + "` in a shell"
      + "\n    and go to"
      + "`localhost:6006` on your web-browser")


class CKNmodelTest(tf.test.TestCase):
    def testCKNmodel(self):

        print("Running tests on the CKNmodel class in unsupervised mode")
        ckntf.core.ckn_layer.DEBUG = True
        ckntf.core.ckn_model.DEBUG = True

        pprint(vars(args))
        pprint(params)

        ### import data
        logging.info("importing data")
        # (Xtr, Ytr,
        #  Xte, Yte) = ckntf.input.read_whitened_dataset_cifar10(args.data_file,
        #                                                      args.sample_size)
        (Xtr, Ytr,
         Xte, Yte) = ckntf.input.load_cifar10(args.data_folder,
                                            args.sample_size,
                                            whitened=not args.online_whitening)
        (N, H, W, C) = Xtr.shape
        logging.info("data imported")

        # data spec
        logging.info("dimensions of the data set ({})".format(Xtr.shape))

        ### run tensorflow
        with self.test_session() as sess:

            # define the model
            model = ckntf.core.CKNmodel.from_param(params, sess, args.batch_size,
                                                 online_whitening=args.online_whitening)

            # encode the model
            print("#### encode the model ####")
            model.encode(H, W, C)

            ##### unsupervised training of the filters
            print("#### unsupervised training ####")

            ## training of the model
            logging.info("START: unsupervised training of the {} layer(s) "
                            .format(len(model.layer_list)) \
                            + "in the model with k-means")
            start = timer()
            model.unsup_train(input_data=Xtr)
            end = timer()
            logging.info("END: training of the {} layers in {:.4f} sec"
                            .format(len(model.layer_list), end - start))

            # get the filters
            logging.info("get the filters")
            filter_list = model.get_filters()
            for ind, filters in enumerate(filter_list):
                logging.info("layer {}: filter shape = {}"
                                .format(ind+1, filters.shape))
                lparam = obj(params[ind])
                if args.online_whitening and ind == 0:
                    # specific case for the first layer
                    # when doing online whitening
                    self.assertAllEqual(filters.shape,
                                        (1,
                                         1,
                                         model.layer_list[ind]
                                              .input.get_shape().as_list()[3],
                                         lparam.out_channels))
                else:
                    self.assertAllEqual(filters.shape,
                                        (lparam.filter_height,
                                         lparam.filter_width,
                                         model.layer_list[ind]
                                              .input.get_shape().as_list()[3],
                                         lparam.out_channels))

            # set the filters
            logging.info("set the filters")
            model.update_filters(filter_list)

            # output of the model
            logging.info("computation of the model output")
            start = timer()
            output = model.get_output(input_data=Xtr)
            end = timer()
            logging.info("computation of the output in {:.4f} sec".format(end - start))
            logging.info("output shape = {}".format(output.shape))
            ## subsampling factor on output
            sub_factor = 1
            for ind,lparam in enumerate(params):
                sub_factor *= obj(lparam).subsampling
            self.assertAllEqual(output.shape,
                                (Xtr.shape[0],
                                 Xtr.shape[1] // sub_factor
                                    + 1 * (Xtr.shape[1] % sub_factor != 0),
                                 Xtr.shape[2] // sub_factor
                                    + 1 * (Xtr.shape[2] % sub_factor != 0),
                                 obj(params[-1]).out_channels))

            ## tensorboard visualization
            logging.info("save metadata for tensorboard visualization")
            writer = tf.summary.FileWriter(args.output_dir, sess.graph)
            writer.close()

            print("#### tests over ####")


if __name__ == "__main__":
    tf.test.main()
