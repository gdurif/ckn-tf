"""running test on the CKN layer class"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
                "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import logging
import os
import sys
logging.basicConfig(stream=sys.stdout,
                    format='%(levelname)s:%(message)s',
                    level=logging.DEBUG)

from timeit import default_timer as timer
from pprint import pprint

import pkg_resources

import tensorflow as tf
import numpy as np
import math
import ckntf.core
import ckntf.input

class args:
    ## data
    sample_size = 1000
    batch_size = 500
    data_folder = pkg_resources.resource_filename('ckntf', 'data')


class param:
    ## CKN layer parameters
    filter_height = 3
    filter_width = 3
    out_channels = 64
    subsampling = 2
    n_sampled_patches = 10000
    kernel = 'exp0'
    kernel_param = 0.7
    dtype = tf.float32
    padding = "SAME"
    name = "ckn_layer"
    epsilon_norm = 1e-5
    epsilon_inv = 1e-3


class CKNlayerTest(tf.test.TestCase):
    def testCKNlayer(self):

        print("Running tests on the CKNlayer class")

        pprint(vars(args))
        pprint(vars(param))

        ### import data
        logging.info("START: importing data")
        (Xtr, Ytr,
         Xte, Yte) = ckntf.input.load_cifar10(args.data_folder,
                                            args.sample_size,
                                            whitened=True)
        (N, H, W, C) = Xtr.shape
        logging.info("END: data imported")

        # data spec
        logging.info("dimensions of the data set ({})".format(Xtr.shape))

        ### run tensorflow
        with self.test_session() as sess:

            # input
            input = tf.placeholder(dtype=param.dtype, shape=[None, H, W, C])

            # define a layer
            simple_layer = ckntf.core.CKNlayer.from_param(param)

            # encode the layer
            print("#### encode the layer ####")
            output = simple_layer.encode(input)

            ##### unsupervised training of the filters
            print("#### unsupervised training ####")

            # batch nb
            n_batch = Xtr.shape[0] // args.batch_size \
                        + 1 * (args.sample_size % args.batch_size != 0)

            # extract patches
            logging.info("START: extract the patches")
            logging.info("data separated in {} batch(es)".format(n_batch))
            start = timer()
            for i_batch in range(0,(n_batch)):
                logging.debug("batch_index = %d / %d" %(i_batch+1, n_batch))

                i_start = i_batch * args.batch_size
                i_end = Xtr.shape[0] if i_batch == n_batch - 1 \
                        else (i_batch + 1) * args.batch_size
                # logging.debug("    i_start = {} // i_end = {}"
                #                 .format(i_start, i_end))
                sub_data = Xtr[i_start:i_end,]
                ## feed placeholder
                simple_layer.append_patches(
                                sess,
                                n_batch=n_batch,
                                feed_dict={input: sub_data})

            simple_layer.concatenate_patches()
            end = timer()
            logging.info("END: extraction of the patches in {:.4f} sec"
                            .format(end - start))

            # update the filters
            logging.info("START: train the filters with k-means")
            start = timer()
            simple_layer.update_filters_kmeans()
            simple_layer.compute_update_filters_kmeans(sess)
            end = timer()
            logging.info("END: training of the filters in {:.4f} sec"
                            .format(end - start))

            # get the filters
            logging.info("get the filters")
            filters = simple_layer.get_filters(sess)
            logging.info("filter shape = {}".format(filters.shape))
            self.assertAllEqual(filters.shape,
                                (param.filter_height,
                                 param.filter_width,
                                 Xtr.shape[3],
                                 param.out_channels))

            # set the filters
            logging.info("set the filters")
            simple_layer.update_filters(filters)
            simple_layer.compute_update_filters(sess)

            # output of the layer
            logging.info("START: computation of the layer output")
            logging.info("data separated in {} batch(es)".format(n_batch))
            output = []
            start = timer()
            for i_batch in range(0,(n_batch)):
                logging.debug("batch_index = %d / %d" %(i_batch+1, n_batch))

                i_start = i_batch * args.batch_size
                i_end = Xtr.shape[0] if i_batch == n_batch - 1 \
                        else (i_batch + 1) * args.batch_size
                # logging.debug("    i_start = {} // i_end = {}"
                #                 .format(i_start, i_end))
                sub_data = Xtr[i_start:i_end,]
                ## feed placeholder
                run_metadata = tf.RunMetadata()
                tmp = sess.run(simple_layer.output,
                               feed_dict={input: sub_data})
                output.append(tmp)

            output = np.concatenate(output, axis=0)
            end = timer()
            # logging.info("END: computation of the output in {:.4f} sec"
            #                 .format(end - start))
            logging.info("output shape = {}".format(output.shape))
            self.assertAllEqual(output.shape,
                                (Xtr.shape[0],
                                 Xtr.shape[1] // param.subsampling,
                                 Xtr.shape[2] // param.subsampling,
                                 param.out_channels))

            print("#### tests over ####")


if __name__ == "__main__":
    tf.test.main()
