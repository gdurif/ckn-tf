"""Definition of a CKN model.

Reference: Mairal, Julien. "End-to-End Kernel Learning with Supervised
Convolutional Kernel Networks." In Advances in Neural Information Processing
Systems, 1399–1407, 2016.
http://papers.nips.cc/paper/6184-end-to-end-kernel-learning-with-supervised-convolutional-kernel-networks.

Input:
    X: a set of images as a tensor [N, in_height, in_width, in_channels]
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Dexiong Chen, Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import logging
import os
import sys
from datetime import datetime
from timeit import default_timer as timer
from glob import glob

import tensorflow as tf
import numpy as np
import scipy

import ckntf.core
import ckntf.input
from ckntf.core.loss import squared_hinge
import miso_svm
from ckntf.input.transform_data import whiten_images

DEBUG = False
BIAS_SCALE = 0.003
FC_SCALE = 1.

class CKNmodel(object):
    """Defines a CKN model

    Attributes:
        input (placeholder): placeholder to store input data, or any provided
            input data.
        output (4-D tensor): output of the model.
        layer_list: list of CKN layers.
        session (tf.Session): current Tensorflow session.
        batch_size (int or None): size of the batches to consider,
            if None training is done in a single batch, default is None.
        online_whitening (boolean): indicates if the images should be
            whitened online or not. Default is False, it is then
            recommended to use pre-whitened images (preliminary
            offline whitening).

    Attributes (supervised mode):
        normalize_filters (Tensorflow op list): list of op that normalize
            the filters of each layer.
        compute_sqrt_inv (Tensorflow op list): list of op that compute matrix
            squared-root inverse for each layer.
        output_fc (2-D tensor): reshaped output of the model (samples x features).
        labels (1-D tensor): label of the samples.
        W (2-D tensor): weight coefficients in the last fully connected layer.
        b (1-D tensor): bias coefficients in the last fully connected layer.
        predictor (1-D tensor): linear combination of output features
            `output_fc` with coefficients `W` and bias `b`.
        init (tf.initializer): Tesorflow graph node that initialize variables.
        loss (scalar): current value of the loss.
        W_ext (placeholder): placeholder to store externally computed values
            of coefficients in the fully connected layer.
        b_ext (placeholder): placeholder to store externally computed values
            of bias in the fully connected layer.
        bias_scale (scalar tensor): scaling factor for the bias coefficient `b`.
        buffer_bias_scale (placeholder): used to update `bias_scale`.
        update_bias_scale (tf.assign op): operation to update `bias_scale`.
        update_W (tf.assign op): operation to update W with external values.
        update_b (tf.assign op): operation to update b with external values.
        train_acc (scalar): current value of the accuracy of prediction on
            the training set.
        test_acc (scalar): current value of the accuracy of prediction on
            the test set.
        sgd_op (Tensorflow operator): minimize a Tensorflow optimzer.
        l2_reg (scalar tensor): l2 regularization parameter.
        buffer_l2_reg (placeholder): used to update `l2_reg`.
        update_l2_reg (tf.assign op): operation to update `l2_reg`.
        saver (tf.train.saver): TensorFlow object used for checkpointings.
        global_step (tf.train.global_step): TensorFlow optimization iterations.
    """

    def __init__(self, layer_list, session, batch_size=None,
                 online_whitening=False):
        """Constructor for CKNmodel class

        Args:
            layer_list (list): list of CKNlayer objects.
            session (tf.Session): current Tensorflow session.
            batch_size (int or None): size of the batches to consider,
                if None training is done in a single batch, default is None.
            online_whitening (boolean): indicates if the images should be
                whitened online or not. Default is False, it is then
                recommended to use pre-whitened images (preliminary
                offline whitening).

        """
        self.layer_list = layer_list
        self.session = session
        self.batch_size = batch_size
        self.online_whitening = online_whitening

        self.input = None
        self.output = None

        ## used in supervised mode
        self.normalize_filters = None
        self.compute_sqrt_inv = None
        self.output_fc = None
        self.labels = None
        self.predictor = None
        self.W = None
        self.b = None
        self.init = None
        self.loss = None
        self.W_ext = None
        self.b_ext = None
        self.bias_scale = None
        self._bias_scale = None
        self.buffer_bias_scale = None
        self.update_bias_scale = None
        self.update_W = None
        self.update_b = None
        self.train_acc = None
        self.test_acc = None

        self.sgd_op = None
        self.l2_reg = None
        self._l2_reg = None
        self.buffer_l2_reg = None
        self.update_l2_reg = None

        self.saver = None
        self.global_step = None

        # used to save filter dimension for patch extraction in online whitening
        self._layer0_filter_height = None
        self._layer0_filter_width = None


    @classmethod
    def from_param(cls, param_list, session, batch_size=None,
                   online_whitening=False):
        """Initialize a CKN model based on a list of parameter containers,
        one for each CKN layer

        Args:
            param_list (list of dict or objects): list of parameter containers,
                each dictionary/object in the list contains the parameters to
                define a CKNlayer class, param_list can be a single dictionary
                or object for model with a single layer.
            session (tf.Session): current Tensorflow session.
            batch_size (int or None): size of the batches to consider,
                if None training is done in a single batch, default is None.
            online_whitening (boolean): indicates if the images should be
                whitened online or not. Default is False, it is then
                recommended to use pre-whitened images (preliminary
                offline whitening).

        """
        if not isinstance(param_list, (tuple, list)):
            param_list = [param_list]

        layer_list= []
        for ind,param in enumerate(param_list):
            ckn_layer = ckntf.core.CKNlayer.from_param(param)
            layer_list.append(ckn_layer)

        return cls(layer_list=layer_list,
                   session=session,
                   batch_size=batch_size,
                   online_whitening=online_whitening)


    def encode(self, in_height, in_width, in_channels):
        """Encode the CKN model in Tensorflow

        Args:
            in_height (int): input image height.
            in_width (int): input image width.
            in_channels (int): input image number of channels.

        """

        if self.online_whitening:
            self._layer0_filter_height = self.layer_list[0].filter_height
            self._layer0_filter_width = self.layer_list[0].filter_width
            self.layer_list[0].filter_height = 1
            self.layer_list[0].filter_width = 1
            filter_height = self._layer0_filter_height
            filter_width = self._layer0_filter_width
            if self.layer_list[0].padding == "VALID":
                in_height = (in_height - filter_height + 1)
                in_width = (in_width - filter_width + 1)

            in_channels = filter_height * filter_width * in_channels

        # placeholder for the input
        self.input = tf.placeholder(dtype=self.layer_list[0].dtype,
                                    shape=[None,
                                           in_height,
                                           in_width,
                                           in_channels],
                                    name='input')

        # encode the layers
        for ind, layer in enumerate(self.layer_list):
            self.output = layer.encode(self.input) if ind == 0 \
                                else layer.encode(self.output)


    # def _whitening(self, in_height, in_width, in_channels):
    #     """encode the online whitening (deprecated: not done with Tensorflow)
    #
    #     Images in input are transformed by whitened patches. The filter size of
    #     the first layer is set to 1, since the patch extraction was already
    #     done.
    #
    #     Args:
    #         in_height (int): input image height.
    #         in_width (int): input image width.
    #         in_channels (int): input image number of channels.
    #
    #     """
    #     logging.warning("Deprecated: not done with Tensorflow anymore")
    #     with tf.variable_scope("whitening") as scope:
    #
    #         ## filter dimensions
    #         filter_height = self.layer_list[0].filter_height
    #         filter_width = self.layer_list[0].filter_width
    #
    #         ### extract patches
    #         patches = tf.extract_image_patches(images=self.input,
    #                                            ksizes=[1,
    #                                                    filter_height,
    #                                                    filter_width,
    #                                                    1],
    #                                             strides=[1, 1, 1, 1],
    #                                             rates=[1, 1, 1, 1],
    #                                             padding="VALID",
    #                                             name="patch_extraction")
    #         # dim of 'patches' = [ batch_size, out_heigth, out_width,
    #         #                      filter_height * filter_width * in_channels ]
    #
    #         # number of patches in an image channel
    #         n_patches = (in_height - filter_height + 1) \
    #                         * (in_width - filter_width + 1)
    #
    #         # size of the patches
    #         patch_size = filter_height * filter_width * in_channels
    #
    #         # convert patches to 2-D matrix
    #         patches = tf.reshape(patches,
    #                              shape=[-1, n_patches, patch_size],
    #                              name="patch_reshape")
    #
    #         # centering
    #         patches = patches - tf.reduce_mean(patches, axis=1,
    #                                            keep_dims=True,
    #                                            name="patch_mean")
    #
    #         # covariance matrix
    #         cov = tf.matmul(tf.transpose(patches, [0, 2, 1]), patches,
    #                         name="covariance")
    #
    #         # eigen decomposition
    #         s, U = tf.self_adjoint_eig(cov, name="eigen_decomposition")
    #
    #         # whitening
    #         null_s = tf.zeros(dtype=s.dtype, shape=tf.shape(s),
    #                           name="null_vector")
    #         s = tf.where(tf.greater(s, null_s), s, null_s,
    #                      name="remove_null_eigenvalues")
    #         s = tf.where(tf.greater(s,
    #                                 1e-8 * tf.reduce_max(s, axis=0,
    #                                                      keep_dims=True,
    #                                                      name="max_eigenval")),
    #                      1. / tf.sqrt(s),
    #                      null_s,
    #                      name="filter_eigenvalues")
    #         # reshape to 4-D tensor
    #         self.output = tf.reshape(
    #                         tf.matmul(patches,
    #                                   tf.matmul(tf.matmul(U, tf.matrix_diag(s)),
    #                                             tf.transpose(U, [0, 2, 1])),
    #                                   name="updated_patches"),
    #                         shape=[-1,
    #                                 (in_height - filter_height + 1),
    #                                 (in_width - filter_width + 1),
    #                                 patch_size],
    #                         name = "whitened_input")
    #
    #     ## change dimension of the filter in the first layer
    #     ## in order to avoid patch extraction in first convolution
    #     self.layer_list[0].filter_height = 1
    #     self.layer_list[0].filter_width = 1
    #     self.layer_list[0].padding = "VALID"



    #--------------------------------------------------------------------------#
    # UNSUPERVISED MODE
    #--------------------------------------------------------------------------#

    def unsup_train(self, input_data, run_options=None,
                    writer=None, timeline=None,
                    n_iter_kmeans=10):
        """training of the CKN model in the unsupervised case

        Args:
            input_data (4-D numpy array): data for the training.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.
            n_iter_kmeans (int): number of iterations in spherical k-means.
                Default is 10. If set to 0, only a sub-sampling and random
                initialization of cluster centers are done.

        """

        ## online whitening
        if self.online_whitening:
            start = timer()
            input_data = whiten_images(input_data,
                                       patch_height=self._layer0_filter_height,
                                       patch_width=self._layer0_filter_width,
                                       padding=self.layer_list[0].padding,
                                       reconstruct_image=False)
            end = timer()
            logging.debug("input whitening done in {:.4f} sec, shape = {}"
                            .format(end - start, input_data.shape))

        ## data batch
        batch_set = BatchData(sample_size=input_data.shape[0],
                              batch_size=self.batch_size, shuffle=False,
                              respect_size=False)

        ## training
        for ind, layer in enumerate(self.layer_list):

            logging.info("### training of layer {} \"{}\""
                            .format(ind+1, layer.name))

            logging.debug("START: patch subsampling in \"{}\""
                                 .format(layer.name))
            start = timer()
            ## extract patches by batch
            for i_batch in range(0,(batch_set.n_batch)):

                batch_data,_ = batch_set.next_batch(input_data,
                                                    input_labels=None,
                                                    debug=DEBUG)

                ## feed placeholder
                layer.append_patches(self.session,
                                     n_batch=batch_set.n_batch,
                                     feed_dict={self.input: batch_data},
                                     run_options=run_options,
                                     writer=writer,
                                     timeline=timeline)

            layer.concatenate_patches()
            end = timer()
            logging.debug("END: patch subsampling in {:.4f} sec"
                                .format(end - start))

            logging.debug("patch set in layer {} \"{}\": shape {} and type {}"
                            .format(ind+1, layer.name, layer.patch_set.shape,
                                    type(layer.patch_set)))

            ## compute filters
            layer.update_filters_kmeans(n_iter=n_iter_kmeans)
            layer.compute_update_filters_kmeans(self.session,
                                        run_options=run_options,
                                        writer=writer,
                                        timeline=timeline)


    #--------------------------------------------------------------------------#
    # SUPERVISED MODE
    #--------------------------------------------------------------------------#

    def encode_supervised(self, n_class,
                          loss_type='squared_hinge', l2_reg=0.1):
        """Encode additional nodes for the supervised training of the CKN model

        Args:
            n_class (int): number of class in the data set.
            loss_type (string): name of the loss to use.
            l2_reg (float): l2 regularization parameter.
        """

        # placeholder for the input
        self.labels = tf.placeholder(dtype=tf.int32,
                                     shape=[None],
                                     name='labels')

        ## training parameters
        with tf.variable_scope("training_param") as scope:
            ## l2 regularization parameter
            self.l2_reg = tf.get_variable(
                    name='l2_reg',
                    dtype=self.layer_list[0].dtype,
                    trainable=False,
                    initializer=l2_reg)

            self._l2_reg = l2_reg

            self.buffer_l2_reg = tf.placeholder(dtype=self.layer_list[0].dtype,
                                                shape=[],
                                                name='buffer_l2_reg')

            self.update_l2_reg = tf.assign(self.l2_reg, self.buffer_l2_reg)

            ## bias scaling factor
            self.bias_scale = tf.get_variable(
                    name='bias_scale',
                    dtype=self.layer_list[0].dtype,
                    trainable=False,
                    initializer=BIAS_SCALE)

            self._bias_scale = BIAS_SCALE

            self.buffer_bias_scale = tf.placeholder(dtype=self.layer_list[0].dtype,
                                                    shape=[],
                                                    name='buffer_bias_scale')

            self.update_bias_scale = tf.assign(self.bias_scale,
                                               self.buffer_bias_scale)

        ## fully connected layer
        with tf.variable_scope("full_connect") as scope:

            ## output of CKN model
            _, out_width, out_height, \
                    out_channels = self.output.get_shape().as_list()
            out_dim = out_width * out_height * out_channels
            self.output_fc = tf.reshape(self.output,
                                        shape=[-1, out_dim],
                                        name='output_fc')

            ## linear coefficients
            self.W = tf.get_variable(
                    name='W',
                    dtype=self.layer_list[-1].dtype,
                    shape=[out_dim, n_class],
                    trainable=True,
                    initializer=tf.truncated_normal_initializer(stddev=0.01))

            ## intercept
            self.b = tf.get_variable(
                    name='b',
                    dtype=self.layer_list[-1].dtype,
                    shape=[n_class],
                    trainable=True,
                    initializer=tf.constant_initializer(0.0))

            ## linear predictor
            self.predictor = tf.matmul(self.output_fc, self.W) + self.b
            self.predictor = FC_SCALE * self.predictor

        with tf.name_scope('supervised_training') as scope:

            ## definition of the loss
            with tf.name_scope('loss') as scope:
                self.loss = self.loss_op(n_class=n_class, type='squared_hinge')
                # tf.summary.scalar('total_loss', self.loss)

            ## external computation of W and b with miso
            with tf.name_scope('external_full_connect') as scope:
                # update the values of coefficients with miso
                self.W_ext = tf.placeholder(self.W.dtype,
                                            shape=self.W.shape)
                self.b_ext = tf.placeholder(self.b.dtype,
                                            shape=self.b.shape)
                self.update_W = tf.assign(self.W, self.W_ext, name="update_W")
                self.update_b = tf.assign(self.b, self.b_ext, name="update_b")

            ## computing accuracy on training set
            with tf.name_scope('validation') as scope:
                self.train_acc = tf.reduce_mean(tf.to_float(
                                        tf.nn.in_top_k(self.predictor,
                                                       self.labels, 1)),
                                        name="tr_acc")
                # tf.summary.scalar('train_acc', self.train_acc)

            ## computing accuracy on test set
            with tf.name_scope('evaluation') as scope:
                ## computing accuracy on test set
                self.test_acc = tf.reduce_mean(tf.to_float(
                                        tf.nn.in_top_k(self.predictor,
                                                       self.labels, 1)),
                                        name="te_acc")
                # tf.summary.scalar('test_acc', self.test_acc)


    def loss_op(self, n_class, type='squared_hinge'):
        """Definition of l2-penalized loss

        Args:
            n_class (int): number of class in the data set.
            predictor (1-D tensor): linear combination of output features.
            type (string): type of loss.
        """
        with tf.name_scope('data_loss') as scope:
            if type == 'cross_entropy':
                # loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
                #             labels=self.labels, logits=self.predictor)
                raise NotImplementedError("'cross-entropy' loss is not "
                                            + "implemented yet")
            elif type == 'squared_hinge':
                loss = squared_hinge(self.labels, self.predictor, n_class)
            else:
                raise NotImplementedError("only 'squared_hinge' loss is "
                                            + "implemented at the moment.")

            data_loss = tf.reduce_mean(loss, name='data_loss')

        with tf.name_scope('l2_penalty') as scope:
            penalty = self.l2_reg * tf.nn.l2_loss(FC_SCALE * self.W,
                                            name='pen_W') \
                                            / n_class \
                        + self.l2_reg * tf.nn.l2_loss(FC_SCALE * self.b,
                                                name="pen_b") \
                                                / ((self.bias_scale ** 2) * n_class)

        ## full loss
        return tf.add(data_loss, penalty, name='full_loss')


    def encode_optim(self, sample_size, learning_rate=5.0):
        """encode the nodes regarding optimization for the supervised training

        Args:
            sample_size (int): size of the sample feeded to the training
                algorithm (defines the size of an epoch and fix the decay
                step of the learning rate every 100 epochs).

        """
        with tf.name_scope('optimize') as scope:
            self.global_step = tf.contrib.framework.get_or_create_global_step()
            lr = tf.train.exponential_decay(learning_rate=learning_rate/self.batch_size,
                                        global_step=self.global_step,
                                        decay_steps=100 * sample_size // self.batch_size,
                                        decay_rate=0.1,
                                        staircase=True)
            tf.summary.scalar('learning_rate', lr)
            optimizer = tf.train.MomentumOptimizer(learning_rate=lr,
                                                   momentum=0.9)

            self.sgd_op = optimizer.minimize(self.loss, self.global_step)


    def normalize_filter_op(self):
        """define a node that contains all filter normalization op for each
        layer in the model
        """
        updates = []

        for layer in self.layer_list:
            ## projection of the filters on the sphere
            layer.sphere_proj_op()
            updates.append(layer.normalize_Z)

        return tf.group(*updates)


    def compute_sqrt_inv_op(self):
        """define a node that contains all matrix squared-root inverse op
            for each layer in the model
        """
        updates = []

        for layer in self.layer_list:
            ## projection of the filters on the sphere
            layer.sqrt_inv_op()
            updates.append(layer.update_sqrt_inv_kZtZ)

        return tf.group(*updates)


    def initialize_variables(self):
        """initialize TensorFlow variables used in supervised mode

        To be called before 'initialize_filters' and before 'sup_train'
        methods.
        """
        logging.info("Initializing variables")
        ## global initializer
        with tf.name_scope('init') as scope:
            self.init = tf.global_variables_initializer()
        self.session.run(self.init)
        ## saver
        self.saver = tf.train.Saver([self.W, self.b]
                                    + [layer.Z for layer in self.layer_list]
                                    + [layer.sqrt_inv_kZtZ for layer in self.layer_list],
                                    max_to_keep=2)


    def initialize_filters(self, input_data, output_dir, run_options=None,
                           writer=None, timeline=None, tag=None,
                           continue_run=False, n_iter_kmeans=0):
        """init the filters in the CKN model with the k-means
        (in supervised case)

        equivalent to `unsup_train`

        Args:
            input_data (4-D numpy array): data for the training.
            output_dir (string): directory where to save the filters trained
                with k-means.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.
            tag (string): a tag to identify a previous run in the 'output_dir'
                directory. Default is None.
            continue_run (boolean): initialize with filter values computed
                from a previous run either identified by 'tag' (if provided) or
                stored in a "train_<date>" sub-directory in the 'output_dir'
                folder. Default is False.
            n_iter_kmeans (int): number of iterations in spherical k-means.
                If set to 0, only a sub-sampling and random initialization
                of cluster centers are done. Default is 0. In supervised mode,
                it saves time and does not impact results.

        """

        if (tag is not None) and continue_run:
            tmp_output_dir = glob(os.path.join(output_dir, tag + "_train_*"))[-1]
        elif continue_run:
            tmp_output_dir = glob(os.path.join(output_dir, "train_*"))[-1]
        else:
            tmp_output_dir = output_dir

        try:
            if continue_run:
                ## check for a tf saver
                file_name = os.path.join(tmp_output_dir, "checkpoint")
                self.saver.restore(self.session, file_name)
            else:
                raise Exception
        except:
            logging.info("No previous session to restore "
                            + "or restoration failed "
                            + "=> trying restoration from pickled file")
            file_name = os.path.join(output_dir, "filters.pkl")

            try:
                filter_list = ckntf.input.unpickle_data(file_name)
                self.update_filters(filter_list,
                                    run_options=run_options,
                                    writer=writer, timeline=timeline)
                logging.info("filters updated from {}".format(file_name))
            except:
                logging.info("no initial values for filters saved in {}: "
                                .format(file_name)
                                + "filters were recomputed")
                logging.info("number of iteration in spherical k-means = {}".
                                format(n_iter_kmeans))
                if n_iter_kmeans == 0:
                    logging.info("NOTE: only random initialization is performed")
                self.unsup_train(input_data, run_options=run_options,
                                 writer=writer, timeline=timeline,
                                 n_iter_kmeans=n_iter_kmeans)
                filter_list = self.get_filters(
                                        run_options=run_options,
                                        writer=writer, timeline=timeline)
                ckntf.core.utils.mkdir(output_dir)
                ckntf.input.pickle_data(filter_list, file_name)


    def sup_train(self, output_dir, input_data, input_labels,
                  test_data, test_labels, n_iter=70000,
                  check_point=None, batch_size_eval=500,
                  alternate_miso=True,
                  run_options=None, writer=None, timeline=None, tag=None):
        """supervised training of the CKN model in the unsupervised case

        Args:
            output_dir (string): directory to store Tensorflow metadata.
            input_data (4-D numpy array): data for the training.
            input_labels (1-D numpy array): data labels for the training.
            test_data (4-D numpy array): data for testing.
            test_labels (1-D numpy array): data labels for testing.
            n_iter (int): maximum number of iterations.
            check_point (int): number of iterations between two checkpoints,
                i.e. filter saving. If None, no check pointing is done.
                Default is None. When checkpointing, intermediate results are
                stored in a "train_<date>" sub-directory in the 'output_dir'
                folder.
            batch_size_eval (int): batch size for testing.
            alternate_miso (bool): indicates whether a miso training step is
                performed every 400 iterations (if True). If False, a miso
                training step is only performed at the beginning of the
                training alogorithm.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.
            tag (string): a tag to identify the run (metadata and results)
                in the 'output_dir' directory. If None, the tag is
                automatically generated and trained filters are saved in
                a "train_<date>" sub-directory in the 'output_dir' folder.
                Default is None.

        """

        ## online whitening
        if self.online_whitening:
            start = timer()
            input_data = whiten_images(input_data,
                                       patch_height=self._layer0_filter_height,
                                       patch_width=self._layer0_filter_width,
                                       padding=self.layer_list[0].padding,
                                       reconstruct_image=False)
            end = timer()
            logging.debug("input whitening done in {:.4f} sec, shape = {}"
                            .format(end - start, input_data.shape))

        ## training OP
        with tf.name_scope('training_op') as scope:
            with tf.control_dependencies([self.sgd_op]):
                with tf.name_scope('proj_updates') as scope:
                    # normalize filters
                    self.normalize_filters = self.normalize_filter_op()

                with tf.control_dependencies([self.normalize_filters]):
                    with tf.name_scope('matrix_sqrt_inv') as scope:
                        # compute matrix squared-root inverse
                        self.compute_sqrt_inv = self.compute_sqrt_inv_op()

                    train_op = [self.compute_sqrt_inv]
                    self.train_op = tf.group(*train_op, name="train_op")


        run_name = 'train_' + datetime.now().strftime("%Y%m%d_%H%M%S.%f")

        if tag is not None:
            run_name = tag + "_" + run_name

        tmp_output_dir = os.path.join(output_dir, run_name)
        ckntf.core.utils.mkdir(tmp_output_dir)

        ## recording for visualization in tensorboard
        if run_options is None:
            run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE,
                                        output_partition_graphs=True)

        writer_is_None = False
        if writer is None:
            writer_is_None = True
            writer = tf.summary.FileWriter(os.path.join(output_dir, run_name),
                                           self.session.graph)

        ## op to merge all collected summaries
        merge_summary = tf.summary.merge_all()

        ## data batches
        train_set = BatchData(sample_size=input_data.shape[0],
                              batch_size=self.batch_size, shuffle=True,
                              respect_size=False)

        ## store original value of l2_reg and bias_scale
        l2_reg_original = self._l2_reg
        bias_scale_original = self._bias_scale
        sample_size = input_data.shape[0]

        ## run
        t0 = timer()

        for step in range(n_iter):
            epoch = step * self.batch_size / input_data.shape[0]
            # eval on training + train last layer with miso
            if (step == 0) or (alternate_miso and step % 400 == 0):
                logging.info("**********************************")
                logging.info("**** Step {}: training last layer with miso"
                                .format(step))
                ## output of fully connected layers
                X = self.get_output_fc(input_data, run_options=run_options,
                                       writer=writer, timeline=timeline)

                self._bias_scale = np.mean(np.sqrt(np.mean(np.power(X,2), axis=1)))
                self.session.run(self.update_bias_scale,
                                 feed_dict={self.buffer_bias_scale: self._bias_scale})

                self._l2_reg = l2_reg_original * self._bias_scale**2 * X.shape[1]
                self._l2_reg = self._l2_reg / X.shape[0]
                self.session.run(self.update_l2_reg,
                                 feed_dict={self.buffer_l2_reg: self._l2_reg})

                logging.info("bias scaling factor = {:.6E}".format(self._bias_scale))
                logging.info("l2 regularization parameter = {:.6E}"
                                .format(self._l2_reg))
                if DEBUG:
                    Xtest = self.get_output_fc(test_data, run_options=run_options,
                                               writer=writer, timeline=timeline)
                else:
                    Xtest = None
                Wnew, bnew, loss = _train_miso(X=X, Y=input_labels,
                                               Xtest=Xtest, Ytest=test_labels,
                                               Lambda=self._l2_reg,
                                               bias_scale=self._bias_scale,
                                               verbose=DEBUG)
                run_metadata = tf.RunMetadata()
                tensor_list = [self.update_W, self.update_b]
                logging.debug("computing nodes: {}"
                                .format(list(map(lambda x: x.name, tensor_list))))
                self.session.run(tensor_list,
                                 feed_dict={self.W_ext: Wnew / FC_SCALE,
                                            self.b_ext: bnew / FC_SCALE},
                                 options=run_options,
                                 run_metadata=run_metadata)
                # writer.add_run_metadata(run_metadata, "full_connect_layer_{}"
                #                                         .format(step))
                if timeline is not None:
                    timeline.update_timeline(run_metadata)

                t = timer()

                train_loss, train_acc = self._run_from_batch(
                                                [self.loss, self.train_acc],
                                                input_data=input_data,
                                                input_labels=input_labels,
                                                batch_size=batch_size_eval,
                                                debug=DEBUG,
                                                run_options=run_options,
                                                writer=writer,
                                                timeline=timeline,
                                                tag="miso_train_loss_and_acc")
                train_loss = np.mean(train_loss)
                train_acc = np.mean(train_acc)
                logging.info("\tepoch {:.4f}\n".format(epoch)
                                + "\ttrain loss = {:.4f}\n".format(train_loss)
                                + "\ttrain acc = {:.4f}\n".format(train_acc)
                                + "\teval time = {:.4f}\n".format(timer() - t)
                                + "\ttotal time = {:.4f}".format(timer() - t0))

            # evaluate on train/test data
            if step % ((sample_size // self.batch_size)
                + 1 * (sample_size % self.batch_size != 0)) == 0:
                logging.info("**********************************")
                logging.info("**** Step {}: ".format(step)
                                + "evaluation on test set")
                t = timer()
                test_acc = self._run_from_batch(self.test_acc,
                                                input_data=test_data,
                                                input_labels=test_labels,
                                                batch_size=batch_size_eval,
                                                debug=DEBUG,
                                                run_options=run_options,
                                                writer=writer,
                                                timeline=timeline,
                                                tag="eval_test_acc")
                test_acc = np.mean(test_acc)
                logging.info("\tepoch {:.4f}\n".format(epoch)
                                + "\ttest acc = {:.4f}\n".format(test_acc)
                                + "\teval time = {:.4f}\n".format(timer() - t)
                                + "\ttotal time = {:.4f}".format(timer() - t0))
                writer.add_summary(tf.Summary(
                        value=[tf.Summary.Value(tag='test_acc',
                                                simple_value=test_acc)]),
                                   step)

            # SGD step + compute loss
            if step % ((sample_size // (2*self.batch_size))
                + 1 * (sample_size % self.batch_size != 0)) == 0:
                logging.info("**********************************")
                logging.info("**** Step {}: ".format(step)
                                + "SGD step")
                batch_data, batch_labels = train_set.next_batch(input_data,
                                                                input_labels,
                                                                debug=DEBUG)

                if DEBUG:
                    self._check_filters(run_options=run_options,
                                        writer=writer,
                                        timeline=timeline)
                logging.debug("bias scaling factor = {:.6E}".format(self._bias_scale))
                logging.debug("l2 regularization parameter = {:.6E}"
                                .format(self._l2_reg))

                run_metadata = tf.RunMetadata()
                tensor_list = [self.train_op, self.loss, self.train_acc, merge_summary]
                logging.debug("computing nodes: {}"
                                .format(list(map(lambda x: x.name, tensor_list))))
                _, train_loss, train_acc, summary = self.session.run(
                                tensor_list,
                                feed_dict={self.input: batch_data,
                                           self.labels: batch_labels},
                                options=run_options,
                                run_metadata=run_metadata)
                t = timer()

                writer.add_run_metadata(run_metadata, "sgd_step_{}"
                                                        .format(step))
                writer.add_summary(summary, step)
                train_loss = np.mean(train_loss)
                train_acc = np.mean(train_acc)
                writer.add_summary(tf.Summary(
                        value=[tf.Summary.Value(tag='train_loss',
                                                simple_value=train_loss)]),
                                   step)
                writer.add_summary(tf.Summary(
                        value=[tf.Summary.Value(tag='train_acc',
                                                simple_value=train_acc)]),
                                   step)
                if timeline is not None:
                    timeline.update_timeline(run_metadata)
                logging.info("\tepoch {:.4f}\n".format(epoch)
                                + "\tloss = {:.4f}\n".format(train_loss)
                                + "\ttrain acc = {:.4f}\n".format(train_acc)
                                + "\teval time = {:.4f}\n".format(timer() - t)
                                + "\ttotal time = {:.4f}".format(timer() - t0))

            else:
                if DEBUG or step % 50 == 0:
                    logging.info("**** Step {}: ".format(step)
                                    + "SGD step")
                    logging.debug("bias scaling factor = {:.6E}".format(self._bias_scale))
                    logging.debug("l2 regularization parameter = {:.6E}"
                                    .format(self._l2_reg))

                batch_data, batch_labels = train_set.next_batch(input_data,
                                                                input_labels,
                                                                debug=DEBUG)
                if DEBUG:
                    self._check_filters(run_options=run_options,
                                        writer=writer,
                                        timeline=timeline)

                run_metadata = tf.RunMetadata()
                tensor_list = [self.train_op]
                logging.debug("computing nodes: {}"
                                .format(list(map(lambda x: x.name, tensor_list))))
                _ = self.session.run(tensor_list,
                                     feed_dict={self.input: batch_data,
                                                self.labels: batch_labels},
                                     options=run_options,
                                     run_metadata=run_metadata)
                # writer.add_run_metadata(run_metadata, "sgd_step_{}"
                #                                         .format(step))
                if timeline is not None:
                    timeline.update_timeline(run_metadata)

            if (check_point is not None) and (step % check_point == 0) and (step > 0):
                ## check pointing filters
                file_name = os.path.join(tmp_output_dir, "filters.pkl")
                logging.info("Check-point filters saved in {}".format(file_name))
                filter_list = self.get_filters(
                                        run_options=run_options,
                                        writer=writer, timeline=timeline)
                ckntf.input.pickle_data(filter_list, file_name)
                ## check pointing with tensorflow
                file_name = os.path.join(tmp_output_dir, "checkpoint")
                self.saver.save(self.session, file_name,
                                global_step=self.global_step)

        logging.info("Training is over")

        ## save filters
        file_name = os.path.join(tmp_output_dir, "filters.pkl")
        logging.info("Trained filters saved in {}".format(file_name))
        filter_list = self.get_filters(
                                run_options=run_options,
                                writer=writer, timeline=timeline)
        ckntf.input.pickle_data(filter_list, file_name)
        ## check pointing with tensorflow
        file_name = os.path.join(tmp_output_dir, "checkpoint")
        self.saver.save(self.session, file_name,
                        global_step=self.global_step)

        ## close metadata writer
        if writer_is_None:
            writer.close()




    # def tmp_check_filter(self, filter_list):
    #     def kappa(x):
    #         return np.exp((1/0.6**2) * (x - 1.0))
    #     for ind, filters in enumerate(filter_list):
    #         print("filters {}".format(ind))
    #         print("shape {}".format(filters.shape))
    #         Z = filters.reshape(-1, filters.shape[3])
    #         print("reshape {}".format(Z.shape))
    #         print("norm =\n{}".format(np.linalg.norm(Z, axis=0)))
    #
    #         print("kZtZ =")
    #         ZtZ = np.dot(np.transpose(Z), Z)
    #         kZtZ = kappa(ZtZ)
    #         print(kZtZ)
    #         print("inverse squared root of kZtZ:")
    #         try:
    #             invkZtZ = scipy.linalg.sqrtm(np.linalg.inv(kZtZ
    #                         + 0.01 * np.eye(kZtZ.shape[0])))
    #             print(invkZtZ)
    #         except:
    #             raise
    #         if np.iscomplex(invkZtZ).any():
    #             raise ValueError('inverse squared root is complex')


    #--------------------------------------------------------------------------#
    # GETTER and SETTER
    #--------------------------------------------------------------------------#

    def update_filters(self, filter_list, run_options=None,
                       writer=None, timeline=None):
        """update filters trained from another model

        Args:
            filter_list (list of np.array): list of filter values for each
                layer.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """

        if len(filter_list) != len(self.layer_list):
            raise TypeError("'filter_list' length does not correspond to the "
                            + "the number of layers in the model")

        for i, layer in enumerate(self.layer_list):
            ## filter shape of current layer
            layer_filter_shape = (layer.filter_height,
                                  layer.filter_width,
                                  layer.in_channels,
                                  layer.out_channels)

            ## if shape corresponds
            if (filter_list[i].shape != layer_filter_shape):
                raise TypeValue("Shape does not correspond for filter update "
                                + "in layer {}".format(ind))

            ## update current filter
            current_filter = filter_list[i]
            layer.update_filters(current_filter)
            layer.compute_update_filters(
                        self.session, run_options=run_options,
                        writer=writer, timeline=timeline)


    def get_output(self, input_data, run_options=None,
                   writer=None, timeline=None):
        """return output of the network

        Args:
            input_data (4-D numpy array): data to feed through the model.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """

        if self.online_whitening:
            start = timer()
            input_data = whiten_images(input_data,
                                       patch_height=self._layer0_filter_height,
                                       patch_width=self._layer0_filter_width,
                                       padding=self.layer_list[0].padding,
                                       reconstruct_image=False)
            end = timer()
            logging.debug("input whitening done in {:.4f} sec, shape = {}"
                            .format(end - start, input_data.shape))

        output = self._run_from_batch(self.output, input_data,
                                      input_labels=None,
                                      batch_size=None,
                                      debug=DEBUG,
                                      run_options=run_options,
                                      writer=writer,
                                      timeline=timeline,
                                      tag="get_output")
        return output[0]


    def get_filters(self, run_options=None, writer=None, timeline=None):
        """return a list of the filters as numpy array

        Args:
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """
        filter_list = []
        for i, layer in enumerate(self.layer_list):
            filter_list.append(layer.get_filters(
                                        self.session,
                                        run_options=run_options,
                                        writer=writer, timeline=timeline))

        return filter_list


    def get_output_fc(self, input_data, run_options=None,
                      writer=None, timeline=None):
        """return output of the last fully connected layers

        Args:
            input_data (4-D numpy array): data to feed through the model.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """

        if self.online_whitening:
            start = timer()
            input_data = whiten_images(input_data,
                                       patch_height=self._layer0_filter_height,
                                       patch_width=self._layer0_filter_width,
                                       padding=self.layer_list[0].padding,
                                       reconstruct_image=False)
            end = timer()
            logging.debug("input whitening done in {:.4f} sec, shape = {}"
                            .format(end - start, input_data.shape))

        output = self._run_from_batch(self.output_fc, input_data,
                                      input_labels=None,
                                      batch_size=None,
                                      debug=DEBUG,
                                      run_options=run_options,
                                      writer=writer, timeline=timeline,
                                      tag="get_output_fc")

        return output[0]


    def compute_loss(self, input_data, input_labels,
                     run_options=None, writer=None, timeline=None):
        """compute the loss associated to the model

        Args:
            input_data (4-D numpy array): data to feed through the model.
            input_labels (4-D numpy array): data laebls to feed
                through the model.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        Returns:
            Average value of the loss.

        """
        loss = self._run_from_batch(self.loss, input_data, input_labels,
                                    batch_size=None, debug=DEBUG,
                                    run_options=run_options,
                                    writer=writer, timeline=timeline,
                                    tag="compute_loss")

        return np.mean(loss[0])


    #--------------------------------------------------------------------------#
    # INTERNAL MEMBER FUNCTIONS
    #--------------------------------------------------------------------------#

    def _run_from_batch(self, tensor_list, input_data, input_labels=None,
                        batch_size=None, debug=True,
                        run_options=None, writer=None, timeline=None,
                        tag=None):
        """Compute specified nodes in the Tensorflow computational graph

        Args:
            tensor_list (list of tensors): list of nodes from the Tensorflow
                computational graph that should be computed, can be a single
                tensor.
            input_data (4-D numpy array): data that will be fed to
                placeholder `input`.
            input_labels (1-D numpy array): data labels that will be fed to
                placeholder `labels`, not used if None, default is None.
            batch_size (int): to set batch size to be considered on the fly,
                if None self.n_batch is used, default is None.
            debug (boolean): should debug message be printed.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.
            tag (string): a tag for the metadata. Default is None and the tag
                is automatically generated.

        Returns:
            list of np.arrays or scalars.

        """

        if tag is None:
            tag = datetime.now().strftime("%Y%m%d_%H%M%S.%f") \
                    + str(np.random.randint(0,100000))
        else:
            tag = tag + "_" + datetime.now().strftime("%Y%m%d_%H%M%S.%f") \
                    + str(np.random.randint(0,100000))

        if not isinstance(tensor_list, (tuple, list)):
            tensor_list = [tensor_list]

        logging.debug("computing nodes: {}"
                        .format(list(map(lambda x: x.name, tensor_list))))

        if batch_size is None:
            batch_size = self.batch_size

        output = [[] for i in range(len(tensor_list))]

        ## data batch
        batch_set = BatchData(sample_size=input_data.shape[0],
                              batch_size=batch_size, shuffle=False,
                              respect_size=True)

        ## extract patches by batch
        for i_batch in range(0,(batch_set.n_batch)):

            batch_data, \
            batch_labels = batch_set.next_batch(input_data, input_labels,
                                                debug=debug)

            ## feed placeholder
            feed_dict = {self.input: batch_data} \
                            if input_labels is None \
                            else {self.input: batch_data,
                                  self.labels: batch_labels}
            run_metadata = tf.RunMetadata()
            tmp = self.session.run(tensor_list,
                                   feed_dict=feed_dict,
                                   options=run_options,
                                   run_metadata=run_metadata)
            # if writer is not None:
            #     writer.add_run_metadata(run_metadata, "run_from_batch_"
            #                                             + str(i_batch) + tag)

            if timeline is not None:
                timeline.update_timeline(run_metadata)

            for ind in range(len(output)):
                if (not isinstance(tmp[ind], np.ndarray)) or tmp[ind].ndim == 0:
                    tmp[ind] = np.expand_dims(tmp[ind], axis=0)

                output[ind].append(tmp[ind])

        ## concatenate each results
        for ind in range(len(output)):
            output[ind] = np.concatenate(output[ind], axis=0)

        return output


    ### check filters
    def _check_filters(self, run_options=None,
                       writer=None, timeline=None):
        """check for null, inf or NA filters of each layers

        Args:
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """
        if logging.getLevelName(logging.getLogger().getEffectiveLevel()) \
            == 'DEBUG':
            for ind,layer in enumerate(self.layer_list):
                logging.debug("Checking filters from layer {}"
                                .format(ind+1))
                layer.check_filters(self.session,
                                    run_options=run_options,
                                    writer=writer, timeline=timeline)


#------------------------------------------------------------------------------#
# INTERNAL FUNCTIONS
#------------------------------------------------------------------------------#

def _logging_batch_id(i_batch, n_batch):
    """logging message for debugging when running through batches

    Args:
        i_batch (int): index of the current batch.
        n_batch (int): total number of batches.
    """
    n_message = 20
    interval = n_batch // n_message + 1 * (n_batch % n_message != 0)
    if (i_batch % interval == 0) or (i_batch+1 == n_batch):
        logging.debug("batch_index = {} / {}".format(i_batch+1, n_batch))


def _train_miso(X, Y, Xtest=None, Ytest=None,
                Lambda=0.01, bias_scale=BIAS_SCALE, verbose=False):
    """training a miso classifier

    Args:
        X (4-D numpy array): input data.
        Y (1-D numpy array): input data labels.
        Xtest (4-D numpy array): input data for test. Default is None for no
            validation.
        Ytest (1-D numpy array): input data labels for test. Default is None
            for no validation.
        Lambda (float): regularization parameter. Default is 1e-2.
        bias_scale (float): scaling factor for the bias (intercept). Default
            value is given by `BIAS_SCALE` global variable.
        verbose (bool): set verbosity. Default is false. No evaluation is done
            if false.
    """
    logging.debug("Input dimension X.shape = {}".format(X.shape))
    logging.debug("Input dimension Y.shape = {}".format(Y.shape))
    Xin = np.hstack([X, bias_scale * np.ones((X.shape[0], 1), dtype=X.dtype)])
    Yin = Y.astype(np.float32).squeeze()

    logging.debug("training miso svm with lambda = {:.6E}".format(Lambda))

    clf = miso_svm.MisoClassifier(Lambda, eps=1e-4,
                                  max_iterations=100 * Xin.shape[0],
                                  verbose=False)
    clf.fit(Xin, Yin)

    W = clf.W[:,:-1].T
    b = bias_scale * clf.W[:,-1]

    logging.debug("W shape = {}".format(W.shape))
    logging.debug("b shape = {}".format(b.shape))

    if verbose and Xtest is not None and Ytest is not None:
        Xtein = np.hstack([Xtest, bias_scale * np.ones((Xtest.shape[0], 1), dtype=Xtest.dtype)])
        Ytein = Ytest.astype(np.float32).squeeze()

        activations = clf.W.dot(Xtein.T)
        predictions = np.argmax(activations, axis=0)
        accuracy = 1 - (np.count_nonzero(predictions - Ytein) / Ytein.shape[0])
        logging.info("test accuracy in miso-train = {:.4f}".format(accuracy))

    return W, b, clf.losses


#------------------------------------------------------------------------------#
# INTERNAL CLASSES
#------------------------------------------------------------------------------#

class BatchData(object):
    """create batches of data

    Attributes:
        batch_size (int): size of the batches.
        sample_size (int): number of samples in the data.
        n_batch (int): number of batches to consider.
        perm (1-D np.array): permutation of the samples (if required).
        i_batch (int): current batch index.
        shuffle (bool): indicates if data should be shuffled between each epoch.
        respect_size (bool): indicates what happens for the last batch if
            the `sample_size` cannot be devided by `batch_size`. If True the
            last batch size will be adapted so that all data are used only once.
            If False, the last batch will be completed by data from the first
            batch so that all batches have the same size.

    """

    def __init__(self, sample_size,
                 batch_size, shuffle=False,
                 respect_size=True):
        """Constructor of BatchData object

        Args:
            sample_size (int): number of samples in the data.
            batch_size (int): size of the batches.
            shuffle (boolean): should the data be shuffled before the batch
                creation, default is False.
            respect_size (bool): indicates what happens for the last batch if
                the `sample_size` cannot be devided by `batch_size`. If True the
                last batch size will be adapted so that all data are used only once.
                If False, the last batch will be completed by data from the first
                batch so that all batches have the same size.

        """
        self.sample_size = sample_size
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.respect_size = respect_size

        if self.batch_size > self.sample_size:
            logging.warning("batch_size > input dimension, calling `next_batch` "
                            + "will return all data")

        if (self.batch_size is None) or (sample_size <= self.batch_size):
            self.n_batch = 1
        else:
            self.n_batch = sample_size // self.batch_size \
                        + 1 * (sample_size % self.batch_size != 0)

        self.perm = np.arange(self.sample_size)

        self.i_batch = 0 ## to start at 0


    def next_batch(self, input_data, input_labels=None, debug=True):
        """get next batch of data

        Args:
            input_data (4-D numpy array): input data.
            input_labels (1-D numpy array): input data labels, default is None.
            debug (boolean): indicates if a logging message regarding the
                current batch id should be prompted or not.

        Returns:
            batch_input (4-D numpy array): next batch of data.
            batch_labels (1-D numpy array): next batch of data labels, can be None
                if labels given in input are None.
        """
        assert self.sample_size == input_data.shape[0]
        if input_labels is not None:
            assert self.sample_size == input_labels.shape[0]

        ## reshuffling after each epoch
        if self.i_batch == 0 and self.shuffle:
            self.perm = np.random.permutation(self.sample_size)

        if debug:
            _logging_batch_id(self.i_batch, self.n_batch)

        i_start = self.i_batch * self.batch_size
        i_end = (self.i_batch + 1) * self.batch_size

        # case of the last batch if sample_size cannot be devided by batch_size
        if i_end > self.sample_size:
            if self.respect_size:
                batch_index = np.r_[i_start:self.sample_size]
            else:
                batch_index = np.r_[i_start:self.sample_size,
                                    0:(i_end - self.sample_size)]
        else:
            batch_index = np.r_[i_start:i_end]

        batch_data = input_data[self.perm[batch_index],]
        batch_labels = input_labels[self.perm[batch_index],] \
                                if input_labels is not None \
                                else None

        self.i_batch = self.i_batch + 1 if self.i_batch < self.n_batch \
                                        else 0

        return batch_data, batch_labels
