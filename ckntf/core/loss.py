"""Definition of the different losses used in supervised mode"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import tensorflow as tf

def squared_hinge(labels, predictor, n_class):
    """Compute squared Hinge loss

    Args:
        labels (1-D tensor): label of each sample.
        predictor (2-D tensor): linear predictor values (i.e. linear
            combination of features) for each sample.
    """
    y = 2 * tf.one_hot(labels, depth=n_class) - 1
    hinge = tf.maximum(0., 1. - y * predictor)
    return 0.5 * tf.square(hinge, name='squared_hinge')
