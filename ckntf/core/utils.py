"""Utilitary functions and objects for the ckn package"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

from datetime import datetime
import json
import logging
import os
import pkg_resources
import tensorflow as tf
from tensorflow.python.client import timeline

import ckntf.core


#------------------------------------------------------------------------------#
#
#------------------------------------------------------------------------------#

def format_params(filter_dim=[3],
                    out_channels=[64],
                    subsampling=[2],
                    kernel=['exp0'],
                    kernel_param=[0.6],
                    name=["layer1"],
                    n_sampled_patches=1000000,
                    dtype=tf.float32, padding="SAME",
                    epsilon_norm=1e-5, epsilon_inv=1e-4):
    """format network parameters from separate lists into a list of dictionaries
    describing the layers

    Args:
        filter_dim (list of int): filter dimension in pixels.
        out_channels (list of int): output dimension of each layer,
            i.e. number of filters.
        subsampling (list of int): sampling factor in linear pooling.
        kernel (list string): non-linearity function name.
        kernel_param (list of float or double): hyper-parameters for the
            non-linearity function.
        name (list string): name of each layer.
        n_sampled_patches (int): number of patches sampled across all
            images for the spherical k-means initialization.
            Common to all layers.
        dtype (tf.dtype): type of the data (tf.float32, etc.). Common to
            all layers.
        padding (string): type of padding, "SAME" = 0-pading,
            "VALID" = no padding. Common to all layers.
        epsilon_norm (float or double): regularization parameter for
            normalization. Common to all layers.
        epsilon_inv (float or double): regularization parameter for
            inversion. Common to all layers.

    Returns: list of dictionaries with keywords
        'filter_height' (int): filter dimension (height) in pixels.
        'filter_width' (int): filter dimension (width) in pixels.
        'out_channels' (int): output dimension of the layer, i.e. number of
            filters.
        'subsampling' (int): sampling factor in linear pooling.
        'n_sampled_patches' (int): number of patches sampled across all
            images for the spherical k-means initialization.
        'kernel' (string): non-linearity function name.
        'kernel_param' (float or double): hyper-parameters for the
            non-linearity function.
        'dtype' (tf.dtype): type of the data (tf.float32, etc.).
        'padding' (string): type of padding, "SAME" = 0-pading,
            "VALID" = no padding.
        'name' (string): name of the layer.
        'epsilon_norm' (float or double): regularization parameter for
            normalization.
        'epsilon_inv' (float or double): regularization parameter for
            inversion.

    """
    n_layer = len(filter_dim)
    if set([len(filter_dim), len(out_channels),
            len(subsampling), len(kernel),
            len(kernel_param), len(name)]) != {n_layer}:
        raise ValueError("Different input length")

    params = []
    for ind in range(n_layer):
        params.append({
            'filter_height': filter_dim[ind], 'filter_width': filter_dim[ind],
            'out_channels': out_channels[ind], 'subsampling': subsampling[ind],
            'n_sampled_patches': n_sampled_patches,
            'kernel': kernel[ind], 'kernel_param': kernel_param[ind],
            'dtype': dtype, 'padding': padding, 'name': name[ind],
            'epsilon_norm': epsilon_norm, 'epsilon_inv': epsilon_inv
        })
    return params


#------------------------------------------------------------------------------#
# INTERNAL OBJECTS
#------------------------------------------------------------------------------#

class obj(object):
    """transform a dictionary to an object

    Also transform inner dictionaries to objects
    """
    def __init__(self, d):
        for a, b in d.items():
            if isinstance(b, (list, tuple)):
               setattr(self, a, [obj(x) if isinstance(x, dict)
                                 else x for x in b])
            else:
               setattr(self, a, obj(b) if isinstance(b, dict) else b)


#------------------------------------------------------------------------------#
# UTILS FUNCTIONS
#------------------------------------------------------------------------------#

def mkdir(dir):
    """safe mkdir that pass the exception raised when the directory exists

    Args:
        dir (string): name of the directory to be created

    """
    try:
        os.mkdir(dir)
    except:
        logging.warning("'{}' already exists".format(dir))
        pass


def outdir(tag, in_place=False):
    """create a sub-directroy in 'ckntf/output' with current date + training

    Args:
        tag (string): a name to give to the sub-directory.
        in_place (bool): if True, create a sub-directroy in './output',
            if False, create a sub-directroy in 'ckntf/output'. Default is False.

    Returns:
        The name of the created directory.

    """
    ## sub-directory name
    output_dir = None
    if in_place:
        output_dir = os.path.join('output',
                     datetime.now().strftime("%Y%m%d_%H:%M")
                     + "_" + tag)
    else:
        output_dir = os.path.join(
                        pkg_resources.resource_filename('ckntf', 'output'),
                        datetime.now().strftime("%Y%m%d_%H:%M")
                        + "_" + tag)
    ## create the sub-directory
    ckntf.core.utils.mkdir(output_dir)
    ## return
    return output_dir


#------------------------------------------------------------------------------#
# PROFILING
#------------------------------------------------------------------------------#

class TimeLiner (object):
    """Object to concatenate timeline events from multiple session runs

    Adapted from the work of Illarion Khlestov
    (https://medium.com/@illarionkhlestov/)
    available at <https://github.com/ikhlestov/tensorflow_profiling>

    Attributes:
        _timeline_dict: dictionary storing the events from
            the generated time line.

    """

    def __init__(self):
        self._timeline_dict = None

    def update_timeline(self, run_metadata):
        """Add the event from a session run to the dictionary of events.

        Args:
            run_metadata: A [RunMetadata] protocol buffer passed to tf Session
                running.
        """
        # create a timeline from the metadata run
        fetched_timeline = timeline.Timeline(run_metadata.step_stats)
        chrome_trace = fetched_timeline.generate_chrome_trace_format(
                                            show_memory=True)
        # convert chrome trace to python dict
        chrome_trace_dict = json.loads(chrome_trace)
        # for first run store full trace
        if self._timeline_dict is None:
            self._timeline_dict = chrome_trace_dict
        # for other - update only time consumption, not definitions
        else:
            for event in chrome_trace_dict['traceEvents']:
                # events time consumption started with 'ts' prefix
                # if 'ts' in event:
                self._timeline_dict['traceEvents'].append(event)

    def save(self, file_name):
        """save the timeline in a json file

        Args:
            file_name (string): name of the file to store the timeline
                of events.

        """
        with open(file_name, 'w') as f:
            json.dump(self._timeline_dict, f)
