"""running test on the CKN model class in supervised mode"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
                "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import logging
import sys
logging.basicConfig(stream=sys.stdout,
                    format='%(levelname)s:%(message)s',
                    level=logging.INFO)

from timeit import default_timer as timer
from pprint import pprint

import pkg_resources

import tensorflow as tf
import numpy as np
import math
import ckntf.core
import ckntf.input
from ckntf.core.utils import obj

class args:
    ## data
    sample_size = 1000
    batch_size = 64
    data_folder = pkg_resources.resource_filename('ckntf', 'data')
    ## online whitening
    online_whitening = False
    ## tensorboard
    output_dir = 'test_supervised_mode'
    ## regularization parameter
    l2_reg = 1e-1
    ## learning rate
    learning_rate = 10
    ## tag
    tag = "test"

## CKN model parameters
params = [
    {
        'filter_height': 3, 'filter_width': 3,
        'out_channels': 512, 'subsampling': 2,
        'n_sampled_patches': 1000, 'kernel': 'exp0', 'kernel_param': 0.6,
        'dtype': tf.float32, 'padding': "SAME", 'name': "layer1",
        'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
    }
]


## for tensorboard
print("For tensorboard:\n    run `tensorboard --logdir="
      + args.output_dir + "` in a shell"
      + "\n    and go to"
      + "`localhost:6006` on your web-browser")



class CKNmodelTest(tf.test.TestCase):
    def testCKNmodel(self):

        print("Running tests on the CKNmodel class in supervised mode")
        ckntf.core.ckn_layer.DEBUG = False
        ckntf.core.ckn_model.DEBUG = False

        pprint(vars(args))
        pprint(params)

        ### import data
        logging.info("importing data")
        (Xtr, Ytr,
         Xte, Yte) = ckntf.input.load_cifar10(args.data_folder,
                                            args.sample_size,
                                            whitened=not args.online_whitening)
        (N, H, W, C) = Xtr.shape
        n_class = len(set(Ytr))
        logging.info("data imported")

        # data spec
        logging.info("dimensions of the data set ({})".format(Xtr.shape))

        ### run tensorflow
        with self.test_session() as sess:

            # define the model
            model = ckntf.core.CKNmodel.from_param(params, sess, args.batch_size,
                                                 online_whitening=args.online_whitening)

            # encode the model
            print("#### encode the model ####")
            model.encode(H, W, C)
            model.encode_supervised(n_class, loss_type='squared_hinge',
                                    l2_reg=args.l2_reg)
            model.encode_optim(sample_size=Xtr.shape[0],
                               learning_rate=args.learning_rate)

            # initialize tf variables
            model.initialize_variables()

            ##### initialization of the filters with k-means
            print("#### init filters with k-means ####")

            logging.info("START: unsupervised training of the {} layer(s) "
                            .format(len(model.layer_list)) \
                            + "in the model with k-means")
            start = timer()
            model.initialize_filters(input_data=Xtr,
                                     output_dir=args.output_dir,
                                     n_iter_kmeans=0)
            end = timer()
            logging.info("END: training of the {} layers in {:.4f} sec"
                            .format(len(model.layer_list), end - start))

            ##### supervised training
            print("#### supervised training ####")

            logging.info("START: supervised training of the {} layer(s) "
                            .format(len(model.layer_list)))
            start = timer()
            model.sup_train(output_dir=args.output_dir,
                            input_data=Xtr,
                            input_labels=Ytr,
                            test_data=Xte,
                            test_labels=Yte,
                            n_iter=9000,
                            batch_size_eval=200,
                            alternate_miso=False)
            end = timer()
            logging.info("END: training of the {} layers in {:.4f} sec"
                            .format(len(model.layer_list), end - start))


            print("#### tests over ####")


if __name__ == "__main__":
    tf.test.main()
