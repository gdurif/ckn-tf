"""Definition of a CKN layer.

Reference: Mairal, Julien. "End-to-End Kernel Learning with Supervised
Convolutional Kernel Networks." In Advances in Neural Information Processing
Systems, 1399–1407, 2016.
http://papers.nips.cc/paper/6184-end-to-end-kernel-learning-with-supervised-convolutional-kernel-networks.

Input:
    X: a set of images as a tensor [batch_size, in_height, in_width, in_channels]
    Z: the weights of dimension [filter_height, filter_width, in_channels, out_channels]
    kappa: a kernel function

Output:
    Finite dimensional representation with smaller resolution

"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Dexiong Chen, Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

from timeit import default_timer as timer
from datetime import datetime
import logging

import tensorflow as tf
import numpy as np
import math
# import sklearn.preprocessing

from ckntf.core.ops_cc import matrix_inverse_sqrt as _matrix_sqrtinv
from ckntf.core.kernels import kernel_exp
from ckntf.core.kernels import kernel_poly2
from ckntf.core.kernels import kernel_poly02
from ckntf.core.kernels import kernel_poly
from ckntf.core.kernels import kernel_inv_poly
from ckntf.core.kernels import kernel_acos0
from ckntf.core.kernels import kernel_acos1
from ckntf.core.kernels import kernel_acos2
from ckntf.core.utils import obj

import spherical_kmeans

from numbers import Number, Real

DEBUG = False


class CKNlayer(object):
    """Defines a CKN layer.

    Attributes (Tensorflow computation graph):
        input (4-D tensor): batch of images, as a tensor of
            shape [batch_size, in_height, in_width, in_channels].
        input_shape (1-D tensor): shape of input.
        patch_norm (4-D tensor): l2 norm of each filter_height x filter_width
            patch in input.
        output (4-D tensor): transformed batch of images.
        Z (4-D tensor): weights as variables, as a tensor of
            shape [filter_height, filter_width, in_channels, out_channels].
        normalize_Z (tf.assign op): operation to normalize Z, i.e. projection
            onto the unitary sphere.
        update_Z (tf.assign op): operation to update Z.
        update_Z_kmeans (tf.assign op): operation to update Z from the k-means
            in unsupervised case.
        sqrt_inv_kZtZ (2-D tensor): inverse squared root of the matrix
            kappa(ZtZ).
        update_sqrt_inv_kZtZ (tf.assign op): operation to update sqrt_inv_kZtZ
            when Z is modified.
        patches (4-D tensor): set of patches extracted from input.

        ones_filter (4-D tensor): filter of 1 with shape [filter_height,
            filter_width, in_channels, 1], used to compute the norm of each
            patch in input.
        gaussian_filter (2-D tensor): filter for Gaussian subsampling of
            shape [2*subsampling - 1, 2*subsampling - 1].

    Attributes (data container):
        patch_set (numpy array): set of patches that are fed to the kmeans in
            unsupervised case, default is None.

    Attributes (parameters):
        batch_size (int): number of images
        in_height (int): image height.
        in_width (int): image width.
        in_channels (int): number of channels in input.
        filter_height (int): patch height.
        filter_width (int): patch width.
        out_channels (int): number of channels in output.
        subsampling (int): sampling factor in linear pooling.
        n_sampled_patches (int): number of patches sampled across all
            images for the spherical k-means initialization.
        kappa (function): non-linearity function, default is kernel_exp.
        kernel_param (scalar of list): list of hyper-parameters for the
            non-linearity function kappa, None if kappa has no parameter.
        dtype (tensorflow dtype): type of the data (tf.float32, etc.), default
            is tf.float32.
        padding (string): type of padding, "SAME" = 0-pading,
            "VALID" = no padding, default is "SAME".
        name (string): name of the layer, default is "ckn_layer".
        epsilon_norm (float): regularization parameter for normalization,
            default is 1e^-12.
        epsilon_inv (float): regularization parameter for inverse,
            default is 1e^-3.

    """

    def __init__(self, filter_height, filter_width, out_channels, subsampling,
                 n_sampled_patches=10000, kernel='exp', kernel_param=0.7,
                 dtype=tf.float32, padding="SAME",
                 name="ckn_layer", epsilon_norm=1e-12, epsilon_inv=1e-3):
        """Constructor for CKNlayer class

        Args:
            filter_height (int): patch height.
            filter_width (int): patch width.
            out_channels (int): number of channels in output.
            subsampling (int): sampling factor in linear pooling.

        Keyword args:
            n_sampled_patches (int): number of patches sampled across all images
                in unsupervised case, default is 10000.
            kernel (string): non-linearity function to be used,
                default is 'exp'.
            kernel_param (scalar of list): list of hyper-parameters for the
                non-linearity function kappa, None if kappa has no parameter..
            dtype (tensorflow dtype): type of the data (tf.float32, etc.),
                default is tf.float32.
            padding (string): type of padding, "SAME" = 0-pading,
                "VALID" = no padding, default is "SAME".
            name (string): name of the layer, default is "ckn_layer".
            epsilon_norm (float): regularization parameter for normalization,
                default is 1e^-12.
            epsilon_inv (float): regularization parameter for inverse,
                default is 1e^-3.
        """
        ## declare Tensorflow computation graph attributes to None
        self._declare_tensorflow_object()

        ## declare data container
        # first a list of patches, that should be concatenated after extraction
        self.patch_set = []

        ## init parameters
        self._init_layer_param(filter_height, filter_width, out_channels,
                               subsampling, n_sampled_patches,
                               kernel, kernel_param,
                               dtype, padding, name,
                               epsilon_norm, epsilon_inv)


    @classmethod
    def from_param(cls, params):
        """Initialize a layer based on an object storing all parameters

        Args:
            param (dict or object): a dictionary or an object whose
                keys or attributes (respectively) correspond to all
                input arguments of the `__init__` function of the `CKNlayer`
                class.
        """
        if isinstance(params, dict):
            params = obj(params)

        return cls(filter_height=params.filter_height,
                   filter_width=params.filter_width,
                   out_channels=params.out_channels,
                   subsampling=params.subsampling,
                   n_sampled_patches=params.n_sampled_patches,
                   kernel=params.kernel,
                   kernel_param=params.kernel_param,
                   dtype=params.dtype,
                   padding=params.padding,
                   name=params.name,
                   epsilon_norm=params.epsilon_norm,
                   epsilon_inv=params.epsilon_inv)


    ### encode the CKN layer
    def encode(self, input):
        """Encoding of the CKN layer in the Tensorflow computational graph

        Defines all Tensorflow objects, variables and operations necessary
        to the 3 sub-layers (conv, mult and pool)

        Args:
            input (4-D tensor): input of the layer.
        """
        logging.debug("encoding of computational graph for layer \"{}\""
                        .format(self.name))

        with tf.variable_scope(self.name) as scope:
            ## input
            with tf.variable_scope("input") as scope:
                self.input = input
                self.input_shape = tf.shape(self.input, name="input_shape")
                _, self.in_width, \
                   self.in_height, \
                   self.in_channels = self.input.get_shape().as_list()
                logging.debug("Layer \"{}\": input dimensions = {}"
                    .format(self.name, self.input.get_shape().as_list()))

            ## extract patches for unsupervised filter update
            with tf.variable_scope("extract_patches") as scope:
                self._extract_patches()

            ## init variables and objects
            with tf.variable_scope("init") as scope:
                self._init_layer_variables()

            ## compute sqrt_inv_kZtZ
            with tf.name_scope('sqrt_inv_kZtZ') as scope:
                # compute matrix squared-root inverse
                self.sqrt_inv_op()

            ## conv step
            with tf.variable_scope("conv") as scope:
                self.conv_layer()

            ## linear mult step_stats
            with tf.variable_scope("mult") as scope:
                self.mult_layer()

            ## pooling step
            with tf.variable_scope("pool") as scope:
                self.pool_layer()

            self.output = tf.identity(self.output, name='output')
            return self.output


    def _init_layer_variables(self):
        """init Tensorflow objects and variables
        """

        ## filters
        self.ones_filter = tf.constant(value=1,
                                       dtype=self.dtype,
                                       shape=[self.filter_height,
                                              self.filter_width,
                                              self.in_channels,
                                              1],
                                       name='ones_filter')

        g_filter_1d = np.expand_dims(
                            _gaussian_filter_1d(2*self.subsampling + 1,
                                        float(self.subsampling)/math.sqrt(2)),
                            axis=1)
        self.gaussian_filter = tf.constant(value=np.matmul(g_filter_1d,
                                                           g_filter_1d.T),
                                           dtype=self.dtype,
                                           shape=[2*self.subsampling + 1,
                                                  2*self.subsampling + 1],
                                           name='gaussian_filter')

        self.Z = tf.get_variable(name="Z", dtype=self.dtype,
                                 shape=[self.filter_height,
                                        self.filter_width,
                                        self.in_channels,
                                        self.out_channels],
                                 initializer=None,
                                 trainable=True)
        self.tmp_norm = tf.sqrt(tf.reduce_sum(tf.square(self.Z),
                                axis=[0, 1, 2], keep_dims=True))

        ## update operations (defined later)
        self.update_Z = None
        self.update_Z_kmeans = None

        ## kappa(ZtZ)^{-1/2}
        self.sqrt_inv_kZtZ = tf.get_variable(name="sqrt_inv_kZtZ",
                                             dtype=self.dtype,
                                             shape=[self.out_channels,
                                                    self.out_channels],
                                             initializer=None,
                                             trainable=False)

        self.update_sqrt_inv_kZtZ = None

        ## normalize filters (defined later)
        self.normalize_Z = None


    ### projection of the filters on the sphere
    def sphere_proj_op(self):
        """normalize the filters

        Projection on the unitary sphere
        """
        if DEBUG:
            Z = tf.Print(self.Z, [tf.reduce_any(tf.is_nan(self.Z)),
                                  tf.reduce_any(tf.is_inf(self.Z))],
                         message="Layer \"" + self.name + "\": check_Z_before_proj",
                         name="check_Z_before_proj")
        else:
            Z = self.Z

        # Znorm = tf.sqrt(tf.reduce_sum(tf.square(self.Z),
        #                         axis=[0, 1, 2], keep_dims=True))

        normZ = tf.nn.l2_normalize(Z, dim=[0, 1, 2],
                                   epsilon=self.epsilon_norm)

        if DEBUG:
            # normZ = tf.Print(normZ, [tf.reduce_any(tf.is_nan(normZ)),
            #                          tf.reduce_any(tf.is_inf(normZ)),
            #                          Znorm],
            #              message="Layer \"" + self.name + "\": check_normZ",
            #              summarize=Z.get_shape()[3].value,
            #              name="check_normZ")
            normZ = tf.Print(normZ, [tf.reduce_any(tf.is_nan(normZ)),
                                     tf.reduce_any(tf.is_inf(normZ))],
                         message="Layer \"" + self.name + "\": check_normZ",
                         name="check_normZ")
        self.normalize_Z = tf.assign(self.Z, normZ)


    ### matrix squared-root inverse
    def sqrt_inv_op(self):
        """define the matrix squared-root inverse operation
        """
        self.update_sqrt_inv_kZtZ = tf.assign(self.sqrt_inv_kZtZ,
                                              self._compute_sqrt_inv_kZtZ())


    ### compute l2 norm of X
    def _l2_norm(self):
        """Compute l2 norm of X by patches

        For a patch of size [filter_height, filter_width, in_channels],
        compute the l2-norm of the vectorized patch in
        R^{filter_height x filter_width x in_channels}

        Returns:
            4-D tensor of shape [batch_size, in_height, in_width, 1]
                with l2-norm computed for each patch.
        """
        self.patch_norm = tf.maximum(
                            tf.sqrt(tf.nn.conv2d(tf.square(self.input),
                                                 self.ones_filter,
                                                 strides=[1, 1, 1, 1],
                                                 padding=self.padding)),
                            self.epsilon_norm)


    ### conv layer
    def conv_layer(self):
        """Convolution layer

        Compute ||X|| * kappa(Zt X/||X||)
        """
        # Step 1: compute the norm of the patches in each image
        self._l2_norm()
        # step 2: convolution of X by Z
        conv_XZ = tf.nn.conv2d(self.input, self.Z,
                               strides=[1, 1, 1, 1],
                               padding=self.padding)
        # step 3: divide conXZ by the norm of the patches
        conv_XZ = conv_XZ / self.patch_norm
        # step 4: non-linearity
        self.output = self.kappa(conv_XZ, self.kernel_param)
        # step 5: multiply by the norm of patches in X
        self.output = self.output * self.patch_norm


    ### mult layer
    def mult_layer(self):
        """Multiplication layer

        Compute kappa(ZtZ)^{-1/2} * (ZtZ)^{-1/2}
        """
        # multiply ||X|| * kappa(Zt X/||X||) by (ZtZ)^{-1/2}
        self.output = tf.tensordot(self.output,
                                   self.sqrt_inv_kZtZ,
                                   axes=[[3],[0]])


    ### pooling layer
    def pool_layer(self):
        """Pooling layer

        Compute I(z) = \sum_{z'} phi(z') x exp(-\beta_1 ||z'-z||_2^2)

        No pooling if `subsampling=1`
        """
        ## direct version
        if self.subsampling > 1:
            g_filter = tf.expand_dims(tf.tile(
                                        tf.expand_dims(self.gaussian_filter, 2),
                                            [1,1,self.out_channels]), 3)
            self.output = tf.nn.depthwise_conv2d(self.output, g_filter,
                                                 strides=[1, self.subsampling,
                                                          self.subsampling, 1],
                                                 padding=self.padding)


    #### normalize the filters
    def normalize_filters(self, session, feed_dict=None,
                          run_options=None, writer=None, timeline=None):
        """Normalize the filters of the current layer

        Args:
            session (tf.Session): tensorflow current session.
            feed_dict (dictionary): feeding dictionary for potential
                placeholders, default is None.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """

        tag = datetime.now().strftime("%Y%m%d_%H%M%S.%f")

        run_metadata = tf.RunMetadata()
        session.run(self.normalize_Z, feed_dict=feed_dict,
                    options=run_options, run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "normalize_Z_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)


    #### exctract patches
    def _extract_patches(self):
        """extract patches from images for sub-sampling and k-means
        """

        ### extract patches
        self.patches = tf.extract_image_patches(images=self.input,
                                                ksizes=[1,
                                                        self.filter_height,
                                                        self.filter_width,
                                                        1],
                                                strides=[1, 1, 1, 1],
                                                rates=[1, 1, 1, 1],
                                                padding="VALID",
                                                name="extract_patches")
        # convert patches to 2-D matrix
        self.patches = tf.reshape(self.patches,
                                  shape=[-1,
                                         self.filter_height
                                         * self.filter_width
                                         * self.in_channels],
                                  name="patch_reshape")


    #### concat sampled patches from batch
    def append_patches(self, session, n_batch=1, feed_dict=None,
                       run_options=None, writer=None, timeline=None):
        """Sample patches from current batch and concatenate with patches
        from previous batches

        Args:
            session (tf.Session): tensorflow current session.
            n_batch (int): number of batches in the data, default is 1.
            feed_dict (dictionary): feeding dictionary for potential
                placeholders, default is None.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """

        tag = datetime.now().strftime("%Y%m%d_%H%M%S.%f")

        # input data shape
        run_metadata = tf.RunMetadata()
        self.batch_size,_,_,_ = session.run(self.input_shape,
                                            feed_dict=feed_dict,
                                            options=run_options,
                                            run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "batch_size_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)

        # # nb of patches
        # if(self.padding == "VALID"):
        #     n_patches = self.batch_size \
        #                     * (self.in_height - self.filter_height + 1) \
        #                     * (self.in_width - self.filter_width + 1)
        # else: ## SAME pagging
        #     n_patches = self.batch_size * self.in_height * self.in_width

        # number of sampled patches
        n_sampled_patches = self.n_sampled_patches // n_batch

        # extract patches
        run_metadata = tf.RunMetadata()
        patches = session.run(self.patches, feed_dict=feed_dict,
                              options=run_options,
                              run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "append_patches_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)

        # nb of patches
        n_patches = patches.shape[0]

        if n_sampled_patches > n_patches:
            n_sampled_patches = n_patches

        ## sub-sampling
        # logging.debug("START: patch subsampling in \"{}\""
        #                     .format(self.name))
        # start = timer()
        sampled_id = np.random.randint(low=0,
                                       high=n_patches,
                                       size=n_sampled_patches)
        patches = patches[sampled_id,]

        # end = timer()
        # logging.debug("END: patch subsampling in {:.4f} sec"
        #                     .format(end - start))

        ## append sampled_patches to patch_set
        # logging.debug("START: append patches from \"{}\""
        #                     .format(self.name))
        # start = timer()
        self.patch_set.append(patches)
        # end = timer()
        # logging.debug("END: append patches in {:.4f} sec"
        #                     .format(end - start))


    #### concat sampled patches from batch
    def concatenate_patches(self):
        """Concatenate sampled patches

        Should be called after self.append_patches
        """

        # logging.debug("START: patch concatenation in \"{}\""
        #                     .format(self.name))
        # start = timer()
        self.patch_set = np.concatenate(self.patch_set, axis=0)
        # end = timer()
        # logging.debug("END: patch concatenation in {:.4f} sec"
        #                     .format(end - start))


    ### sub-sampling and k-means step
    def compute_kmeans(self, n_iter=10):
        """sub-sampling and spherical k-means

        Args:
            n_iter (int): number of iterations in spherical k-means. Default
                is 10. If set to 0, only a sub-sampling and random
                initialization of cluster centers are done.
        """
        # k-means
        logging.debug("START: kmeans in layer \"{}\"".format(self.name))
        start = timer()
        clusters_centers = spherical_kmeans.kmeans.fast_kmeans(
                                    input_data=self.patch_set,
                                    n_clusters=self.out_channels,
                                    num_iter=n_iter,
                                    verbose=0)
        end = timer()
        logging.debug("END: kmeans in layer \"{}\" in {:.4f} sec"
                .format(self.name, end - start))
        logging.debug("Layer \"{}\": clusters_centers shape = {}"
                        .format(self.name, clusters_centers.shape))
        self.patch_set = []

        ## check for null filters
        centroid_norm = np.linalg.norm(clusters_centers, axis=1)
        logging.debug("Layer \"{}\": check null filters after k-means: {}"
                        .format(self.name, any(centroid_norm < 1e-5)))

        return np.reshape(np.transpose(clusters_centers),
                          [self.filter_height, self.filter_width,
                           self.in_channels, self.out_channels])


    ### update filters
    def update_filters_kmeans(self, n_iter=10):
        """update filters Z and compute k(ZtZ)^{-1/2} from the k-means

        Args:
            n_iter (int): number of iterations in spherical k-means. Default
                is 10. If set to 0, only a sub-sampling and random
                initialization of cluster centers are done.

        """

        with tf.variable_scope("k-means") as scope:
            ### update filters
            self.update_Z_kmeans = tf.assign(self.Z,
                                             self.compute_kmeans(n_iter))


    def compute_update_filters_kmeans(self, session, run_options=None,
                                      writer=None, timeline=None):
        """compute update filters Z and compute k(ZtZ)^{-1/2} from the k-means

        To be called in unsupervised training

        Args:
            session (tf.Session): tensorflow current session.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """

        tag = datetime.now().strftime("%Y%m%d_%H%M%S.%f")

        ### compute k-means
        run_metadata = tf.RunMetadata()
        session.run(self.update_Z_kmeans,
                    options=run_options, run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata,
        #                             "update_filters_k_means_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)

        ### compute sqrt_inv_kZtZ
        run_metadata = tf.RunMetadata()
        session.run(self.update_sqrt_inv_kZtZ,
                    options=run_options, run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "sqrt_inv_kZtZ_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)

        if DEBUG:
            ## check for nan or inf
            self.check_filters(session, run_options=run_options,
                               writer=writer,
                               timeline=timeline)


    ### update filters
    def update_filters(self, Z):
        """update filters Z and compute k(ZtZ)^{-1/2}

        Args:
            Z (4-D tensor): values to update the filters.
        """
        with tf.variable_scope("update_Z") as scope:
            ### update filters
            self.update_Z = tf.assign(self.Z, Z)


    def compute_update_filters(self, session, run_options=None,
                               writer=None, timeline=None):
        """update filters Z and compute k(ZtZ)^{-1/2}

        Args:
            session (tf.Session): tensorflow current session.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """

        tag = datetime.now().strftime("%Y%m%d_%H%M%S.%f")

        ### compute update
        run_metadata = tf.RunMetadata()
        session.run(self.update_Z,
                    options=run_options, run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "update_filters_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)

        ### compute sqrt_inv_kZtZ
        run_metadata = tf.RunMetadata()
        session.run(self.update_sqrt_inv_kZtZ,
                    options=run_options, run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "sqrt_inv_kZtZ_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)


    def get_filters(self, session, run_options=None,
                    writer=None, timeline=None):
        """return the layer filters as numpy array

        Args:
            session (tf.Session): tensorflow current session.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """

        tag = datetime.now().strftime("%Y%m%d_%H%M%S.%f")

        run_metadata = tf.RunMetadata()
        filters = session.run(self.Z, options=run_options,
                              run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "get_filters_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)

        return filters


    #--------------------------------------------------------------------------#
    # CLASS ATTRIBUTES DECLARATION
    #--------------------------------------------------------------------------#
    def _declare_tensorflow_object(self):
        """declare Tensorflow computation graph attributes to None

        """
        self.input = None
        self.input_shape = None
        self.patch_norm = None
        self.output = None
        self.update_Z = None
        self.sqrt_inv_kZtZ = None
        self.update_sqrt_inv_kZtZ = None
        self.patches = None


    def _init_layer_param(self, filter_height, filter_width, out_channels,
                          subsampling, n_sampled_patches,
                          kernel, kernel_param,
                          dtype, padding, name,
                          epsilon_norm, epsilon_inv):
        """init layer parameters

        """
        ## attributes
        self.batch_size = None
        self.in_height = None
        self.in_width = None
        self.in_channels = None
        self.filter_height = filter_height
        self.filter_width = filter_width
        self.out_channels = out_channels
        self.subsampling = subsampling

        self.n_sampled_patches = n_sampled_patches

        self.kernel_param = kernel_param
        self._set_kernel(kernel)

        self.dtype = tf.as_dtype(dtype)
        self.padding = padding

        self.name = name

        self.epsilon_norm = epsilon_norm
        self.epsilon_inv = epsilon_inv


    def _set_kernel(self, kernel):
        """return kernel function associated to the name `kernel`

        """
        if kernel == 'exp' or kernel == 'exp0':
            self.kappa = kernel_exp
            self.kernel_param = 1/(self.kernel_param**2)
        elif kernel == 'poly2':
            self.kappa = kernel_poly2
            self.kernel_param = 0
        elif kernel == 'poly02':
            self.kappa = kernel_poly02
            self.kernel_param = 0
        elif kernel == 'inv_poly':
            self.kappa = kernel_inv_poly
            self.kernel_param = 0
        elif kernel == 'poly':
            self.kappa = kernel_poly
        elif kernel == 'acos0':
            self.kappa = kernel_acos0
            self.kernel_param = 0
        elif kernel == 'acos1':
            self.kappa = kernel_acos1
            self.kernel_param = 0
        elif kernel == 'acos2':
            self.kappa = kernel_acos2
            self.kernel_param = 0
        else:
            raise NotImplementedError("{} is not implemented at the moment"
                                        .format(str(kernel)))


    #--------------------------------------------------------------------------#
    # INTERNAL MEMBER FUNCTIONS
    #--------------------------------------------------------------------------#

    ### compute kappa( (ZtZ)^{-1/2})
    def _compute_sqrt_inv_kZtZ(self):
        """Compute kappa(ZtZ)^{-1/2}

        """
        if DEBUG:
            Z = tf.Print(self.Z, [tf.reduce_any(tf.is_nan(self.Z)),
                                  tf.reduce_any(tf.is_inf(self.Z))],
                         message="Layer \"" + self.name + "\": check_Z_after_proj",
                         name="check_Z_after_proj")
        else:
            Z = self.Z
        # step 1: ZtZ
        ZtZ = tf.tensordot(Z, Z, axes=[[0,1,2], [0,1,2]])
        if DEBUG:
            logging.debug("Layer \"{}\": ZtZ shape = {}"
                    .format(self.name, ZtZ.get_shape()))
            ZtZ = tf.Print(ZtZ, [tf.reduce_any(tf.is_nan(ZtZ)),
                                 tf.reduce_any(tf.is_inf(ZtZ))],
                           message="Layer \"" + self.name + "\": check_ZtZ",
                           name="check_ZtZ")
        # step 2: non-linearity kappa(ZtZ)
        kZtZ = self.kappa(ZtZ, self.kernel_param)
        if DEBUG:
            logging.debug("Layer \"{}\": kZtZ shape = {}"
                    .format(self.name, kZtZ.get_shape()))
            kZtZ = tf.Print(kZtZ, [tf.reduce_any(tf.is_nan(kZtZ)),
                                   tf.reduce_any(tf.is_inf(kZtZ))],
                            message="Layer \"" + self.name + "\": check_kZtZ",
                            name="check_kZtZ")
        # step 3: squared-root inverse
        return tf.cast(_matrix_sqrtinv(tf.cast(kZtZ, dtype=tf.float64),
                                        eps=self.epsilon_inv),
                        dtype=self.dtype)


    #--------------------------------------------------------------------------#
    # INTERNAL CHECKS
    #--------------------------------------------------------------------------#

    ### check filters
    def check_filters(self, session, run_options=None,
                      writer=None, timeline=None):
        """check for null, inf or NA filters

        Args:
            session (tf.Session): tensorflow current session.
            run_options (tf.RunOptions): A [RunOptions] protocol buffer
                used for tf Session running. Default is None.
            writer (tf.summary.FileWriter): a [FileWriter] object used to
                save metadata after a tf Session run. Default is None and no
                metadata are saved.
            timeline (ckntf.core.utils.TimeLiner): an object to concatenate
                events from a tf Session run in a timeline. Default is None,
                no timeline is saved.

        """
        tag = datetime.now().strftime("%Y%m%d_%H%M%S.%f")
        ## check for nan or inf
        run_metadata = tf.RunMetadata()
        Z = session.run(self.Z, options=run_options,
                        run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "check_filters_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)

        logging.debug("Layer \"{}\": check for nan or inf in filters after update: {}"
                        .format(self.name, _check_nan_inf(Z)))

        run_metadata = tf.RunMetadata()
        sqrt_inv_kZtZ = session.run(self.sqrt_inv_kZtZ,
                                    options=run_options,
                                    run_metadata=run_metadata)

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "check_sqrt_inv_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)

        logging.debug("Layer \"{}\": check for nan or inf in k(ZtZ)^{{-1/2}}: {}"
                        .format(self.name, _check_nan_inf(sqrt_inv_kZtZ)))

        run_metadata = tf.RunMetadata()
        norm = session.run(self.tmp_norm,
                           options=run_options,
                           run_metadata=run_metadata)

        norm = norm.ravel()

        # if writer is not None:
        #     writer.add_run_metadata(run_metadata, "filter_norm_" + tag)

        if timeline is not None:
            timeline.update_timeline(run_metadata)

        condition = bool((norm > 0.9).all() and (norm < 1.1).all())
        logging.debug("Layer \"{}\": filter norm is 1 for all: {}"
                .format(self.name, condition))
        if not condition:
            mask = np.any([(norm < 0.9), (norm > 1.1)], axis=0)
            logging.debug("Layer \"{}\": ill filter norm = \n{}"
                .format(self.name, norm[mask]))


#------------------------------------------------------------------------------#
# INTERNAL FUNCTIONS
#------------------------------------------------------------------------------#

def _gaussian_filter_1d(shape, sigma):
    """define a 1-D gaussian filter

    Filter defined as exp(-1/(2 \sgima^2) x^2) with
            x = (-m, -m+1, ..., 0, 1, ..., m-1, m)
    where m = (shape - 1)/2

    Args:
        shape (int): range of the filter
        sigma (float): dispersion parameter
    """
    m = (shape - 1)//2
    filt = np.arange(-m,m+1)
    filt = np.exp(-np.square(filt)/(2.*sigma**2))
    return filt/np.sum(filt)


def _sampled_index(n_patches, n_sampled_patches):
    """sampled 'n_sampled_patches' among 'n_patches'

    Args:
        n_patches (int): number of patches in the data set
        n_sampled_patches (int): number of patches to be sampled
    """
    sampled_index = np.arange(n_patches)
    np.random.shuffle(sampled_index)
    sampled_index = sampled_index[:n_sampled_patches]
    return sampled_index


def _check_nan_inf(input):
    """check if input contains any nan or inf values

    Args:
        input (numpy array): an array of values.

    Returns:
        a boolean value indicating if input contains any nan or inf values.
    """
    return np.isnan(input).any() or np.isinf(input).any()
