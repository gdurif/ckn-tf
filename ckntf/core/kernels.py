"""Definition of different non-linearity function (kernel function kernel) in the CKN

Reference: Mairal, Julien. "End-to-End Kernel Learning with Supervised
Convolutional Kernel Networks." In Advances in Neural Information Processing
Systems, 1399–1407, 2016.
http://papers.nips.cc/paper/6184-end-to-end-kernel-learning-with-supervised-convolutional-kernel-networks.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, Julien Mairal, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import tensorflow as tf
from math import pi

### non-linearity kernel_exp
def kernel_exp(X, alpha):
    """Compute non-linearity kernel_exp(X) element wise

    kernel_exp is defined as k(x) = exp(alpha * (x - 1))

    Args:
        X tensor
        alpha (0-D tensor or a scalar): parameter of the non-linearity
            kernel_exp

    Returns
        Tensor of same shape than X
    """
    return tf.exp(alpha * (tf.add(X,-1.0)))


### non-linearity kernel_exp
def kernel_exp0(X, sigma):
    """Compute non-linearity kernel_exp(X) element wise

    kernel_exp0 is defined as k(x) = exp((1/sigma^2) * (x - 1))

    Args:
        X tensor
        sigma (0-D tensor or a scalar): parameter of the non-linearity
            kernel_exp0

    Returns
        Tensor of same shape than X
    """
    return tf.exp((1/sigma**2) * (tf.add(X,-1.0)))


### non-linearity kernel_poly2
def kernel_poly2(X, param=None):
    """Compute non-linearity kernel_poly2(X) element wise

    kernel_poly2 is defined as k(x) = (1/4)*(x + 1)^2

    Args:
        X: tensor
        param: useless, present for compatibility issue

    Returns
        Tensor of same shape than X
    """
    return tf.scalar_mul(0.25,tf.square(tf.add(X,1.0)))


def kernel_poly02(X, param=None):
    """Compute non-linearity kernel_poly02(X) element wise

    kernel_poly02 is defined as k(x) = x^2

    Args:
        X: tensor
        param: useless, present for compatibility issue

    Returns
        Tensor of same shape than X
    """
    return tf.square(X)


def kernel_inv_poly(X, param=None):
    """Compute non-linearity kernel_inv_poly(X) element wise

    kernel_inv_poly is defined as k(x) = 1/(2-x)

    Args:
        X: tensor
        param: useless, present for compatibility issue

    Returns
        Tensor of same shape than X
    """
    return tf.reciprocal(tf.add(tf.negative(X),2.0))


### non-linearity kernel_poly
def kernel_poly(X,p):
    """Compute non-linearity kernel_poly(X,p) element wise

    kernel_poly is defined as k(x) = (x + 1)^p

    Args:
        X: tensor
        p: 0-D tensor or a scalar

    Returns
        Tensor of same shape than X
    """
    return tf.pow(tf.add(X,1.0),p)


### non-linearity kernel_arc_cosine0
def kernel_acos0(X, param=None):
    """Compute non-linearity kernel_acos0(X) element wise

    kernel_acos0 is defined as k(x) = (1/pi)*(pi - acos(x))

    Args:
        X: tensor
        param: useless, present for compatibility issue

    Returns
        Tensor of same shape than X
    """
    return tf.scalar_mul(1/pi,tf.add(-tf.acos(X),pi))


### non-linearity kernel_arc_cosine1
def kernel_acos1(X, param=None):
    """Compute non-linearity kernel_acos1(X) element wise

    kernel_acos1 is defined as k(x) = (1/pi)(sin(theta) + (pi-theta) * x) with theta=acos(x)

    Args:
        X: tensor
        param: useless, present for compatibility issue

    Returns
        Tensor of same shape than X
    """
    theta=tf.acos(X)
    return tf.scalar_mul(1/pi,tf.sin(theta) + tf.multiply(tf.add(-theta, pi), X))


### non-linearity kernel_arc_cosine2
def kernel_acos2(X, param=None):
    """Compute non-linearity kernel_acos2(X) element wise

    kernel_acos2 is defined as k(x) = (1/pi)(3*sin(theta)*x + (pi-theta) * (1+x^2)) with theta=acos(x)

    Args:
        X: tensor
        param: useless, present for compatibility issue

    Returns
        Tensor of same shape than X
    """
    theta=tf.acos(X)
    return tf.scalar_mul(1/pi,tf.scalar_mul(3, tf.multiply(theta,X)) \
        + tf.multiply(tf.add(-Y, pi),
                      tf.add(tf.scalar_mul(2, tf.square(X)), 1.0)))
