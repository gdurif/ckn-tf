"""cleaning output directory"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__license__ = "BSD"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import os
import pkg_resources
import shutil

def clean():
    """clean the 'ckn/output' directory"""

    output_dir = pkg_resources.resource_filename('ckntf', 'output')
    for root, dirs, files in os.walk(output_dir):
        for f in files:
            if not f in ('__init__.py', 'clean.py'):
                os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))


if __name__ == "__main__":
    clean()
