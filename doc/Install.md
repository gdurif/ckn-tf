# Installation
---

### GNU/Linux and MacOS

* To avoid messing with your system, we recommend to install our CKN package
  inside a python virtualenv. We also recommend to use the Python Anaconda
  distribution since it provides the MKL from Intel (for Blas and OpenMP),
  that is required for the additional [miso_svm](../miso_svm),
  [spherical_kmeans](../spherical_kmeans) and
  [whitening](../whitening) packages.

* When using python virtualenv:

```bash
virtualenv -p $(which python3) cknenv
source cknenv/bin/activate
### Install required Python package:
pip install numpy scipy
pip install tensorflow
```

You can check this [page](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
for more information on virtualenv.

* When using Anaconda:

  1) You can get anaconda or miniconda from <https://conda.io/docs/user-guide/install/index.html>
     or <https://conda.io/miniconda.html>.

  2) Create a conda virtual environment and install dependencies within it:

```bash
conda create -n cknenv  # if not done yet
source activate cknenv
conda install numpy scipy
conda install tensorflow=1.3
```

**NB:** Due to some modifications in Tensorflow C++ API in more recent version,
our `ckntf` package requires Tensorflow version 1.3.


* To install the 'ckntf' package:

  1) Get the sources from the git repository:

```bash
git clone https://gitlab.inria.fr/thoth/ckn-tf
cd ckn
```

  2) or from the archives:

```bash
wget http://pascal.inrialpes.fr/data2/gdurif/ckntf-1.0.tar.gz
tar zxvf ckntf-1.0.tar.gz
cd ckntf-1.0
```

  3) Get the required data file:

```bash
wget http://pascal.inrialpes.fr/data2/gdurif/cifar10_whitened.pkl
mv cifar10_whitened.pkl ckntf/data
wget http://pascal.inrialpes.fr/data2/gdurif/cifar10_batches.tar.gz
tar zxvf cifar10_batches.tar.gz
mv cifar10_batches ckntf/data
```

**NB:** these data files are necessary to run the joined examples (but not
mandatory to install the 'ckntf' package).

  4) Install (you can use the option `--prefix=/path/to/dir` to choose the
     installation directory):

```bash
python setup.py install
```

---

### Windows
If you encounter any issue when installing the 'ckntf' package,
you can contact us at <ckn.dev@inria.fr>.
