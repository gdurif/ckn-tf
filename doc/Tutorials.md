# Tutorials
--------------------------------------------------------------------------------

## Introduction
We give here a small presentation of the 'ckntf' package:
* how to use a CKN layer in unsupervised mode (definition and training).
* how to use a CKN model (with one or more layers) in unsupervised and
  supervised mode (definition and training).

### Unsupervised mode

When using a single CKN layer or a full CKN model in unsupervised mode,
the filters of each layer are trained with a spherical k-means algorithm
applied to a set of patches sampled from the previous layer output (or from
the batch of input images for the first layer).

The output from a CKN layer/model trained in unsupervised mode can be used
for any learning task. For example, one can unsupervisingly train a CKN model
from a training set of images. Then, one can encode the training and test sets
of images thanks to this model, and use these transformed features to solve
a classification problem.

Full examples of CKN model definition and unsupervised training
with classification (based on a MISO SVM algorithm) are available in
[ckntf.tutorials.unsupervised_training.py](../ckntf/tutorials/unsupervised_training.py).

In addition the script [ckntf.tutorials.unsupervised_example.py](../ckntf/tutorials/unsupervised_example.py).
can be used to reproduce the results from Mairal (2016), with different
network architectures (c.f. [general README file](../README.md)).

### Supervised mode

In supervised mode, the training of a CKN model is done as follows: the labels
of images are used to optimize a loss regarding the filters of each CKN layer
in the considered model.

Full examples of CKN model definition and supervised training are available in
[ckntf.tutorials.supervised_training.py](../ckntf/tutorials/supervised_training.py).

In addition the script [ckntf.tutorials.unsupervised_example.py](../ckntf/tutorials/supervised_example.py).
can be used to reproduce the results from Mairal (2016), with different
network architecture (c.f. [general README file](../README.md)).

---

## Examples of unsupervised training

To get better performance, we encourage users to run unsupervised training
on CPU. If you are using the package 'tensorflow_gpu', you can set the
following environment variable (on Unix system) to avoid using your GPU:

```bash
export CUDA_VISIBLE_DEVICES=""
```

1) [Import modules and data](#import-module-and-data) <br>
2) [Use of a single CKN layer](#use-of-a-single-ckn-layer) <br>
3) [Use of the CKN model wrapper](#use-of-the-ckn-model-wrapper) <br>

### Import modules and data

* Choose your logging message level (INFO, DEBUG, etc.)

```python
import logging
import sys
logging.basicConfig(stream=sys.stdout,
                    format='%(levelname)s:%(message)s',
                    level=logging.INFO)
```

* Import TensorFlow, Numpy and 'ckntf' sub-modules

```python
import numpy as np
import tensorflow as tf
import ckntf.core
import ckntf.input
```

* Argument related to CIFAR-10 data (careful: large 'batch_size' requires a
  lot of memory)

```python
class args:
    sample_size = 10000
    batch_size = 500
    data_file = pkg_resources.resource_filename('ckntf',
                                                'data/cifar10_whitened.pkl')
```

* Import the data

```python
(Xtr, Ytr,
 Xte, Yte) = ckntf.input.read_whitened_dataset_cifar10(args.data_file,
                                                       args.sample_size)
(N, H, W, C) = Xtr.shape
```

* TensorFlow session

```python
sess = tf.InteractiveSession()
```

* Input for the layer/model:

```python
input = tf.placeholder(dtype=param.dtype, shape=[None, H, W, C])
```

---

### Use of a single CKN layer

* Parameters defining the CKN layer

```python
class params:
    filter_height = 3
    filter_width = 3
    out_channels = 64
    subsampling = 2
    supervised = False
    n_sampled_patches = 10000
    kernel = 'exp0'
    kernel_param = 0.7
    dtype = tf.float32
    padding = "SAME"
    name = "layer1"
    epsilon_norm = 1e-5
    epsilon_inv = 1e-3
```

* Layer declaration

```python
simple_layer = ckntf.core.CKNlayer.from_param(params)
```

* Layer encoding

```python
output = simple_layer.encode(input)
```

* Unsupervised training of the filters (using batches of images)

```python
# batch number
n_batch = Xtr.shape[0] // args.batch_size \
            + 1 * (args.sample_size % args.batch_size != 0)

# patch extraction (by batch)
for i_batch in range(0,(n_batch)):

    i_start = i_batch * args.batch_size
    i_end = Xtr.shape[0] if i_batch == n_batch - 1 \
            else (i_batch + 1) * args.batch_size
    sub_data = Xtr[i_start:i_end,]
    ## feed placeholder
    simple_layer.append_patches(sess,
            n_batch=n_batch, feed_dict={input: sub_data})

# update the filters with k-means from the extracted patches
simple_layer.update_filters_kmeans(sess)
```

* How to get the trained filters (as a np.array)

```python
filters = simple_layer.get_filters(sess)
```

* How to set the filters (from a np.array)

```python
simple_layer.update_filters(filters, sess)
```

* How to compute the output of the layer (using batches of images)

```python
output = None
start = timer()
for i_batch in range(0,(n_batch)):
    i_start = i_batch * args.batch_size
    i_end = Xtr.shape[0] if i_batch == n_batch - 1 \
            else (i_batch + 1) * args.batch_size
    sub_data = Xtr[i_start:i_end,]
    ## feed placeholder
    tmp = sess.run(simple_layer.output,
                   feed_dict={input: sub_data})
    output = tmp if output is None \
             else np.concatenate((tmp, output), axis=0)
```

---

### Use of the CKN model wrapper

* Parameters defining the CKN model (with 2 layers)

```python
params = [
    {
        'filter_height': 3, 'filter_width': 3,
        'out_channels': 64, 'subsampling': 2,
        'supervised': False,
        'n_sampled_patches': 1000000,
        'kernel': 'exp0', 'kernel_param': 0.6,
        'dtype': tf.float32, 'padding': "SAME", 'name': "layer1",
        'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
    },
    {
        'filter_height': 2, 'filter_width': 2,
        'out_channels': 256, 'subsampling': 6,
        'supervised': False,
        'n_sampled_patches': 1000000,
        'kernel': 'exp0', 'kernel_param': 0.6,
        'dtype': tf.float32, 'padding': "SAME", 'name': "layer2",
        'epsilon_norm': 1e-5, 'epsilon_inv': 1e-3
    }
]
```

* Definition of the model from the parameters

```python
model = ckntf.core.CKNmodel.from_param(params, sess, args.batch_size)
```

* Encoding of the model

```python
model.encode(H, W, C)
```

* Unsupervised training of the model
```python
model.unsup_train(input_data=Xtr)
```

* How to get the trained filters (as a list of np.array)

```python
filter_list = model.get_filters()
```

* How to set the filters (from a list of np.array)

```python
model.update_filters(filter_list)
```

* How to encode some data with a trained model

```python
output = model.get_output(input_data=Xtr)
```


---

## References

Mairal, J., 2016. End-to-end kernel learning with supervised convolutional kernel networks, in: Advances in Neural Information Processing Systems. pp. 1399–1407.
