import pkg_resources

import numpy as np
import tensorflow as tf

import ckn.input

from sklearn.feature_extraction import image
import matplotlib.pyplot as plt

class args:
    sample_size = 10
    ## data
    folder = pkg_resources.resource_filename('ckn', 'data')

X, _,_,_ = ckn.input.load_cifar10(args.folder,
                                  args.sample_size,
                                  whitened=False)

Xw, _,_,_ = ckn.input.load_cifar10(args.folder,
                                   args.sample_size,
                                   whitened=True)


(N, in_height, in_width, in_channels) = X.shape
filter_height = 3
filter_width = 3

sess = tf.InteractiveSession()

input = tf.placeholder(dtype=tf.as_dtype(X.dtype),
                       shape=[None, in_height, in_width, in_channels],
                       name='input')

### extract patches
patches = tf.extract_image_patches(images=input,
                                   ksizes=[1,
                                           filter_height,
                                           filter_width,
                                           1],
                                    strides=[1, 1, 1, 1],
                                    rates=[1, 1, 1, 1],
                                    padding="VALID",
                                    name="patch_extraction")
# dim of 'patches' = [ batch_size, out_heigth, out_width,
#                      filter_height * filter_width * in_channels ]
test_patches = sess.run(patches, feed_dict={input: X})

# number of patches in an image channel
n_patches = (in_height - filter_height + 1) * (in_width - filter_width + 1)

# size of the patches
patch_size = filter_height * filter_width * in_channels

# convert patches to 2-D matrix
patches = tf.reshape(patches,
                     shape=[-1, n_patches, patch_size],
                     name="patch_reshape")
test_patches = sess.run(patches, feed_dict={input: X})

# center
patch_mean = tf.reduce_mean(patches, axis=1, keep_dims=True, name="patch_mean")
test_patch_mean = sess.run(patch_mean, feed_dict={input: X})

patches = patches - patch_mean
test_patches = sess.run(patches, feed_dict={input: X})

# covariance
cov = tf.matmul(tf.transpose(patches, [0, 2, 1]), patches)

# svd
s, U = tf.self_adjoint_eig(cov, name="eigen_decomposition")
test_s, test_U = sess.run([s, U], feed_dict={input: X})

null_s = tf.zeros(dtype=s.dtype, shape=tf.shape(s), name="null_vector")
s = tf.where(tf.greater(s, null_s), s, null_s)
s = tf.where(tf.greater(s, 1e-8 * tf.reduce_max(s, axis=0, keep_dims=True, name="max_eigenval")),
             1. / tf.sqrt(s), null_s)

out = tf.matmul(patches, tf.matmul(tf.matmul(U, tf.matrix_diag(s)), tf.transpose(U, [0, 2, 1])))
test_out = sess.run(out, feed_dict={input: X}).reshape(-1, n_patches, filter_height, filter_width, in_channels)

print(test_out.shape)

## check
X1 = image.reconstruct_from_patches_2d(test_out[1], (in_height, in_width, in_channels))

plt.figure(1)
plt.subplot(311)
plt.imshow(X[1])
plt.title("Original image")
plt.subplot(312)
plt.imshow(Xw[1]/0.4 + 0.2)
plt.title("Pre-whitened image")
plt.subplot(313)
plt.imshow(X1/0.4 + 0.2)
plt.title("Whitened image")
plt.show()
