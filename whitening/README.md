# Spherical kmeans

This package implements a whitening algorithm to cut high frequencies in images
(FIXME ref?).

The 'whitening' package is based on C++ interfaced codes.

All files included in the 'whitening' package ([whitening/*]
and in particular [whitening/whitening/*]) are released under
the GPL-v3 license.

---

# Installation

This package requires the MKL from Intel (for Blas and OpenMP). You can get
the MKL by using the Python Anaconda distribution, or you can use your own
MKL license if you have one.


## When using anaconda

You can get anaconda or miniconda from <https://conda.io/docs/user-guide/install/index.html>
or <https://conda.io/miniconda.html>.

Create a conda virtual environment and install dependencies within it:
```bash
conda create -n cknenv # if not done yet
source activate cknenv
conda install numpy scipy scikit-learn matplotlib
```

## Install package

* On GNU/Linux and MacOS:

If using previously created conda environment:
```bash
source activate cknenv
```

then
```bash
git clone https://gitlab.inria.fr/thoth/ckn-tf
cd ckn-tf/whitening
python setup.py install
```

OR

```bash
wget http://pascal.inrialpes.fr/data2/gdurif/whitening-1.0.tar.gz
tar zxvf whitening-1.0.tar.gz
cd whitening-1.0
python setup.py install
```

To specify an installation directory:
```bash
inst=<your_install_directory>
PYV=$(python -c "import sys;t='{v[0]}.{v[1]}'.format(v=list(sys.version_info[:2]));sys.stdout.write(t)";)
export PYTHONPATH=$inst/lib/python${PYV}/site-packages:$PYTHONPATH
python setup.py install --prefix=$inst
```


## When using the official GitLab repository (for developpers)

(on GNU/Linux and MacOs only)

* To build/install/test the package, see:

```bash
./dev_command.sh help
```


## Example of use

See [whiten_test.py](whitening/whiten_test.py)
