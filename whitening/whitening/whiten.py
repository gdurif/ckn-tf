#! /usr/bin/env python
"""Wrapper for fast spherical k-means by Julien Mairal"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"

import numpy as np
from sklearn.feature_extraction import image

from whitening._transform import cwhitening


def whiten(input_image, patch_size):
    """whitening code (wrapper for Julien Mairal's code in C)

    Args:
        input_images (np.array): image of shape [heigth, width, channels].
        patch_size (int): size of the patches to extract (the function can
            only deal with squared patches, where patch_height = patch_width).

    """

    ## number of channels
    channels = input_image.shape[-1]
    ## extract patches
    patch_set = image.extract_patches_2d(input_image, (patch_size, patch_size))
    npatch = patch_set.shape[0]
    ## centering by channel
    patch_set = patch_set.reshape(patch_set.shape[0], patch_size * patch_size, -1)
    mean_p = patch_set.mean(axis=(1))
    patch_set = patch_set - np.repeat(np.expand_dims(mean_p, axis=1), patch_size * patch_size, axis=1)
    patch_set = patch_set.reshape(npatch, -1)
    ## whitening
    cwhitening(patch_set, channels)
    ## return
    return patch_set
