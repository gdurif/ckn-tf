"""tests on functions to load data set for CKN tests"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

__author__ = "Ghislain Durif, THOTH TEAM INRIA Grenoble Alpes"
__copyright__ = "INRIA"
__credits__ = ["Alberto Bietti", "Dexiong Chen", "Ghislain Durif",
               "Julien Mairal", "Daan Wynen"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Ghislain Durif"
__email__ = "ghislain.durif@inria.fr"
__status__ = "Development"
__date__ = "2017"


import logging
import sys
logging.basicConfig(stream=sys.stdout,
                    format='%(levelname)s:%(message)s',
                    level=logging.DEBUG)
import pkg_resources
import unittest
import matplotlib.pyplot as plt

from timeit import default_timer as timer

import numpy as np
from sklearn.feature_extraction import image

import ckntf.input
from whitening.whiten import whiten

class args:
    sample_size = 1
    ## data
    folder = pkg_resources.resource_filename('ckn', 'data')


def whiten_images(X, patch_size,
                  verbose=True, use_C=True):
    """whiten images (cut down high frequencies)

    Args:
        X (np.array): images, shape (num_images, height, width, num_channels).
        patch_size (int): size of the patches to extract (the function can
            only deal with squared patches, where patch_height = patch_width).
        verbose (bool): set verbosity.

    Whitening is made in place on X.

    """

    # input dimension
    h, w, c = X.shape[1:]

    logging.debug("whitening")
    for idx in range(X.shape[0]):
        im = X[idx]
        if use_C:
            im = whiten(im, patch_size)
        else:
            im = pywhiten(im, patch_size, patch_size)
        im = im.reshape(im.shape[0], patch_size, patch_size, -1)
        X[idx] = image.reconstruct_from_patches_2d(im, (h, w, c))


def pywhiten(input_image, patch_height, patch_width):
    """whitening code in Python

    Args:
        input_images (np.array): image of shape [heigth, width, channels].
        patch_height (int): height of the required patch.
        patch_width (int): width of the required patch.

    """
    p = image.extract_patches_2d(input_image, (patch_height, patch_width))
    npatch = p.shape[0]

    ## centering by channel
    p = p.reshape(p.shape[0], patch_height * patch_width, -1)
    mean_p = p.mean(axis=(1))
    p = p - np.repeat(np.expand_dims(mean_p, axis=1), patch_height * patch_width, axis=1)
    p = p.reshape(npatch, -1)

    ## centering
    mean_p = p.mean(axis=0)
    p = p - mean_p

    ## filtering
    U, s, _ = np.linalg.svd(p.T, full_matrices=False)
    s[s < 0] = 0
    ind = s > 1e-8 * s.max()
    s[ind] = 1. / np.sqrt(s[ind])
    s[~ind] = 0
    Wfilt = np.matmul(U, np.matmul(np.diag(s), U.T))
    p = np.matmul(p, Wfilt)
    return p


class TransformDataTest(unittest.TestCase):

    def test_whiten_images(self):
        logging.info("testing `whiten_images`")

        logging.info("loading pre-whitened images")
        Xw, _,_,_ = ckntf.input.load_cifar10(args.folder,
                                             args.sample_size,
                                             whitened=True)

        logging.info("loading standard images")
        X, _,_,_ = ckntf.input.load_cifar10(args.folder,
                                            args.sample_size,
                                            whitened=False)

        X0 = np.copy(X)
        X1 = np.copy(X)

        logging.info("START: whitening step (Python)")
        start = timer()
        whiten_images(X, 3, use_C=False)
        end = timer()
        logging.info("END: whitening of {} images done in {:.4f} sec"
                        .format(args.sample_size,
                                end - start))

        logging.info("START: whitening step (C)")
        start = timer()
        whiten_images(X1, 3, use_C=True)
        end = timer()
        logging.info("END: whitening of {} images done in {:.4f} sec"
                        .format(args.sample_size,
                                end - start))

        plt.figure(1)
        plt.subplot(421)
        plt.imshow(X0[1])
        plt.title("Original image")
        plt.subplot(423)
        plt.imshow(Xw[1])
        plt.title("Pre-whitened image")
        plt.subplot(425)
        plt.imshow(X[1])
        plt.title("Whitened image (Python)")
        plt.subplot(427)
        plt.imshow(X1[1])
        plt.title("Whitened image (C)")
        plt.subplot(422)
        plt.imshow(X0[2])
        plt.title("Original image")
        plt.subplot(424)
        plt.imshow(Xw[2])
        plt.title("Pre-whitened image")
        plt.subplot(426)
        plt.imshow(X[2])
        plt.title("Whitened image (Python)")
        plt.subplot(428)
        plt.imshow(X1[2])
        plt.title("Whitened image (C)")
        plt.tight_layout()
        plt.show()


if __name__ == "__main__":
    unittest.main()
