#ifndef WHITENING_H
#define WHITENING_H

#include "linalg.h"
#include "common.h"
#include <iostream>


template <typename T>
void wrapper_whitening(T* X,
                    const int n_features,
                    const int n_samples,
                    const int channels) {
    Matrix<T> X_mat(X, n_features, n_samples);
    // centering(X_mat, channels);
    whitening(X_mat);
};


#endif // WHITENING_H
